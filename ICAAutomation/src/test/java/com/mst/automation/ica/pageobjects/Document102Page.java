package com.mst.automation.ica.pageobjects;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.customexception.CustomException;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document102Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document102Page(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//div[1]/span[text()='Last Name']/following::span[2]")
	public WebElement clmntLastName;
	
	@FindBy(xpath = "//div[1]/span[text()='First Name']/following::span[2]")
	public WebElement clmntFirstName;
	
	@FindBy(xpath = "//div[1]/span[text()='Last Name']/following::span[30]")
	public WebElement socialSecurityNo;
	
	@FindBy(xpath = "//div[1]/span[text()='Last Name']/following::span[18]")
	public WebElement clmntZipcode;
	
	@FindBy(xpath = "//div[1]/span[text()='Sex']/following::span[14]")
	public WebElement clmntState;
	
	@FindBy(xpath = "//div[1]/span[text()='Last Name']/following::span[26]")
	public WebElement clmntCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Sex']/following::span[22]")
	public WebElement clmntAddress;
	
	@FindBy(xpath = "//div[1]/span[text()='Sex']/following::span[2]")
	public WebElement clmntSex;
	
	@FindBy(xpath = "//div[1]/span[text()='Marital Status']/following::span[2]")
	public WebElement clmntMaritalStatus;
	
	@FindBy(xpath = "//div[1]/span[text()='Occupation When Injured']/following::span[2]")
	public WebElement clmntOcc;
	
	@FindBy(xpath = "//div[1]/span[text()='Time of Injury']/following::span[2]")
	public WebElement tmeOfinjury;
	
	@FindBy(xpath = "//div[1]/span[text()='Employer']/following::span[2]")
	public WebElement employer;
	
	@FindBy(xpath = "//div[1]/span[text()='Employer']/following::span[6]")
	public WebElement empState;
	
	@FindBy(xpath = "//div[1]/span[text()='Employer']/following::span[6]")
	public WebElement empCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Office Address']/following::span[2]")
	public WebElement empOfceAddr;
	
	@FindBy(xpath = "//div[1]/span[text()='Office Address']/following::span[6]")
	public WebElement empZipcode;
	
	@FindBy(xpath = "//div[1]/span[text()='Location']/following::span[2]")
	public WebElement trtmntLocation;
	
	@FindBy(xpath = "//div[1]/span[text()='Complaints & Physical Findings']/following::span[5]")
	public WebElement cmplntAndPhysical;
	
	@FindBy(xpath = "//div[1]/span[text()='Describe Treatment Given By You']/following::span[2]")
	public WebElement dscrbeTreatment;
	
	@FindBy(xpath = "//div[1]/span[text()='Were X-Rays Taken?']/following::span[2]")
	public WebElement wereXRay;
	
	@FindBy(xpath = "//div[1]/span[text()='Was Laboratory Work Done?']/following::span[2]")
	public WebElement wasLaboratory;
	
	@FindBy(xpath = "//div[1]/span[text()='Was Patient Hospitalized?']/following::span[2]")
	public WebElement wasPatientHospitalized;
	
	@FindBy(xpath = "//div[1]/span[text()='Is Further Treatment Needed?']/following::span[2]")
	public WebElement furthrTrmntNeeded;
	
	@FindBy(xpath = "//div[1]/span[text()='Subject to Sustain Perm. Impairment?']/following::span[5]")
	public WebElement subjectToSustain;
	
	@FindBy(xpath = "//div[1]/span[text()='Subject to Sustain Perm. Impairment?']/following::span[12]")
	public WebElement ableToDoSameWork;
	
	@FindBy(xpath = "//h3/button/span[text()='Medical Provider Details']/following::span[3]")
	public WebElement nameOfPhysician;
	
	@FindBy(xpath = "//h3/button/span[text()='Medical Provider Details']/following::span[11]")
	public WebElement medAddress;
	
	@FindBy(xpath = "//h3/button/span[text()='Medical Provider Details']/following::span[15]")
	public WebElement medZip;
	
	@FindBy(xpath="//span[text()='Match Flag']/following::div[1]/span")
	public WebElement matchFlag;
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='Match Flag']/following::div[1]/button/span")
	public WebElement matchFlagEdit;
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='Related']")
	public WebElement related;
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='Details']")
	public WebElement details;
	
	@FindBy(xpath="//div[@class='riseTransitionEnabled test-id__inline-edit-record-layout-container risen']//../span[text()='Match Flag']/following::div[4]/a")
	public WebElement editMatchFlagDetailPage;
	
	@FindBy(xpath="//div[@class='riseTransitionEnabled test-id__inline-edit-record-layout-container risen']//../span[text()='Insurance Carrier']/following::input")
	public WebElement insuranceCarrier;
	
	@FindBy(css="a[title='Original']")
	public WebElement original;
	
	@FindBy(css="button[title='Save']")
	public WebElement save;
	
	@FindBy(css = "a[title='Edit']")
	public WebElement edit;
	
	@FindBy(xpath="//span[text()='Owner']/following::force-lookup[1]/span")
	public WebElement owner;
	
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//./table/tbody/tr/td[1]/a")
	public WebElement documentsTableClaim;
	
	public static String claimNumber;
	public static String claimNumberDuplicate;
	
	public ClaimsEditPage document102DetailPage(String methodName, String tcName, ReportGenerator generator) throws Exception {
		Thread.sleep(3000);
		generator.childReport("verified the match flag");
		SeleniumUtils.highLightElement(matchFlag, driver);
		String text = matchFlag.getText();
		TestUtils.compareText(text, GoogleSheetAPI.ReadData(methodName, tcName, "matchFlag"), matchFlag, driver);
		
		generator.childReport("Click the related tab");
		SeleniumUtils.highLightElement(related, driver);
		related.click();		
		return new ClaimsEditPage(driver);
	}
	

	public ClaimDetailPage compareClaimNumbers(String methodName, String tcName, ReportGenerator generator) throws InterruptedException, IOException {
		
		Thread.sleep(10000);
		generator.childReport("Get the Claim record Number");
		SeleniumUtils.highLightElement(documentsTableClaim, driver);
		claimNumber = documentsTableClaim.getText();
		
		Thread.sleep(3000);
		details.click();
		
		Thread.sleep(4000);
		SeleniumUtils.highLightElement(matchFlagEdit, driver);
		matchFlagEdit.click();
		
		Thread.sleep(3000);
		SeleniumUtils.highLightElement(editMatchFlagDetailPage, driver);
		editMatchFlagDetailPage.click();
		
		generator.childReport("Selected Original");
		Thread.sleep(2000);
		original.click();
		
		Thread.sleep(4000);
		generator.childReport("Entered Insurance Carrier");
		SeleniumUtils.highLightElement(insuranceCarrier, driver);
		insuranceCarrier.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "insurancecarrier"));
		
		generator.childReport("Click Save");
		Thread.sleep(2000);
		SeleniumUtils.highLightElement(save, driver);
		save.click();
		
		Thread.sleep(5000);
		SeleniumUtils.scrollup(driver);
		Thread.sleep(1000);
		SeleniumUtils.highLightElement(related, driver);
		related.click();
		
		Thread.sleep(4000);
		generator.childReport("Get the Duplicate claim record Number");
		SeleniumUtils.highLightElement(documentsTableClaim, driver);
		claimNumberDuplicate = documentsTableClaim.getText();
		
		if(!claimNumber.equals(claimNumberDuplicate))
			documentsTableClaim.click();
		else
			throw new CustomException("The Claim number is duplicate. It should be different");
		
	  	return new ClaimDetailPage(driver);
	}
	
	public HomePage getClaimNumber(String methodName, String tcName, ReportGenerator generator) throws InterruptedException, IOException {
		
		Thread.sleep(10000);
		generator.childReport("Get the Claim record Number");
		SeleniumUtils.highLightElement(documentsTableClaim, driver);
		claimNumber = documentsTableClaim.getText();
		System.out.println(claimNumber);
	  	return new HomePage(driver);
	}
	

	public ClaimDetailPage document102RelatedPage(String methodName, String tcName, ReportGenerator generator) throws InterruptedException, IOException {
		
		Thread.sleep(10000);
		generator.childReport("Click the Claim record");
		SeleniumUtils.highLightElement(documentsTableClaim, driver);
		 if(!(methodName.contains("ClaimMatchLogic"))) {
			String claimRecord = documentsTableClaim.getText();
			claimRecord = claimRecord.substring(0, 8);
			String date = CommonUtils.returnDateNumber();		
			if(date.contains(claimRecord)) {
				documentsTableClaim.click();
			}
			else
			{
				throw new CustomException("Date values are not matched");
			}
		 }
		 else
			documentsTableClaim.click();
	  	return new ClaimDetailPage(driver);
	}
	
	public void verify102Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		generator.childReport("Verify 102 Document");
		SeleniumUtils.highLightElement(clmntLastName, driver);
		String clmntLstNam = clmntLastName.getText();
		TestUtils.compareText(clmntLstNam, GoogleSheetAPI.ReadData(methodName, tcName, "worker last name"), clmntLastName, driver);
		
		SeleniumUtils.highLightElement(clmntFirstName, driver);
		String clmntFstNam = clmntFirstName.getText();
		TestUtils.compareText(clmntFstNam, GoogleSheetAPI.ReadData(methodName, tcName, "worker first name"), clmntFirstName, driver);
		
		SeleniumUtils.highLightElement(socialSecurityNo, driver);
		String socSecNo = socialSecurityNo.getText();
		TestUtils.compareText(socSecNo, GoogleSheetAPI.ReadData(methodName, tcName, "social security number"), socialSecurityNo, driver);
		
		SeleniumUtils.highLightElement(clmntZipcode, driver);
		String clmntZip = clmntZipcode.getText();
		TestUtils.compareText(clmntZip, GoogleSheetAPI.ReadData(methodName, tcName, "worker zipcode"), clmntZipcode, driver);
		
		SeleniumUtils.highLightElement(clmntState, driver);
		String clmntStat = clmntState.getText();
		TestUtils.compareText(clmntStat, GoogleSheetAPI.ReadData(methodName, tcName, "worker state"), clmntState, driver);
		
		SeleniumUtils.highLightElement(clmntCity, driver);
		String clmntCty = clmntCity.getText();
		TestUtils.compareText(clmntCty, GoogleSheetAPI.ReadData(methodName, tcName, "worker city"), clmntCity, driver);
		
		SeleniumUtils.highLightElement(clmntAddress, driver);
		String clmntAdd = clmntAddress.getText();
		TestUtils.compareText(clmntAdd, GoogleSheetAPI.ReadData(methodName, tcName, "worker address"), clmntAddress, driver);
		
		SeleniumUtils.highLightElement(clmntSex, driver);
		String cmntSex = clmntSex.getText();
		TestUtils.compareText(cmntSex, GoogleSheetAPI.ReadData(methodName, tcName, "worker sex"), clmntSex, driver);
		
		SeleniumUtils.highLightElement(clmntMaritalStatus, driver);
		String cmntMarStat = clmntMaritalStatus.getText();
		TestUtils.compareText(cmntMarStat, GoogleSheetAPI.ReadData(methodName, tcName, "worker marital status"), clmntMaritalStatus, driver);
		
		SeleniumUtils.highLightElement(clmntOcc, driver);
		String cmntOccup = clmntOcc.getText();
		TestUtils.compareText(cmntOccup, GoogleSheetAPI.ReadData(methodName, tcName, "worker occupation"), clmntOcc, driver);
		
		SeleniumUtils.highLightElement(tmeOfinjury, driver);
		String timeOfjury = tmeOfinjury.getText();
		TestUtils.compareText(timeOfjury, GoogleSheetAPI.ReadData(methodName, tcName, "timeofinjury"), tmeOfinjury, driver);
		
		SeleniumUtils.highLightElement(employer, driver);
		String emp = employer.getText();
		TestUtils.compareText(emp, GoogleSheetAPI.ReadData(methodName, tcName, "Employer"), employer, driver);
		
		SeleniumUtils.highLightElement(empState, driver);
		String empStat = empState.getText();
		TestUtils.compareText(empStat, GoogleSheetAPI.ReadData(methodName, tcName, "worker state1"), empState, driver);
		
		SeleniumUtils.highLightElement(empCity, driver);
		String empCty = empCity.getText();
		TestUtils.compareText(empCty, GoogleSheetAPI.ReadData(methodName, tcName, "worker city1"), empCity, driver);
		
		SeleniumUtils.highLightElement(empOfceAddr, driver);
		String empOffAdd = empOfceAddr.getText();
		TestUtils.compareText(empOffAdd, GoogleSheetAPI.ReadData(methodName, tcName, "worker office address"), empOfceAddr, driver);
		
		SeleniumUtils.highLightElement(empZipcode, driver);
		String empZip = empZipcode.getText();
		TestUtils.compareText(empZip, GoogleSheetAPI.ReadData(methodName, tcName, "worker zipcode1"), empZipcode, driver);
		
		SeleniumUtils.highLightElement(trtmntLocation, driver);
		String trtmntLoc = trtmntLocation.getText();
		TestUtils.compareText(trtmntLoc, GoogleSheetAPI.ReadData(methodName, tcName, "location"), trtmntLocation, driver);
		
		SeleniumUtils.highLightElement(cmplntAndPhysical, driver);
		String cmpPhysical = cmplntAndPhysical.getText();
		TestUtils.compareText(cmpPhysical, GoogleSheetAPI.ReadData(methodName, tcName, "complaint physical findings"), cmplntAndPhysical, driver);
		
		SeleniumUtils.highLightElement(dscrbeTreatment, driver);
		String dscTreat = dscrbeTreatment.getText();
		TestUtils.compareText(dscTreat, GoogleSheetAPI.ReadData(methodName, tcName, "describe treatment"), dscrbeTreatment, driver);
		
		SeleniumUtils.highLightElement(wereXRay, driver);
		String wrXRay = wereXRay.getText();
		TestUtils.compareText(wrXRay, GoogleSheetAPI.ReadData(methodName, tcName, "were xrays taken"), wereXRay, driver);
		
		SeleniumUtils.highLightElement(wasLaboratory, driver);
		String waslab = wasLaboratory.getText();
		TestUtils.compareText(waslab, GoogleSheetAPI.ReadData(methodName, tcName, "laboratory work"), wasLaboratory, driver);
		
		SeleniumUtils.highLightElement(wasPatientHospitalized, driver);
		String patientHospitalized = wasPatientHospitalized.getText();
		TestUtils.compareText(patientHospitalized, GoogleSheetAPI.ReadData(methodName, tcName, "patient hospitalized"), wasPatientHospitalized, driver);
		
		SeleniumUtils.highLightElement(furthrTrmntNeeded, driver);
		String frthrTrmnt = furthrTrmntNeeded.getText();
		TestUtils.compareText(frthrTrmnt, GoogleSheetAPI.ReadData(methodName, tcName, "further treatment"), furthrTrmntNeeded, driver);
		
		SeleniumUtils.highLightElement(subjectToSustain, driver);
		String subtosus = subjectToSustain.getText();
		TestUtils.compareText(subtosus, GoogleSheetAPI.ReadData(methodName, tcName, "subject to sustain"), subjectToSustain, driver);
		
		SeleniumUtils.highLightElement(ableToDoSameWork, driver);
		String ableToDo = ableToDoSameWork.getText();
		TestUtils.compareText(ableToDo, GoogleSheetAPI.ReadData(methodName, tcName, "able to do same type of work"), ableToDoSameWork, driver);
		
		SeleniumUtils.highLightElement(nameOfPhysician, driver);
		String nameofphys = nameOfPhysician.getText();
		TestUtils.compareText(nameofphys, GoogleSheetAPI.ReadData(methodName, tcName, "name of physician"), nameOfPhysician, driver);
		
		SeleniumUtils.highLightElement(medAddress, driver);
		String medadd = medAddress.getText();
		TestUtils.compareText(medadd, GoogleSheetAPI.ReadData(methodName, tcName, "patient address"), medAddress, driver);
		
		SeleniumUtils.highLightElement(medZip, driver);
		String medzip = medZip.getText();
		TestUtils.compareText(medzip, GoogleSheetAPI.ReadData(methodName, tcName, "patient zipcode"), medZip, driver);
	}
}
