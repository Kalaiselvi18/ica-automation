/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Aartheeswaran
 * Created date: Jan 22, 2018
 * Last Edited by: Aartheeswaran...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form108Page extends DriverClass {
	
	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public By element() {
		By ul=By.xpath("//div[1]/div[1]/div[1]/b[contains(.,'RECOMMENDED AVERAGE MONTHLY WAGE CALCULATION OF CARRIER')]");
		return ul;
	}
	
	@FindBy(xpath = "//div[1]/div[1]/div[1]/b[contains(.,'RECOMMENDED AVERAGE MONTHLY WAGE CALCULATION OF CARRIER')]")
	public WebElement ica108Verify;
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;


	@FindBy(xpath = "//th/label[contains(.,'ICA Case No:')]/following::td[1]/div[1]/input[1]")
	public WebElement icaCaseNo;

	@FindBy(xpath = "//tr/td[7]/div/input")
	public WebElement averageMonthlyWage;	
	
	public Form108Page(WebDriver driver){
		 super(driver);
	 }
	
	
	
	
	
public void fillingForm108Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	SeleniumUtils.presenceOfElement(driver, community());
	
	SeleniumUtils.switchToFrame(driver, frameswitch);
	
	generator.childReport("Signature frame switched");
	
	SeleniumUtils.presenceOfElement(driver, element());
	SeleniumUtils.highLightElement(ica108Verify, driver);
	generator.childReport("ICA form name verified");
	
	SeleniumUtils.highLightElement(icaCaseNo, driver);
	icaCaseNo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "ICA Case No"));
	generator.childReport("ICA Case No Entered");
	
	SeleniumUtils.highLightElement(averageMonthlyWage, driver);
	averageMonthlyWage.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Average Monthly Wage"));
	generator.childReport("Average Monthly Wage Entered");
	
	SeleniumUtils.highLightElement(submitbutton, driver);
	submitbutton.click();
	generator.childReport("submitbutton entered");
	
	driver.switchTo().defaultContent();
	
	SeleniumUtils.presenceOfElement(driver, community());
	
	SeleniumUtils.switchToFrame(driver, frameswitch);
	
	SeleniumUtils.presenceOfElement(driver, verify());
	SeleniumUtils.highLightElement(verifysuccess, driver);
	
	
	
	
}
}
