/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 17-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form122Page extends DriverClass{
	
	@FindBy(xpath = "//center[contains(.,'REQUEST TO LEAVE THE STATE')]")
	public WebElement ica122Verify;
	
	public By element() {
		By open = By.xpath("//center[contains(.,'REQUEST TO LEAVE THE STATE')]");
		return open;
	}
	
	@FindBy(xpath = "//label[text()='Claimant First Name']/following::input[1]")
	public WebElement claimantfirstname;
	
	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::input[1]")
	public WebElement claimantlastname;
	
	@FindBy(xpath = "//label[text()='ICA Claim #:']/following::input[1]")
	public WebElement icaclaimno;
	
	@FindBy(xpath = "//label[text()='Date of Injury:']/following::a[1]")
	public WebElement dateofinjury;
	
	@FindBy(xpath = "//label[text()='Reason for Leaving the State:']/following::textarea[1]")
	public WebElement reasondetails;
	
	@FindBy(xpath = "//label[text()='Reason for Leaving the State:']/following::a[1]")
	public WebElement leavingon;
	
	@FindBy(xpath = "//label[text()='Reason for Leaving the State:']/following::a[2]")
	public WebElement returningon;
	
	@FindBy(xpath = "//h3[text()='Out of State Details']/following::textarea[1]")
	public WebElement outaddress;
	
	@FindBy(xpath = "//h3[text()='Out of State Details']/following::input[1]")
	public WebElement outcity;
	
	@FindBy(xpath = "//h3[text()='Out of State Details']/following::input[2]")
	public WebElement outstate;
	
	@FindBy(xpath = "//h3[text()='Out of State Details']/following::input[3]")
	public WebElement outzipcode;
	
	@FindBy(xpath = "//h3[text()='Out of State Details']/following::input[4]")
	public WebElement outphone;
	
	@FindBy(xpath = "//h3[text()='Provider Details']/following::input[1]")
	public WebElement providername;
	
	@FindBy(xpath = "//h3[text()='Provider Details']/following::textarea[1]")
	public WebElement provideraddress;
	
	@FindBy(xpath = "//h3[text()='Provider Details']/following::textarea[2]")
	public WebElement providercity;
	
	@FindBy(xpath = "//h3[text()='Provider Details']/following::input[2]")
	public WebElement providerstate;
	
	@FindBy(xpath = "//h3[text()='Provider Details']/following::input[3]")
	public WebElement providerzipcode;
	
	@FindBy(xpath = "//h3[text()='Provider Details']/following::input[4]")
	public WebElement providerphone;
	
	public By frame() {
		By user=By.cssSelector("iframe[class='cke_wysiwyg_frame cke_reset']");
		return user;
	}
	
	@FindBy(css = "iframe[class='cke_wysiwyg_frame cke_reset']")
	public WebElement iframe;
	
	@FindBy(xpath = "html/body")
	public WebElement workerDescribe;
	
	@FindBy(xpath = "//h3[text()='Authentication Details']/following::a[16]")
	public WebElement authenticationdate;
	
	@FindBy(xpath = "//h3[text()='Authentication Details']/following::input[2]")
	public WebElement submitteremail;
	
	@FindBy(xpath = "//h3[text()='Authentication Details']/following::textarea[2]")
	public WebElement submitteraddress;
	
	@FindBy(xpath = "//h3[text()='Authentication Details']/following::input[3]")
	public WebElement authenticationcity;
	
	@FindBy(xpath = "//h3[text()='Authentication Details']/following::input[4]")
	public WebElement authenticationstate;
	
	@FindBy(xpath = "//h3[text()='Authentication Details']/following::input[5]")
	public WebElement authenticationzipcode;
	
	@FindBy(xpath = "//h3[text()='Authentication Details']/following::input[6]")
	public WebElement authenticationphone;
	
	
	@FindBy(css = "input[type='Submit']")
	public WebElement submit;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;
	

	public By community() {
		By user = By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}

	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	
	public Form122Page(WebDriver driver){
		 super(driver);
	 }
	
	public void fillingForm122(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica122Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(claimantfirstname, driver);
		claimantfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"));
		generator.childReport("claimantfirstname entered");
		
		SeleniumUtils.highLightElement(claimantlastname, driver);
		claimantlastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantlastname"));
		generator.childReport("claimantlastname entered");
		
		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");
		
		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");
		
		SeleniumUtils.highLightElement(reasondetails, driver);
		reasondetails.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "reasondetails"));
		generator.childReport("reasondetails entered");
		
		SeleniumUtils.highLightElement(leavingon, driver);
		leavingon.click();
		generator.childReport("leavingon entered");
		
		SeleniumUtils.highLightElement(returningon, driver);
		returningon.click();
		generator.childReport("returningon entered");
		
		SeleniumUtils.highLightElement(outaddress, driver);
		outaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outaddress"));
		generator.childReport("outaddress entered");
		
		SeleniumUtils.highLightElement(outcity, driver);
		outcity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outcity"));
		generator.childReport("outcity entered");
		
		SeleniumUtils.highLightElement(outstate, driver);
		outstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outstate"));
		generator.childReport("outstate entered");
		
		SeleniumUtils.highLightElement(outzipcode, driver);
		outzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outzipcode"));
		generator.childReport("outzipcode entered");
		
		SeleniumUtils.highLightElement(outphone, driver);
		outphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outphone"));
		generator.childReport("outphone entered");
		
		SeleniumUtils.highLightElement(providername, driver);
		providername.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providername"));
		generator.childReport("providername entered");
		
		SeleniumUtils.highLightElement(provideraddress, driver);
		provideraddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "provideraddress"));
		generator.childReport("provideraddress entered");
		
		SeleniumUtils.highLightElement(providercity, driver);
		providercity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providercity"));
		generator.childReport("providercity entered");
		
		SeleniumUtils.highLightElement(providerstate, driver);
		providerstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerstate"));
		generator.childReport("providerstate entered");
		
		SeleniumUtils.highLightElement(providerzipcode, driver);
		providerzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerzipcode"));
		generator.childReport("providerzipcode entered");
		
		SeleniumUtils.highLightElement(providerphone, driver);
		providerphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerphone"));
		generator.childReport("providerphone entered");
		
		
		
	SeleniumUtils.presenceOfElement(driver, frame());
		
		SeleniumUtils.switchToFrame(driver, iframe);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.highLightElement(authenticationdate, driver);
		authenticationdate.click();
		generator.childReport("authenticationdate entered");
		
		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");
		
		SeleniumUtils.highLightElement(submitteraddress, driver);
		submitteraddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteraddress"));
		generator.childReport("submitteraddress entered");
		
		SeleniumUtils.highLightElement(authenticationcity, driver);
		authenticationcity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationcity"));
		generator.childReport("authenticationcity entered");
		
		SeleniumUtils.highLightElement(authenticationstate, driver);
		authenticationstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationstate"));
		generator.childReport("authenticationstate entered");
		
		SeleniumUtils.highLightElement(authenticationzipcode, driver);
		authenticationzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationzipcode"));
		generator.childReport("authenticationzipcode entered");
		
		SeleniumUtils.highLightElement(authenticationphone, driver);
		authenticationphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationphone"));
		generator.childReport("authenticationphone entered");
		
		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);


}
	public void fillingForm122Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		generator.childReport("Signature frame switched");

		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica122Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(claimantfirstname, driver);
		claimantfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"));
		generator.childReport("claimantfirstname entered");
		
		SeleniumUtils.highLightElement(claimantlastname, driver);
		claimantlastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantlastname"));
		generator.childReport("claimantlastname entered");
		
		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");
		
		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");
		
		SeleniumUtils.highLightElement(reasondetails, driver);
		reasondetails.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "reasondetails"));
		generator.childReport("reasondetails entered");
		
		SeleniumUtils.highLightElement(leavingon, driver);
		leavingon.click();
		generator.childReport("leavingon entered");
		
		SeleniumUtils.highLightElement(returningon, driver);
		returningon.click();
		generator.childReport("returningon entered");
		
		SeleniumUtils.highLightElement(outaddress, driver);
		outaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outaddress"));
		generator.childReport("outaddress entered");
		
		SeleniumUtils.highLightElement(outcity, driver);
		outcity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outcity"));
		generator.childReport("outcity entered");
		
		SeleniumUtils.highLightElement(outstate, driver);
		outstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outstate"));
		generator.childReport("outstate entered");
		
		SeleniumUtils.highLightElement(outzipcode, driver);
		outzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outzipcode"));
		generator.childReport("outzipcode entered");
		
		SeleniumUtils.highLightElement(outphone, driver);
		outphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "outphone"));
		generator.childReport("outphone entered");
		
		SeleniumUtils.highLightElement(providername, driver);
		providername.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providername"));
		generator.childReport("providername entered");
		
		SeleniumUtils.highLightElement(provideraddress, driver);
		provideraddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "provideraddress"));
		generator.childReport("provideraddress entered");
		
		SeleniumUtils.highLightElement(providercity, driver);
		providercity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providercity"));
		generator.childReport("providercity entered");
		
		SeleniumUtils.highLightElement(providerstate, driver);
		providerstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerstate"));
		generator.childReport("providerstate entered");
		
		SeleniumUtils.highLightElement(providerzipcode, driver);
		providerzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerzipcode"));
		generator.childReport("providerzipcode entered");
		
		SeleniumUtils.highLightElement(providerphone, driver);
		providerphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerphone"));
		generator.childReport("providerphone entered");
		
		
		
	SeleniumUtils.presenceOfElement(driver, frame());
		
		SeleniumUtils.switchToFrame(driver, iframe);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.highLightElement(authenticationdate, driver);
		authenticationdate.click();
		generator.childReport("authenticationdate entered");
		
		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");
		
		SeleniumUtils.highLightElement(submitteraddress, driver);
		submitteraddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteraddress"));
		generator.childReport("submitteraddress entered");
		
		SeleniumUtils.highLightElement(authenticationcity, driver);
		authenticationcity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationcity"));
		generator.childReport("authenticationcity entered");
		
		SeleniumUtils.highLightElement(authenticationstate, driver);
		authenticationstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationstate"));
		generator.childReport("authenticationstate entered");
		
		SeleniumUtils.highLightElement(authenticationzipcode, driver);
		authenticationzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationzipcode"));
		generator.childReport("authenticationzipcode entered");
		
		SeleniumUtils.highLightElement(authenticationphone, driver);
		authenticationphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationphone"));
		generator.childReport("authenticationphone entered");
		
		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");
		
		driver.switchTo().defaultContent();

		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);


}
}