package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document446Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document446Page(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//div[1]/span[text()='Defendant Employer']/following::span[2]")
	public WebElement defendentEmployer;
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim No.']/following::span[2]")
	public WebElement icaClaimNo;
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim No.']/following::span[20]")
	public WebElement defendentFirstName;
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim No.']/following::span[32]")
	public WebElement defendentLastName;
	
	@FindBy(xpath = "//div[1]/span[text()='Social Security Number']/following::span[2]")
	public WebElement socialSecurityNo;
	
	@FindBy(xpath = "//div[1]/span[text()='Person Requesting Hearing']/following::span[2]")
	public WebElement personRequestingHearing;
	
	@FindBy(xpath = "//div[1]/span[text()='State Reason for the Request:']/following::span[2]")
	public WebElement requestReasonState;
	
	@FindBy(xpath = "//div[1]/span[text()='Hearing Request at City or Town of:']/following::span[2]")
	public WebElement hearingRequestCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Estimated Length of Hearing']/following::span[2]")
	public WebElement estimatedLengthHearing;
	
	@FindBy(xpath = "//div[1]/span[text()='Signature of person requesting hearing']/following::span[1]")
	public WebElement signatureOfPerson;
	
	@FindBy(xpath = "//div[1]/span[text()='Address']/following::span[2]")
	public WebElement authenticationAddress;
	
	@FindBy(xpath = "//div[1]/span[text()='City']/following::span[2]")
	public WebElement authenticationCity;
	
	@FindBy(xpath = "//div[1]/span[text()='State']/following::span[2]")
	public WebElement authenticationState;
	
	@FindBy(xpath = "//div[1]/span[text()='Zip']/following::span[2]")
	public WebElement authenticationZip;
	
	@FindBy(xpath = "//div[1]/span[text()='Telephone No.']/following::span[2]")
	public WebElement authenticationTelephone;
	
	
	
public void verify446Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.highLightElement(defendentEmployer, driver);
		String dfndntEmp = defendentEmployer.getText();
		TestUtils.compareText(dfndntEmp, GoogleSheetAPI.ReadData(methodName, tcName, "defendentEmployer"), defendentEmployer, driver);
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		String icaClaim = icaClaimNo.getText();
		TestUtils.compareText(icaClaim, GoogleSheetAPI.ReadData(methodName, tcName, "defendentICAClaimNo"), icaClaimNo, driver);
		
		SeleniumUtils.highLightElement(defendentFirstName, driver);
		String dfndntFirstName = defendentFirstName.getText();
		TestUtils.compareText(dfndntFirstName, GoogleSheetAPI.ReadData(methodName, tcName, "defendentFirstName"), defendentFirstName, driver);
		
		SeleniumUtils.highLightElement(defendentLastName, driver);
		String dfndntLastName = defendentLastName.getText();
		TestUtils.compareText(dfndntLastName, GoogleSheetAPI.ReadData(methodName, tcName, "defendent Last Name"), defendentLastName, driver);
		
		SeleniumUtils.highLightElement(socialSecurityNo, driver);
		String socialSecNo = socialSecurityNo.getText();
		TestUtils.compareText(socialSecNo, GoogleSheetAPI.ReadData(methodName, tcName, "defendentSociaSecurity"), socialSecurityNo, driver);
		
		SeleniumUtils.highLightElement(personRequestingHearing, driver);
		String reqHearing = personRequestingHearing.getText();
		TestUtils.compareText(reqHearing, GoogleSheetAPI.ReadData(methodName, tcName, "requestHearing"), personRequestingHearing, driver);
		
		SeleniumUtils.highLightElement(requestReasonState, driver);
		String reqReasonStat = requestReasonState.getText();
		TestUtils.compareText(reqReasonStat, GoogleSheetAPI.ReadData(methodName, tcName, "requestStateReason"), requestReasonState, driver);
		
		SeleniumUtils.highLightElement(hearingRequestCity, driver);
		String hearingReqCty = hearingRequestCity.getText();
		TestUtils.compareText(hearingReqCty, GoogleSheetAPI.ReadData(methodName, tcName, "requestCity"), hearingRequestCity, driver);
		
		SeleniumUtils.highLightElement(estimatedLengthHearing, driver);
		String estLength = estimatedLengthHearing.getText();
		TestUtils.compareText(estLength, GoogleSheetAPI.ReadData(methodName, tcName, "requestEstimation"), estimatedLengthHearing, driver);
		
		SeleniumUtils.highLightElement(signatureOfPerson, driver);
		String signOfPerson = signatureOfPerson.getText();
		TestUtils.compareText(signOfPerson, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationSignature"), signatureOfPerson, driver);
		
		SeleniumUtils.highLightElement(authenticationAddress, driver);
		String authAddress = authenticationAddress.getText();
		TestUtils.compareText(authAddress, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationAddress"), authenticationAddress, driver);
		
		SeleniumUtils.highLightElement(authenticationCity, driver);
		String authCity = authenticationCity.getText();
		TestUtils.compareText(authCity, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationCity"), authenticationCity, driver);
		
		SeleniumUtils.highLightElement(authenticationState, driver);
		String authState = authenticationState.getText();
		TestUtils.compareText(authState, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationState"), authenticationState, driver);
		
		SeleniumUtils.highLightElement(authenticationZip, driver);
		String authZip = authenticationZip.getText();
		TestUtils.compareText(authZip, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationZipCode"), authenticationZip, driver);
		
		SeleniumUtils.highLightElement(authenticationTelephone, driver);
		String authTelephone = authenticationTelephone.getText();
		TestUtils.compareText(authTelephone, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationTelephone"), authenticationTelephone, driver);
		
			
		
}

}
