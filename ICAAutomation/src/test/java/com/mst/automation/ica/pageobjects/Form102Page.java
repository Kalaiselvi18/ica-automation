/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Aartheeswaran Created date: Dec 4, 2017 Last Edited by: Aartheeswaran
 *         Last Edited date: Dec 4, 2017 Description:
 *
 */
public class Form102Page extends DriverClass {

	// Worker's Report Div's attributes following:

	@FindBy(xpath = "//h2[contains(.,'REPORT OF INJURY')]")
	public WebElement ica102Verify;

	@FindBy(xpath = "//label[text()='First Name']/following::input[1]")
	public WebElement workerFirstName;

	@FindBy(xpath = "//label[text()='Last Name']/following::input[1]")
	public WebElement workerLastName;

	@FindBy(xpath = "//label[text()='Social Security No.']/following::input[1]")
	public WebElement socialSecurityNumber;

	@FindBy(xpath = "//label[text()='Social Security No.']/following::input[2]")
	public WebElement workerPhoneNo;

	@FindBy(xpath = "//label[text()='Address']/following::textarea[1]")
	public WebElement workerAddress;

	@FindBy(xpath = "//label[text()='City']/following::input[1]")
	public WebElement workerCity;

	@FindBy(xpath = "//label[text()='Social Security No.']/following::input[4]")
	public WebElement workerState;

	@FindBy(xpath = "//label[text()='Social Security No.']/following::input[5]")
	public WebElement workerZipCode;

	@FindBy(xpath = "//label[text()='Date of Birth']/following::a[1]")
	public WebElement workerDateOfBirth;

	@FindBy(xpath = "//label[text()='Sex']/following::select[1]")
	public WebElement workerSex;

	@FindBy(xpath = "//label[text()='Sex']/following::select[1]")
	public WebElement workerMaritalStatus;

	@FindBy(xpath = "//label[text()='Occupation When Injured']/following::input[1]")
	public WebElement workerOccupationWhenInjured;

	@FindBy(xpath = "//label[text()='Time of Injury']/following::input[1]")
	public WebElement timeofinjury;

	@FindBy(xpath = "//label[text()='Occupation When Injured']/following::a[1]")
	public WebElement workerDateOfInjury;

	@FindBy(xpath = "//label[text()='Employer']/following::input[1]")
	public WebElement workerEmployer;

	@FindBy(xpath = "//label[text()='Employer']/following::textarea[1]")
	public WebElement workerOfficeAddress;

	@FindBy(xpath = "//label[text()='Employer']/following::input[4]")
	public WebElement workerCity1;

	@FindBy(xpath = "//label[text()='Employer']/following::input[5]")
	public WebElement workerState1;

	@FindBy(xpath = "//label[text()='Employer']/following::input[6]")
	public WebElement workerZipCode1;

	@FindBy(xpath = "//label[text()='Employer']/following::textarea[2]")
	public WebElement workerMailingAddress;

	@FindBy(xpath = "html/body")
	public WebElement workerDescribe;
	
	@FindBy(xpath = "//label[text()='Date of Injury']/following::input[1]")
    public WebElement workerDateOfInjuryEnter;
	
	@FindBy(xpath = "//label[text()='Date of Birth']/following::input[1]")
    public WebElement workerDateOfBirthEnter;

	public static String form102LastName;

	public By toWindow() {
		By win = By.xpath("//label[text()='Date of Signing']/following::a[1]");
		return win;
	}

	@FindBy(xpath = "//label[text()='Date of Signing']/following::a[1]")
	public WebElement workerDateOfSigning;

	// Physician's Initial Report Div's attributes following:

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::a[1]")
	public WebElement dateFirstTreatment;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::a[2]")
	public WebElement hour;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[1]")
	public WebElement location;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::textarea[1]")
	public WebElement complaintsPhysicalFindings;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::textarea[5]")
	public WebElement describeTreatmentGivenByYou;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[5]")
	public WebElement wereXRaysTaken;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[6]")
	public WebElement wasLaboratoryWorkDone;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[7]")
	public WebElement wasPatientHospitalized;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[8]")
	public WebElement isFurtherTreatmentNeeded;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[9]")
	public WebElement subjectToSustainPermImpairment;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[10]")
	public WebElement ableToDoSameTypeOfWork;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::input[16]")
	public WebElement nameOfPhysician;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::textarea[9]")
	public WebElement patientAddress;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::input[18]")
	public WebElement patientZipCode;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::input[19]")
	public WebElement patientPhone;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::select[12]")
	public WebElement professionalCorp;

	@FindBy(xpath = "//h3[contains(.,'INITIAL REPORT')]/following::a[12]")
	public WebElement dateOfThisReport;

	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;

	@FindBy(css = "iframe[class='cke_wysiwyg_frame cke_reset']")
	public WebElement iframe;

	public By iframeVerify() {
		By frame = By.cssSelector("iframe[class='cke_wysiwyg_frame cke_reset']");
		return frame;
	}

	public By element() {
		By ul = By.xpath("//h2[contains(.,'REPORT OF INJURY')]");
		return ul;
	}

	public By verify() {
		By ver = By.xpath("//td[contains(.,'Your document')]");
		return ver;
	}

	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifySuccess;


	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	@FindBy(xpath = "//label[text()='Policy No.']/following::input[1]")
    public WebElement workerPolicyNumber;



	public Form102Page(WebDriver driver) {
		super(driver);
	}

	public By community() {
		By user = By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}

	// To file the 102 form

	public void fillingForm102(String methodName, String tcName, ReportGenerator generator) throws Exception {

		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica102Verify, driver);
		generator.childReport("ICA form name verified");

		SeleniumUtils.highLightElement(workerFirstName, driver);
		workerFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker first name"));
		generator.childReport("Worker First name entered");

		SeleniumUtils.highLightElement(workerLastName, driver);
		String lastName = GoogleSheetAPI.ReadData(methodName, tcName, "worker last name");

		if (lastName.equals("today")) {

			String date = CommonUtils.returnDateNumber();
			form102LastName = "Automation" + date;
			workerLastName.sendKeys(form102LastName);
		} else
			workerLastName.sendKeys(lastName);

		generator.childReport("Worker last name entered");

		SeleniumUtils.highLightElement(socialSecurityNumber, driver);
		socialSecurityNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "social security number"));
		generator.childReport("social security number entered");

		SeleniumUtils.highLightElement(workerPhoneNo, driver);
		workerPhoneNo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker phone number"));
		generator.childReport("worker phone number entered");

		SeleniumUtils.highLightElement(workerAddress, driver);
		workerAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker address"));
		generator.childReport("worker address entered");

		SeleniumUtils.highLightElement(workerCity, driver);
		workerCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker city"));
		generator.childReport("worker city entered");

		SeleniumUtils.highLightElement(workerState, driver);
		workerState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker state"));
		generator.childReport("worker state entered");

		SeleniumUtils.highLightElement(workerZipCode, driver);
		workerZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker zipcode"));
		generator.childReport("worker zipcode entered");

		if(methodName.contains("ClaimMatchLogic")) {
	        SeleniumUtils.highLightElement(workerDateOfBirthEnter, driver);
	        workerDateOfBirthEnter.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker dob"));
	        }
	        else {
	            SeleniumUtils.highLightElement(workerDateOfBirth, driver);
	            workerDateOfBirth.click();
	        }
        generator.childReport("worker date of birth entered");  

		SeleniumUtils.highLightElement(workerOccupationWhenInjured, driver);
		workerOccupationWhenInjured.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker occupation"));
		generator.childReport("worker occupation when injured entered");

		SeleniumUtils.highLightElement(timeofinjury, driver);
		timeofinjury.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "timeofinjury"));
		generator.childReport("timeofinjury entered");

		if(methodName.contains("ClaimMatchLogic")||methodName.contains("CarrierAccountNotMapped")) {
            SeleniumUtils.highLightElement(workerDateOfInjuryEnter, driver);
            workerDateOfInjuryEnter.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker doi"));
        }
        else {
               SeleniumUtils.highLightElement(workerDateOfInjury, driver);
               workerDateOfInjury.click();
        }

        generator.childReport("worker date of injury entered");  

		SeleniumUtils.highLightElement(workerEmployer, driver);
		workerEmployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Employer"));
		generator.childReport("Employer entered");

		SeleniumUtils.highLightElement(workerOfficeAddress, driver);
		workerOfficeAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker office address"));
		generator.childReport("worker office address entered");

		SeleniumUtils.highLightElement(workerCity1, driver);
		workerCity1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker city1"));
		generator.childReport("worker city 1 entered");

		SeleniumUtils.highLightElement(workerState1, driver);
		workerState1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker state1"));
		generator.childReport("worker state 1 entered");

		SeleniumUtils.highLightElement(workerZipCode1, driver);
		workerZipCode1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker zipcode1"));
		generator.childReport("worker zipcode 1 entered");

		SeleniumUtils.highLightElement(workerMailingAddress, driver);
		workerMailingAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker mailing address"));
		generator.childReport("worker mailing address entered");
		
		if(methodName.contains("CarrierAccountMappedUsingPolicyNo")||methodName.contains("CarrierAccountNotMappedUsingPolicyNo")) {
            SeleniumUtils.highLightElement(workerPolicyNumber, driver);
            workerPolicyNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "policy no"));
            generator.childReport("Policy number entered");
        }  

		SeleniumUtils.presenceOfElement(driver, iframeVerify());

		SeleniumUtils.switchToFrame(driver, iframe);

		generator.childReport("Signature frame switched");

		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker describe"));
		generator.childReport("worker describe entered");

		driver.switchTo().defaultContent();

		SeleniumUtils.presenceOfElement(driver, toWindow());

		SeleniumUtils.highLightElement(workerDateOfSigning, driver);
		workerDateOfSigning.click();
		generator.childReport("worker date of signing entered");

		SeleniumUtils.highLightElement(dateFirstTreatment, driver);
		dateFirstTreatment.click();
		generator.childReport("date first treatment entered");

		SeleniumUtils.highLightElement(hour, driver);
		hour.click();
		generator.childReport("hour entered");

		SeleniumUtils.dropDown(location, GoogleSheetAPI.ReadData(methodName, tcName, "location"));
		generator.childReport("location selected");

		SeleniumUtils.highLightElement(complaintsPhysicalFindings, driver);
		complaintsPhysicalFindings.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "complaint physical findings"));
		generator.childReport("Complaint Physical Findings entered");

		SeleniumUtils.highLightElement(describeTreatmentGivenByYou, driver);
		describeTreatmentGivenByYou.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "describe treatment"));
		generator.childReport("Describe treatment given by you entered");

		SeleniumUtils.dropDown(wereXRaysTaken, GoogleSheetAPI.ReadData(methodName, tcName, "were xrays taken"));
		generator.childReport("were xrays taken selected");

		SeleniumUtils.dropDown(wasLaboratoryWorkDone, GoogleSheetAPI.ReadData(methodName, tcName, "laboratory work"));
		generator.childReport("was laboratory work done selected");

		SeleniumUtils.dropDown(wasPatientHospitalized,
				GoogleSheetAPI.ReadData(methodName, tcName, "patient hospitalized"));
		generator.childReport("was patient hospitalized selected");

		SeleniumUtils.dropDown(isFurtherTreatmentNeeded,
				GoogleSheetAPI.ReadData(methodName, tcName, "further treatment"));
		generator.childReport("is further treatment needed selected");

		SeleniumUtils.dropDown(subjectToSustainPermImpairment,
				GoogleSheetAPI.ReadData(methodName, tcName, "subject to sustain"));
		generator.childReport("subject to sustain perm. impairment selected");

		SeleniumUtils.dropDown(ableToDoSameTypeOfWork,
				GoogleSheetAPI.ReadData(methodName, tcName, "able to do same type of work"));
		generator.childReport("able to do same type of work selected");

		SeleniumUtils.highLightElement(nameOfPhysician, driver);
		nameOfPhysician.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "name of physician"));
		generator.childReport("Name of physician entered");

		SeleniumUtils.highLightElement(patientAddress, driver);
		patientAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "patient address"));
		generator.childReport("patient address entered");

		SeleniumUtils.highLightElement(patientZipCode, driver);
		patientZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "patient zipcode"));
		generator.childReport("patient Zipcode entered");

		SeleniumUtils.highLightElement(patientPhone, driver);
		patientPhone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "patient phone"));
		generator.childReport("patient phone entered");

		SeleniumUtils.dropDown(professionalCorp, GoogleSheetAPI.ReadData(methodName, tcName, "professional corp"));
		generator.childReport("professional corp selected");

		SeleniumUtils.highLightElement(dateOfThisReport, driver);
		dateOfThisReport.click();
		generator.childReport("date of this report entered");

		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");

		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);

	}

	public void fillingForm102Community(String methodName, String tcName, ReportGenerator generator) throws Exception {

		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		generator.childReport("Signature frame switched");

		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica102Verify, driver);
		generator.childReport("ICA form name verified");

		SeleniumUtils.highLightElement(workerFirstName, driver);
		workerFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker first name"));
		generator.childReport("Worker First name entered");

		SeleniumUtils.highLightElement(workerLastName, driver);
		workerLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker last name"));
		generator.childReport("Worker last name entered");

		SeleniumUtils.highLightElement(socialSecurityNumber, driver);
		socialSecurityNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "social security number"));
		generator.childReport("social security number entered");

		SeleniumUtils.highLightElement(workerPhoneNo, driver);
		workerPhoneNo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker phone number"));
		generator.childReport("worker phone number entered");

		SeleniumUtils.highLightElement(workerAddress, driver);
		workerAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker address"));
		generator.childReport("worker address entered");

		SeleniumUtils.highLightElement(workerCity, driver);
		workerCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker city"));
		generator.childReport("worker city entered");

		SeleniumUtils.highLightElement(workerState, driver);
		workerState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker state"));
		generator.childReport("worker state entered");

		SeleniumUtils.highLightElement(workerZipCode, driver);
		workerZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker zipcode"));
		generator.childReport("worker zipcode entered");

		SeleniumUtils.highLightElement(workerDateOfBirth, driver);
		workerDateOfBirth.click();
		generator.childReport("worker date of birth entered");

		/*
		 * SeleniumUtils.dropDown(workerSex, GoogleSheetAPI.ReadData(methodName, tcName,
		 * "worker sex")); generator.childReport("worker sex selected");
		 * 
		 * SeleniumUtils.dropDown(workerMaritalStatus,
		 * GoogleSheetAPI.ReadData(methodName, tcName, "worker marital status"));
		 * generator.childReport("worker marital status selected");
		 */

		SeleniumUtils.highLightElement(workerOccupationWhenInjured, driver);
		workerOccupationWhenInjured.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker occupation"));
		generator.childReport("worker occupation when injured entered");

		SeleniumUtils.highLightElement(timeofinjury, driver);
		timeofinjury.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "timeofinjury"));
		generator.childReport("timeofinjury entered");

		SeleniumUtils.highLightElement(workerDateOfInjury, driver);
		workerDateOfInjury.click();
		generator.childReport("worker date of injury entered");

		SeleniumUtils.highLightElement(workerEmployer, driver);
		workerEmployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Employer"));
		generator.childReport("Employer entered");

		SeleniumUtils.highLightElement(workerOfficeAddress, driver);
		workerOfficeAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker office address"));
		generator.childReport("worker office address entered");

		SeleniumUtils.highLightElement(workerCity1, driver);
		workerCity1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker city1"));
		generator.childReport("worker city 1 entered");

		SeleniumUtils.highLightElement(workerState1, driver);
		workerState1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker state1"));
		generator.childReport("worker state 1 entered");

		SeleniumUtils.highLightElement(workerZipCode1, driver);
		workerZipCode1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker zipcode1"));
		generator.childReport("worker zipcode 1 entered");

		SeleniumUtils.highLightElement(workerMailingAddress, driver);
		workerMailingAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker mailing address"));
		generator.childReport("worker mailing address entered");

		SeleniumUtils.presenceOfElement(driver, iframeVerify());

		SeleniumUtils.switchToFrame(driver, iframe);

		generator.childReport("Signature frame switched");

		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker describe"));
		generator.childReport("worker describe entered");

		driver.switchTo().defaultContent();

		SeleniumUtils.switchToFrame(driver, frameswitch);

		SeleniumUtils.presenceOfElement(driver, toWindow());

		SeleniumUtils.highLightElement(workerDateOfSigning, driver);
		workerDateOfSigning.click();
		generator.childReport("worker date of signing entered");

		SeleniumUtils.highLightElement(dateFirstTreatment, driver);
		dateFirstTreatment.click();
		generator.childReport("date first treatment entered");

		SeleniumUtils.highLightElement(hour, driver);
		hour.click();
		generator.childReport("hour entered");

		SeleniumUtils.dropDown(location, GoogleSheetAPI.ReadData(methodName, tcName, "location"));
		generator.childReport("location selected");

		SeleniumUtils.highLightElement(complaintsPhysicalFindings, driver);
		complaintsPhysicalFindings.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "complaint physical findings"));
		generator.childReport("Complaint Physical Findings entered");

		SeleniumUtils.highLightElement(describeTreatmentGivenByYou, driver);
		describeTreatmentGivenByYou.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "describe treatment"));
		generator.childReport("Describe treatment given by you entered");

		SeleniumUtils.dropDown(wereXRaysTaken, GoogleSheetAPI.ReadData(methodName, tcName, "were xrays taken"));
		generator.childReport("were xrays taken selected");

		SeleniumUtils.dropDown(wasLaboratoryWorkDone, GoogleSheetAPI.ReadData(methodName, tcName, "laboratory work"));
		generator.childReport("was laboratory work done selected");

		SeleniumUtils.dropDown(wasPatientHospitalized,
				GoogleSheetAPI.ReadData(methodName, tcName, "patient hospitalized"));
		generator.childReport("was patient hospitalized selected");

		SeleniumUtils.dropDown(isFurtherTreatmentNeeded,
				GoogleSheetAPI.ReadData(methodName, tcName, "further treatment"));
		generator.childReport("is further treatment needed selected");

		SeleniumUtils.dropDown(subjectToSustainPermImpairment,
				GoogleSheetAPI.ReadData(methodName, tcName, "subject to sustain"));
		generator.childReport("subject to sustain perm. impairment selected");

		SeleniumUtils.dropDown(ableToDoSameTypeOfWork,
				GoogleSheetAPI.ReadData(methodName, tcName, "able to do same type of work"));
		generator.childReport("able to do same type of work selected");

		SeleniumUtils.highLightElement(nameOfPhysician, driver);
		nameOfPhysician.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "name of physician"));
		generator.childReport("Name of physician entered");

		SeleniumUtils.highLightElement(patientAddress, driver);
		patientAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "patient address"));
		generator.childReport("patient address entered");

		SeleniumUtils.highLightElement(patientZipCode, driver);
		patientZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "patient zipcode"));
		generator.childReport("patient Zipcode entered");

		SeleniumUtils.highLightElement(patientPhone, driver);
		patientPhone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "patient phone"));
		generator.childReport("patient phone entered");

		SeleniumUtils.dropDown(professionalCorp, GoogleSheetAPI.ReadData(methodName, tcName, "professional corp"));
		generator.childReport("professional corp selected");

		SeleniumUtils.highLightElement(dateOfThisReport, driver);
		dateOfThisReport.click();
		generator.childReport("date of this report entered");

		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");

		driver.switchTo().defaultContent();

		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);

	}

	public void browserBack() throws InterruptedException {
		Thread.sleep(3000);
		driver.navigate().back();
	}

	public void reload(String methodName, String tcName) throws InterruptedException, IOException {
		
		SeleniumUtils.presenceOfElement(driver, iframeVerify());
		SeleniumUtils.switchToFrame(driver, iframe);
		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "worker describe"));
		driver.switchTo().defaultContent();
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();

		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);
	}
}
