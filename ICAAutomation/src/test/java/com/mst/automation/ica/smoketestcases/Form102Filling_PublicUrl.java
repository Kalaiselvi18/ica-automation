/**
 * 
 */
package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

/**
 * @author Aartheeswaran
 * Created date: Dec 5, 2017
 * Last Edited by: Aartheeswaran
 * Last Edited date: Dec 5, 2017
 * Description: 
 *
 */
public class Form102Filling_PublicUrl extends BaseTest {
@Test (groups = "Form Regression Suite")
	
	public void formfilling102() throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_102";
		
		reporter = new ReportGenerator(getbrowser(), className);
		page102 = loginPage.formUrl102();
		page102.fillingForm102(methodName, tcName, reporter);
		

	}

}
