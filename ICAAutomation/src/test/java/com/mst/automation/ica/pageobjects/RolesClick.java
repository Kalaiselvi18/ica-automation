package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To click on the roles
 *
 */

public class RolesClick extends DriverClass {

	@FindBy(css = "iframe[title='Creating the Role Hierarchy ~ Salesforce - Unlimited Edition']")
	public WebElement iframe;

	public By iframeVerify() {
		By frame = By.cssSelector("iframe[title='Creating the Role Hierarchy ~ Salesforce - Unlimited Edition']");
		return frame;
	}

	@FindBy(xpath = "//a[text()='Expand All']")
	public WebElement expandAll;

	@FindBy(xpath = "//a[text()='Claims Manager']/following::a[text()='Claims Processor']")
	public WebElement claimsProcessorRole;

	public By claimsProcessorRoleVerify() {
		By verifyRole = By.xpath("//a[text()='Claims Manager']/following::a[text()='Claims Processor']");
		return verifyRole;
	}

	@FindBy(xpath = "//span[text()='Industrial Commission of Arizona']")
	public WebElement industrialRole;

	@FindBy(xpath = "//span[text()='Industrial Commission of Arizona']/following::a[text()='Claims']")
	public WebElement claimsRole;

	@FindBy(xpath = "//a[text()='Claims']/following::a[text()='Admin']")
	public WebElement AdminRole;

	@FindBy(xpath = "//a[text()='Admin']/following::a[text()='BLS Manager']")
	public WebElement blsManagerRole;

	@FindBy(xpath = "//a[text()='BLS Manager']/following::a[text()='BLS Worker']")
	public WebElement blsWorkerRole;

	@FindBy(xpath = "//a[text()='BLS Worker']/following::a[text()='Claims Manager']")
	public WebElement claimsManagerRole;

	public RolesClick(WebDriver driver) {
		super(driver);
	}

	/*Check the roles to click on that appropriate roles*/
	public void roleCheck(String methodName, String tcName, ReportGenerator generator) throws Exception {

		SeleniumUtils.presenceOfElement(driver, iframeVerify());
		SeleniumUtils.switchToFrame(driver, iframe);
		generator.childReport("Switched Role frame");
		expandAll.click();
		generator.childReport("Expand All clicked");
		SeleniumUtils.presenceOfElement(driver, claimsProcessorRoleVerify());
		SeleniumUtils.highLightElement(industrialRole, driver);
		generator.childReport("Industrial Verified");
		SeleniumUtils.highLightElement(claimsRole, driver);
		generator.childReport("Claims Role Verified");
		SeleniumUtils.highLightElement(AdminRole, driver);
		generator.childReport("Admin Role Verified");
		SeleniumUtils.highLightElement(blsManagerRole, driver);
		generator.childReport("BLS Manager Role Verified");
		SeleniumUtils.highLightElement(blsWorkerRole, driver);
		generator.childReport("BLS Worker Role Verified");
		SeleniumUtils.highLightElement(claimsManagerRole, driver);
		generator.childReport("Claims Manager Role Verified");
		SeleniumUtils.highLightElement(claimsProcessorRole, driver);
		generator.childReport("Claims Processor Role Verified");

	}

}
