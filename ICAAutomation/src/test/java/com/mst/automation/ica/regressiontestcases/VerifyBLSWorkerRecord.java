/**
 * 
 */
package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 05-Dec-2017
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class VerifyBLSWorkerRecord extends BaseTest {
	
	@Test (groups = "Regression Suite", priority=2)
	@Parameters({ "userType" })
	
	public void blsWorkerRecord (String userType) throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_019";

		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");

		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.login(user, pwd, reporter);
		reporter.childReport("Login_Verify");
		blsRecordPage = homePage.verifyHomePage2();
		editPageClaims = blsRecordPage.verificationBlsWorker(methodName, tcName, reporter);
		editPageClaims.checkOccupation(methodName, tcName, reporter);
}
}
