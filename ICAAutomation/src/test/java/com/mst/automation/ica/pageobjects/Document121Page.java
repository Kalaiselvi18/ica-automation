package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document121Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document121Page(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant First Name']/following::span[2]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Last Name']/following::span[2]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim #:']/following::span[2]")
	public WebElement icaClaim;
	
	@FindBy(xpath = "//div[1]/span[text()='Reason for Requesting Change of Doctors']/following::span[2]")
	public WebElement reasonForRequesting;
	
	@FindBy(xpath = "//h3/button/span[text()='Provider Details From']/following::span[3]")
	public WebElement doctorName;
	
	@FindBy(xpath = "//h3/button/span[text()='Provider Details From']/following::span[9]")
	public WebElement doctorAddress;
	
	@FindBy(xpath = "//h3/button/span[text()='Provider Details From']/following::span[15]")
	public WebElement doctorCity;
	
	@FindBy(xpath = "//h3/button/span[text()='Provider Details From']/following::span[21]")
	public WebElement doctorState;
	
	@FindBy(xpath = "//h3/button/span[text()='Provider Details From']/following::span[27]")
	public WebElement doctorZip;
	
	@FindBy(xpath = "//h3/button/span[text()='Authentication Details']/following::span[2]")
	public WebElement signature;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[2]")
	public WebElement clmntAddress;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[6]")
	public WebElement clmntCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[10]")
	public WebElement clmntState;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[14]")
	public WebElement clmntZip;
	
	
	
public void verify121Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		String clmntFstName = claimantFirstName.getText();
		TestUtils.compareText(clmntFstName, GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"), claimantFirstName, driver);
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		String clmntLstName = claimantLastName.getText();
		TestUtils.compareText(clmntLstName, GoogleSheetAPI.ReadData(methodName, tcName, "claimantlastname"), claimantLastName, driver);
		
		SeleniumUtils.highLightElement(icaClaim, driver);
		String claimNo = icaClaim.getText();
		TestUtils.compareText(claimNo, GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"), icaClaim, driver);
		
		SeleniumUtils.highLightElement(reasonForRequesting, driver);
		String reqReason = reasonForRequesting.getText();
		TestUtils.compareText(reqReason, GoogleSheetAPI.ReadData(methodName, tcName, "reasondetails"), reasonForRequesting, driver);
		
		SeleniumUtils.highLightElement(doctorName, driver);
		String doctName = doctorName.getText();
		TestUtils.compareText(doctName, GoogleSheetAPI.ReadData(methodName, tcName, "providerdoctorname"), doctorName, driver);
		
		SeleniumUtils.highLightElement(doctorAddress, driver);
		String doctAdd = doctorAddress.getText();
		TestUtils.compareText(doctAdd, GoogleSheetAPI.ReadData(methodName, tcName, "provideraddress"), doctorAddress, driver);	
		
		SeleniumUtils.highLightElement(doctorCity, driver);
		String doctCty = doctorCity.getText();
		TestUtils.compareText(doctCty, GoogleSheetAPI.ReadData(methodName, tcName, "providercity"), doctorCity, driver);	
		
		SeleniumUtils.highLightElement(doctorState, driver);
		String doctStat = doctorState.getText();
		TestUtils.compareText(doctStat, GoogleSheetAPI.ReadData(methodName, tcName, "providerstate"), doctorState, driver);	
		
		SeleniumUtils.highLightElement(doctorZip, driver);
		String doctZip = doctorZip.getText();
		TestUtils.compareText(doctZip, GoogleSheetAPI.ReadData(methodName, tcName, "providerzipcode"), doctorZip, driver);	
		
		SeleniumUtils.highLightElement(signature, driver);
		String sign = signature.getText();
		TestUtils.compareText(sign, GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"), signature, driver);	
		
		SeleniumUtils.highLightElement(clmntAddress, driver);
		String clmAdd = clmntAddress.getText();
		TestUtils.compareText(clmAdd, GoogleSheetAPI.ReadData(methodName, tcName, "claimantaddress"), clmntAddress, driver);	
		
		SeleniumUtils.highLightElement(clmntCity, driver);
		String clmCty = clmntCity.getText();
		TestUtils.compareText(clmCty, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationcity"), clmntCity, driver);	
		
		SeleniumUtils.highLightElement(clmntState, driver);
		String clmStat = clmntState.getText();
		TestUtils.compareText(clmStat, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationstate"), clmntState, driver);	
		
		SeleniumUtils.highLightElement(clmntZip, driver);
		String clmZip = clmntZip.getText();
		TestUtils.compareText(clmZip, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationzipcode"), clmntZip, driver);	
		
		
}
	

}
