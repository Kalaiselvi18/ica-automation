package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;

public class ClaimTaskSelect extends DriverClass{
	
	@FindBy(xpath = "//h1/a/div/span[text()='Recently Viewed']")
	public WebElement listClick;
	
	public By listView() {
		By list = By.xpath("//li[2]/a/span[text()='All Open Claim Tasks']");
		return list;
	}
	
	@FindBy(xpath = "//li[2]/a/span[text()='All Open Claim Tasks']")
	public WebElement allServiceClick;
	
	public By assignByClaim() {
		By assign = By.xpath("//li[3]/a/div[text()='Assign by Claim No.']");
		return assign;
	}
	
	public By claimNumber() {
		By no = By.xpath("//label[text()='Enter Claim No.']");
		return no;
	}
	
	
	@FindBy(xpath = "//li[3]/a/div[text()='Assign by Claim No.']")
	public WebElement assignClick;
	
	@FindBy(xpath = "//label[text()='Enter Claim No.']/following::input[1]")
	public WebElement numberEnter;
	
	@FindBy(xpath = "//label[text()='Enter Claim No.']/following::input[2]")
	public WebElement submitClick;
	
	public By taskTable() {
		By tr = By.xpath("html/body/form/div[1]/div/div/div/div/div[1]/span/table");
		return tr;
	}
	
	String tableTask = "html/body/form/div[1]/div/div/div/div/div[1]/span/table";
	
/*	@FindBy(xpath = "html/body/form/div[1]/div/div/div/div/div[1]/span/table")
	public WebElement tableTask;*/
	
	@FindBy(css = "input[value=\"Assign Selected task\"]")
	public WebElement assignSelectedTaskClick;
	
	
	public By myTaskList() {
		By td = By.xpath("//h1/a/div/span[text()='My Tasks']");
		return td;
	}
	
	@FindBy(css = "//h1/a/div/span[text()='My Tasks']")
	public WebElement myTaskVerify;
	
	@FindBy(xpath = "//table/tbody/tr[1]/th/span/a")
	public WebElement firstRecord;
	
	public By firstRecordTable() {
		By tbody = By.xpath("//table/tbody/tr[1]/th/span/a");
		return tbody;
	}
	
	
	public By claimTask() {
		By th = By.xpath("//p[2]/span[text()='In Progress']");
		return th;
	}
	
	@FindBy(xpath = "//p[2]/span[text()='In Progress']")
	public WebElement inprogress;

	public ClaimTaskSelect(WebDriver driver) {
		super(driver);
	}
	
	public ClaimTaskObjectPage selectClaimTask(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		listClick.click();
		generator.childReport("List Element Found");
		
		SeleniumUtils.presenceOfElement(driver, listView());
		SeleniumUtils.highLightElement(allServiceClick, driver);
		allServiceClick.click();
		generator.childReport("Customer Service Clicked");
		
		SeleniumUtils.presenceOfElement(driver, assignByClaim());
		SeleniumUtils.highLightElement(assignClick, driver);
		assignClick.click();
		
		Thread.sleep(5000);
		
		SeleniumUtils.frameSwitch(0, driver);
		SeleniumUtils.presenceOfElement(driver, claimNumber());
		generator.childReport("Assign frame switched");
		
		generator.childReport("Claim Number Entered");
		SeleniumUtils.highLightElement(numberEnter, driver);
		
		if(methodName.contains("doc102PossibleMatchTaskOwn"))
			numberEnter.sendKeys(Document102Page.claimNumber);
		else
			numberEnter.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claim number"));
				
		submitClick.click();
		generator.childReport("Submit Button Clicked");
		
		SeleniumUtils.presenceOfElement(driver, taskTable());
		CommonUtils.claimCheckBoxTable(tableTask,driver);
		assignSelectedTaskClick.click();
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, myTaskList());
		SeleniumUtils.presenceOfElement(driver, firstRecordTable());
		SeleniumUtils.highLightElement(firstRecord, driver);
		firstRecord.click();
		
		SeleniumUtils.presenceOfElement(driver, claimTask());
		SeleniumUtils.highLightElement(inprogress, driver);
		
		return new ClaimTaskObjectPage(driver);

	}
}
