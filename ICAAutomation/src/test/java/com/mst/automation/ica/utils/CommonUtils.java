package com.mst.automation.ica.utils;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mst.automation.ica.customexception.CustomException;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This class contains the common methods which is used evaluate the string comparison,
 * convert the list to array, array to list, list comparison, array comparison etc.,
 */
public final class CommonUtils {
	
	private CommonUtils() {
		
	}
	
	/**
	 * This method is used to compare the two list values
	 * @param listOne
	 * @param listTwo
	 * @return
	 */
	public static List<String> compareLists(List<String>listOne, List<String>listTwo){
		
		List<String> resultList = new ArrayList<>();
		Collection<String> expected = new ArrayList<>(listOne);
		Collection<String> actual = new ArrayList<>(listTwo);
		
		List<String> expectedList = new ArrayList<>(expected);
		List<String> actualList = new ArrayList<>(actual);
		
		int max = expectedList.size() > actualList.size() ? expectedList.size() : actualList.size();
		
		if(!expected.equals(actual)){
			if(max == expectedList.size()){
				expectedList.removeAll(actual);
				resultList.addAll(expectedList);
			}
			else{
				actualList.removeAll(expected);
				resultList.addAll(actualList);
			}
		}
		return resultList;
	}	
	
	/**
	 * This method is used to convert the enum class to a list
	 * @param aEnum
	 * @return
	 */
	public static <T> List<String> convertEnumToList(Class<T> aEnum){
		return Arrays.asList(Arrays.toString(aEnum.getEnumConstants()).replaceAll("^.|.$","").split(","));
	}
	
	/**
	 * This method is used to convert as a list
	 * @param value
	 * @return
	 */
	public static List<String> toList(String value){
		List<String> values = Arrays.asList(value.split("~"));
		return values;
	}
	
	/**
	 * This method us used to sort the date in descending order.
	 * @param collection
	 * @return
	 */
	public static List<String> dateSortDescendingOrder(Collection<String> collection){
		List<String> values = new ArrayList<>(collection);
		Collections.sort(values, Collections.reverseOrder());
		return values;
	}

	public static boolean tableIteration(String elementID, String text,WebDriver driver) throws InterruptedException{
        try{       
            List<WebElement> profileNames = driver.findElements(By.cssSelector(elementID));
            for(WebElement profileName:profileNames) {
            	String actual = profileName.getText();
            	if(text.equals(actual)) {
            		SeleniumUtils.highLightElement(profileName, driver);
            		profileName.click();
            		break;
            	}
            }
        }
        catch(NoSuchElementException c){
            throw c;
        }
        return false;
    }

	public static boolean checkBoxTable(String elementID, String text,WebDriver driver) {
		
        try{
            WebElement table = driver.findElement(By.xpath(elementID));
            
            List<WebElement> rowsTable = table.findElements(By.xpath("tbody/..//tr/th[@scope='row']"));
            int rowsCount = rowsTable.size();
            boolean test = false;
            WebElement relationValue=null;
            for(int row=1; row<=rowsCount; row++){
                relationValue = driver.findElement(By.xpath("//tbody/..//tr["+row+"]/th[@scope='row']/span/a"));   
                if(text.equals(relationValue.getText())){
                    test = true;
                    driver.findElement(By.xpath("//tbody/..//tr["+row+"]/td[2]/span/span/label/span")).click();
                    return true;
                }
            }
            if(test==false){
                throw new CustomException("The given orientation value "+text+" is not matched with the fetched data " +relationValue);
          }
        }
        
        catch(NoSuchElementException c){
            throw c;
        }
        return false;
    }

	public static boolean claimCheckBoxTable(String elementID,WebDriver driver) {
		
        try{
            WebElement table = driver.findElement(By.xpath(elementID));
            
            List<WebElement> rowsTable = table.findElements(By.xpath("tbody/tr"));
            int rowsCount = rowsTable.size();
            driver.findElement(By.xpath("html/body/form/div[1]/div/div/div/div/div[1]/span/table/tbody/tr["+rowsCount+"]/td/input")).click(); 
        }
        
        catch(NoSuchElementException c){
            throw c;
        }
        return false;
    }
	
	
	public static boolean dropdownTable(String elementID, String text,WebDriver driver){
		
        try{
            WebElement table = driver.findElement(By.xpath(elementID));
            
            List<WebElement> rowsTable = table.findElements(By.xpath("tbody/..//tr/th[@scope='row']"));
            int rowsCount = rowsTable.size();
            boolean test = false;
            WebElement relationValue=null;
            for(int row=1; row<=rowsCount; row++){
                relationValue = driver.findElement(By.xpath("//tbody/..//tr["+row+"]/th[@scope='row']/span/a"));   
                if(text.equals(relationValue.getText())){
                    test = true;
                    driver.findElement(By.xpath("//tbody/..//tr["+row+"]/td[7]/span/div/a")).click();
                    return true;
                }
            }
            if(test==false){
                throw new CustomException("The given orientation value "+text+" is not matched with the fetched data " +relationValue);
          }
        }
        
        catch(NoSuchElementException c){
            throw c;
        }
        return false;
	}
	
	public static void clickOnLookup(WebDriver driver, String data, String lookupPath, String valueToSelectPath){

		driver.findElement(By
				.xpath(lookupPath))
				.click();
		String parentWindow = driver.getWindowHandle();
		Set<String> handles = driver.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWindow)) {
				driver.switchTo().window(windowHandle);
				driver.switchTo().frame("resultsFrame");

				WebElement table = driver.findElement(By.xpath(valueToSelectPath));

				List<WebElement> rows = table.findElements(By.tagName("tr"));
				int rowscount = rows.size();

				for (int i = 0; i < rowscount; i++) {
					List<WebElement> columns = rows.get(i).findElements(By.tagName("th"));
					int columnscount = columns.size();

					for (int j = 0; j < columnscount; j++) {
						String var1 = columns.get(j).getText();

						if (data.equals(var1)) {
							columns.get(j).findElement(By.tagName("a")).click();
							i = rowscount + 1;
							j = columnscount + 1;
						}
					}
				}
			}
		}
		driver.switchTo().window(parentWindow);
	}
	
	public static boolean tableIVerify(String elementID, String text,WebDriver driver) throws InterruptedException{
        try{       
            List<WebElement> profileNames = driver.findElements(By.cssSelector(elementID));
            for(WebElement profileName:profileNames) {
            	String actual = profileName.getText();
            	if(text.equals(actual)) {
            		SeleniumUtils.highLightElement(profileName, driver);
            		break;
            	}
            }
        }
        catch(NoSuchElementException c){
            throw c;
        }
        return false;
    }
	
    public static String returnDateNumber() {
        LocalDateTime localDate = LocalDateTime.now();
        String date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate);
        date = date.replaceAll("-","");
        return date;
    }
 
	 public static String returnDate() {
	        LocalDate localDate = LocalDate.now();
	        String date = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(localDate);
	        return date;
	 }

}
