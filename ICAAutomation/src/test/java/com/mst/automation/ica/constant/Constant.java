package com.mst.automation.ica.constant;
/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: It contains the constant values which 
 * is used through out the project.
 */
public class Constant {
	
	private Constant() {
		
	}
	public static final String LINUXCHROMEDRIVER = "/opt/chromedriver";
	public static final String LINUXFIREFOXDRIVER = "/opt/geckodriver";
	//Chrome driver location in the Local in the same project
	//public static final String LINUXCHROMEDRIVER = "/src/test/resources/drivers/chromedriver";
	//public static final String LINUXFIREFOXDRIVER = "/src/test/resources/drivers/geckodriver";
	//chrome driver location in the server
	//public static final String LINUXCHROMEDRIVER = System.getenv("CHROME_DRIVER");
	//public static final String LINUXFIREFOXDRIVER = System.getenv("FIREFOX_DRIVER");
	// Binary location of chrome in the server
	//public static final String CHROMEBINARY = System.getenv("CHROME_BINARY");
	public static final String CHROMEBINARY = "/opt/chromedriver";
	public static final String FIREFOXBINARY = System.getenv("FIREFOX_BINARY");
	    public static final String WINDOWSCHROMEDRIVER = "src/test/resources/drivers/chromedriver.exe";
	    public static final String WINDOWSFIREFOXDRIVER = "src/test/resources/drivers/geckodriver.exe";
	    
	    public static final String PROPERTYFILEPATH ="src/test/resources/test.properties";
	    public static final String REPORTPATH= "src/test/resources/report/report.html";
	    public static final String SCREENSHOTPATH = "src/test/resources/report/screenshot/";
	    public static final String datafilePath = "src/test/resources/Excels/Testdata.xlsx";
	    public static final String UPLOADFILE = "document/ica.exe";
	/*    
	//chrome driver location in the server
	public static final String chromeDriver = System.getenv("CHROME_DRIVER");

	// Binary location of chrome in the server
	public static final String chromeBinary = System.getenv("CHROME_BINARY");
	
    public static final String CHROMEDRIVER = System.getenv("CHROME_DRIVER");//"src/test/resources/drivers/chromedriver.exe";
    public static final String FIREFOXDRIVER = "src/test/resources/drivers/geckodriver.exe";
    public static final String PROPERTYFILEPATH ="src/test/resources/test.properties";
    public static final String REPORTPATH= "src/test/resources/report/report.html";
    public static final String SCREENSHOTPATH = "src/test/resources/report/screenshot/";
    public static final String datafilePath = "src/test/resources/Excels/Testdata.xlsx";
    public static final String UPLOADFILE = "document/ica.exe";*/
}


