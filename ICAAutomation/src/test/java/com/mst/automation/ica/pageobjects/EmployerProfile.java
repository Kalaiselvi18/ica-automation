package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Aartheeswaran
 * Created date: 10/26/2017 
 * Last Edited by: Aartheeswaran
 * Last Edited date: 10/26/2017 
 * Description: To check permissions for Employer Profile
 *
 */

public class EmployerProfile extends DriverClass {

	@FindBy(xpath = "//label[contains(.,'User License')]/following::td[1]")
	public WebElement userProfile;

	public By element() {
		By ul = By.xpath("//label[contains(.,'User License')]/following::td[1]");
		return ul;
	}

	@FindBy(xpath = "//h3[contains(.,'Standard Object Permissions')]")
	public WebElement standardObjectPermission;

	@FindBy(xpath = "//th[contains(.,'Cases')]")
	public WebElement soClaims;

	@FindBy(xpath = "//th[contains(.,'Cases')]/following::tr[1]")
	public WebElement claimsPermission;

	public By claimsPermissionVerify() {
		By verifyPermission = By.xpath("//th[contains(.,'Cases')]/following::tr[1]");
		return verifyPermission;
	}

	@FindBy(css = "iframe[title='Profile: Community-Employer ~ Salesforce - Unlimited Edition']")
	public WebElement iframe;

	public By iframeVerify() {
		By frame = By.cssSelector("iframe[title='Profile: Community-Employer ~ Salesforce - Unlimited Edition']");
		return frame;
	}

	public EmployerProfile(WebDriver driver) {
		super(driver);
	}

	/*Actions that to check the permission sets for that appropriate profile*/
	
	public void employerCheck(String methodName, String tcName, ReportGenerator generator) throws Exception {

		SeleniumUtils.presenceOfElement(driver, iframeVerify());
		SeleniumUtils.switchToFrame(driver, iframe);
		generator.childReport("Switched Profile frame");
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(standardObjectPermission, driver);
		SeleniumUtils.highLightElement(soClaims, driver);
		SeleniumUtils.verifyPermissionSet(claimsPermission,
				GoogleSheetAPI.ReadData(methodName, tcName, "claims_Permission"), driver);
		generator.childReport("Claims Permission Verified");

	}

}
