package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class ClaimTaskObjectPage extends DriverClass{
	
	
	@FindBy(css = "div[title=\"Edit\"]")
	public WebElement editClick;
	
	public By editButton() {
		By button = By.cssSelector("div[title=\"Edit\"]");
		return button;
	}
	
	@FindBy(xpath = "//p[text()='Claim Task Number']/following::span[1]")
	public  WebElement getTaskNumber;
	
	
	public By editPage() {
		By page = By.xpath("//h2[contains(.,'Edit')]");
		return page;
	}
	
	@FindBy(xpath = "//h2[contains(.,'Edit')]")
	public WebElement editPageVerify;
	
	@FindBy(xpath = "//span/span[text()=\"Status\"]/following::a[1]")
	public WebElement statusClick;
	
	@FindBy(xpath = "//a[text()='Closed-Approved']")
	public WebElement approvedClick;
	
	@FindBy(xpath = "//a[text()='Closed-Denied']")
	public WebElement deniedClick;
	
	@FindBy(xpath = "//a[text()='Request for more information']")
	public WebElement moreInformationClick;
	
	@FindBy(xpath = "//label/span[text()='Status Change Comment']/following::textarea[1]")
	public WebElement statusChangeComment;
	
	@FindBy(xpath = "//span/span[text()='Status Change Reason']/following::a[1]")
	public WebElement statusChangeReason;
	
	
	@FindBy(css = "a[title=\"MISSING CLAIMANT AUTHORIZATION LETTER\"]")
	public WebElement missingReason;
	
	@FindBy(xpath = "//span/span[text()=\"Claim Task Origin\"]/following::a[1]")
	public WebElement taskorigin;
	
	@FindBy(css = "a[title=\"Email\"]")
	public WebElement emailOrigin;
	
	
	@FindBy(css = "button[title=\"Save\"]")
	public WebElement saveClick;
	
	public By closedApproved() {
		By approve = By.xpath("//p[2]/span[text()='Closed-Approved']");
		return approve;
	}
	
	@FindBy(xpath = "//p[2]/span[text()='Closed-Approved']")
	public WebElement approvedStatus;
	
	public By moreInformationVerify() {
		By Info = By.xpath("//p[2]/span[text()='Request for more information']");
		return Info;
	}
	
	@FindBy(xpath = "//p[2]/span[text()='Request for more information']")
	public WebElement moreInformationStatus;
	
	public By pendingVerify() {
		By pend = By.xpath("//p[2]/span[text()='Pending']");
		return pend;
	}
	
	@FindBy(xpath = "//p[2]/span[text()='Pending']")
	public WebElement pendingStatus;
	
	public By closedDenied() {
		By denied = By.xpath("//p[2]/span[text()='Closed-Denied']");
		return denied;
	}
	
	@FindBy(xpath = "//p[2]/span[text()='Closed-Denied']")
	public WebElement deniedStatus;
	
	
	@FindBy(xpath = "//span[text()='Requesting User']/following::a[1]")
	public WebElement requstingUser;
	
	public By userAppear() {
		By user = By.xpath("//span[text()='Requesting User']/following::a[1]");
		return user;
	}
	
	public By activeAppear() {
		By act = By.xpath("//span[2]/div/div[1]/following::img");
		return act;
	}
	
	@FindBy(xpath = "//span[2]/div/div[1]/following::img")
	public WebElement activeCheckbox;
	
	static  String taskno;
	
	@FindBy(xpath = ".//span[text()='Claim Task Owner']/following::div[2]/child::div")
	public WebElement claimTaskOwner;
	
	@FindBy(xpath = ".//span[text()='Task Type']/following::span[1]/span")
	public WebElement TaskType;
	
	@FindBy(xpath = ".//span[text()='Claim']/following::span[1]/child::div[1]/a")
	public WebElement claim;
	
	@FindBy(xpath = ".//span[text()='Status']/following::span[1]/child::span")
	public WebElement Status;
	
	@FindBy(xpath = ".//span[text()='Priority']/following::span[1]/child::span")
	public WebElement Priority;
	
	@FindBy(xpath = ".//span[text()='Due Date']/following::span[1]/child::span")
	public WebElement Duedate;
	
	@FindBy(xpath = ".//span[text()='Overdue Date']/following::span[1]/child::span")
	public WebElement OverDuedate;
	
	@FindBy(xpath = ".//span[text()='Description']/following::span[1]/child::span")
	public WebElement Description;


	public ClaimTaskObjectPage(WebDriver driver) {
		super(driver);
	}
	
	/*To change status as Closed-Approved in Claimant type*/
	public void editPageClaimant(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, editButton());
		SeleniumUtils.highLightElement(editClick, driver);
		editClick.click();
		generator.childReport("Edit Record Clicked");
		
		SeleniumUtils.presenceOfElement(driver, editPage());
		SeleniumUtils.highLightElement(editPageVerify, driver);
		generator.childReport("Edit Page Verified");
		
		SeleniumUtils.highLightElement(statusClick, driver);
		statusClick.click();
		generator.childReport("Status Clicked");
		approvedClick.click();
		generator.childReport("Status Changed as 'Closed-Approved'");
		SeleniumUtils.highLightElement(statusChangeComment, driver);
		statusChangeComment.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "status reason"));
		generator.childReport("Status Comment Entered");
		SeleniumUtils.highLightElement(statusChangeReason, driver);
		statusChangeReason.click();
		missingReason.click();
		generator.childReport("Status Reason Entered");
		saveClick.click();
		generator.childReport("Save Button Clicked");
		
		SeleniumUtils.presenceOfElement(driver, closedApproved());
		SeleniumUtils.highLightElement(approvedStatus, driver);
		generator.childReport("Status Verified");
		SeleniumUtils.scroll(requstingUser, driver);
		generator.childReport("Mouse Hover Verified");
		SeleniumUtils.presenceOfElement(driver, activeAppear());
		SeleniumUtils.highLightElement(activeCheckbox, driver);
		generator.childReport("Active Checkbox Verified");
		
		
	}
	
	/*To change status as Request for Information in Employer*/
	public void editPageNonClaimant(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, editButton());
		SeleniumUtils.highLightElement(editClick, driver);
		editClick.click();
		generator.childReport("Edit Record Clicked");
		
		SeleniumUtils.presenceOfElement(driver, editPage());
		SeleniumUtils.highLightElement(editPageVerify, driver);
		generator.childReport("Edit Page Verified");
		
		SeleniumUtils.highLightElement(statusClick, driver);
		statusClick.click();
		generator.childReport("Status Clicked");
		moreInformationClick.click();
		generator.childReport("Status Changed as 'Request for more information'");
		SeleniumUtils.highLightElement(statusChangeComment, driver);
		statusChangeComment.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "status reason"));
		generator.childReport("Status Comment Entered");
		SeleniumUtils.highLightElement(statusChangeReason, driver);
		statusChangeReason.click();
		missingReason.click();
		generator.childReport("Status Reason Entered");
		saveClick.click();
		generator.childReport("Save Button Clicked");

		SeleniumUtils.presenceOfElement(driver, moreInformationVerify());
		SeleniumUtils.highLightElement(moreInformationStatus, driver);
		generator.childReport("Status Verified");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, userAppear());
		SeleniumUtils.scroll(requstingUser, driver);
		generator.childReport("Mouse Hover Verified");
		SeleniumUtils.presenceOfElement(driver, activeAppear());
		SeleniumUtils.highLightElement(activeCheckbox, driver);
		generator.childReport("Active Checkbox Verified");
		
	}
	
	/*To change status as Closed-Approved in New Claim Request*/
	public void editPageClaimRequestApprove(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, editButton());
		SeleniumUtils.highLightElement(editClick, driver);
		editClick.click();
		generator.childReport("Edit Record Clicked");
		
		SeleniumUtils.presenceOfElement(driver, editPage());
		SeleniumUtils.highLightElement(editPageVerify, driver);
		generator.childReport("Edit Page Verified");
		
		SeleniumUtils.highLightElement(statusClick, driver);
		statusClick.click();
		generator.childReport("Status Clicked");
		approvedClick.click();
		generator.childReport("Status Changed as 'Closed-Approved'");
		SeleniumUtils.highLightElement(statusChangeComment, driver);
		statusChangeComment.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "status reason"));
		generator.childReport("Status Comment Entered");
		SeleniumUtils.highLightElement(statusChangeReason, driver);
		statusChangeReason.click();
		missingReason.click();
		generator.childReport("Status Reason Entered");
		taskorigin.click();
		emailOrigin.click();
		generator.childReport("Email Clicked");
		saveClick.click();
		generator.childReport("Save Button Clicked");
		
		SeleniumUtils.presenceOfElement(driver, closedApproved());
		SeleniumUtils.highLightElement(approvedStatus, driver);
		generator.childReport("Status Verified");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, userAppear());
		SeleniumUtils.scroll(requstingUser, driver);
		generator.childReport("Owner name Verified");
	
	}
	
	public void editPageClaimRequestMoreInformation(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
		
		taskno = getTaskNumber.getText();
		
		SeleniumUtils.presenceOfElement(driver, editButton());
		SeleniumUtils.highLightElement(editClick, driver);
		editClick.click();
		generator.childReport("Edit Record Clicked");
		
		SeleniumUtils.presenceOfElement(driver, editPage());
		SeleniumUtils.highLightElement(editPageVerify, driver);
		generator.childReport("Edit Page Verified");
		
		SeleniumUtils.highLightElement(statusClick, driver);
		statusClick.click();
		generator.childReport("Status Clicked");
		moreInformationClick.click();
		generator.childReport("Status Changed as 'Request for more information'");
		SeleniumUtils.highLightElement(statusChangeComment, driver);
		statusChangeComment.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "status reason"));
		generator.childReport("Status Comment Entered");
		SeleniumUtils.highLightElement(statusChangeReason, driver);
		statusChangeReason.click();
		missingReason.click();
		generator.childReport("Status Reason Entered");
		taskorigin.click();
		emailOrigin.click();
		generator.childReport("Email Clicked");
		saveClick.click();
		generator.childReport("Save Button Clicked");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, moreInformationVerify());
		SeleniumUtils.highLightElement(moreInformationStatus, driver);
		generator.childReport("Status Verified");
		
		/*SeleniumUtils.presenceOfElement(driver, userAppear());
		SeleniumUtils.scroll(requstingUser, driver);
		generator.childReport("Mouse Hover Verified");
		SeleniumUtils.presenceOfElement(driver, activeAppear());
		SeleniumUtils.highLightElement(activeCheckbox, driver);
		generator.childReport("Active Checkbox Verified");*/
	}
	
	public void verifyPendingClaimRequest(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
		SeleniumUtils.presenceOfElement(driver, pendingVerify());
		SeleniumUtils.highLightElement(pendingStatus, driver);
		generator.childReport("Pending Status Verified");
	}
	
	public void editPageClaimRequestdenied(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
		SeleniumUtils.presenceOfElement(driver, editButton());
		SeleniumUtils.highLightElement(editClick, driver);
		editClick.click();
		generator.childReport("Edit Record Clicked");
		
		SeleniumUtils.presenceOfElement(driver, editPage());
		SeleniumUtils.highLightElement(editPageVerify, driver);
		generator.childReport("Edit Page Verified");
		
		SeleniumUtils.highLightElement(statusClick, driver);
		statusClick.click();
		generator.childReport("Status Clicked");
		deniedClick.click();
		generator.childReport("Status Changed as 'Closed-Denied'");
		SeleniumUtils.highLightElement(statusChangeComment, driver);
		statusChangeComment.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "status reason"));
		generator.childReport("Status Comment Entered");
		SeleniumUtils.highLightElement(statusChangeReason, driver);
		statusChangeReason.click();
		missingReason.click();
		generator.childReport("Status Reason Entered");
		taskorigin.click();
		emailOrigin.click();
		generator.childReport("Email Clicked");
		saveClick.click();
		generator.childReport("Save Button Clicked");
		
		SeleniumUtils.presenceOfElement(driver, closedDenied());
		SeleniumUtils.highLightElement(deniedStatus, driver);
		generator.childReport("Status Verified");
		
	}
	
	//To verify the Task details
	public void VerifyClaimTask(String methodName, String tcName, ReportGenerator generator) throws Exception {
		generator.childReport("Task Owner verified");
		Thread.sleep(3000);
		SeleniumUtils.highLightElement(claimTaskOwner, driver);
		String TaskOwner = claimTaskOwner.getText();
		TestUtils.compareText(TaskOwner, GoogleSheetAPI.ReadData(methodName, tcName, "task Owner"), claimTaskOwner, driver);
		
		generator.childReport("Task Type verified");
		SeleniumUtils.highLightElement(TaskType, driver);
		String StrTaskType = TaskType.getText();
		TestUtils.compareText(StrTaskType, GoogleSheetAPI.ReadData(methodName, tcName, "task Type"), TaskType, driver);
		
		generator.childReport("Task Status verified");
		SeleniumUtils.highLightElement(Status, driver);
		String StrTaskStatus = Status.getText();
		TestUtils.compareText(StrTaskStatus, GoogleSheetAPI.ReadData(methodName, tcName, "task Status"), Status, driver);
		
		generator.childReport("Task Priority verified");
		SeleniumUtils.highLightElement(Priority, driver);
		String StrTaskPriority = Priority.getText();
		TestUtils.compareText(StrTaskPriority, GoogleSheetAPI.ReadData(methodName, tcName, "task Priority"), Priority, driver);
		
		generator.childReport("Task Duedate verified");
		SeleniumUtils.highLightElement(Duedate, driver);
		String StrTaskDuedate = Duedate.getText();
		TestUtils.compareDate(StrTaskDuedate, GoogleSheetAPI.ReadData(methodName, tcName, "task Duedate"), Duedate, driver);
		
		generator.childReport("Task OverDuedate verified");
		SeleniumUtils.highLightElement(OverDuedate, driver);
		String StrTaskOverDuedate = OverDuedate.getText();
		TestUtils.compareDate(StrTaskOverDuedate, GoogleSheetAPI.ReadData(methodName, tcName, "task OverDuedate"), OverDuedate, driver);
		
		generator.childReport("Task Description verified");
		SeleniumUtils.highLightElement(Description, driver);
		String StrTaskDescription = Description.getText();
		TestUtils.compareText(StrTaskDescription, GoogleSheetAPI.ReadData(methodName, tcName, "task Description"), Description, driver);

	}

}
