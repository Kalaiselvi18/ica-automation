package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To verify whether the Claims Processor salesforce profile is created
 *
 */

public class ClaimsProcessor extends BaseTest {
	
	@Test (groups = "Smoke Suite")
	@Parameters({ "userType" })
	
	public void claimsProcessorProfile(String userType) throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_002";
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");

		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.login(user, pwd, reporter);
		reporter.childReport("Login_Verify");
		profileSetup = homePage.verifyHomePage1();	
		profileNameSelect = profileSetup.searchProfile(methodName, tcName,reporter);
		claimsProcessor = profileNameSelect.nameProfile002(methodName, tcName, reporter);
		driver.switchTo().defaultContent();
		claimsProcessor.claimsProcessorCheck(methodName, tcName, reporter);
	}

}
