/**
 * @author sophiya
 * Created date:
 * Last Edited by: sophiya
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author sophiya
 * Created date: Feb 10, 2018
 * Last Edited by: sophiya
 * Last Edited date: 
 * Description: Create a 102 form and verify new claim is created with current date (depends on DOI,DOB, Last name)
 * Login to Salesforce, open the claim, edit claimant details and verify the claimant account match logic */

public class ExistingClaimantAccountMapping extends BaseTest {

	@Test (groups = {"Form Regression Suite","102 Flow"})
	@Parameters({"env1","userType"})
	
	public void doc102existingClaimantAccountMapping(String env1,String userType) throws Exception {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_104";
		
		String Url = TestUtils.getStringFromPropertyFile(env1);
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		reporter = new ReportGenerator(getbrowser(), className);
		page102 = loginPage.formUrl102();
		page102.fillingForm102(methodName, tcName, reporter);
		
		SeleniumUtils.switchToNewTab(driver, Url);
		homePage = loginPage.login(user, pwd, reporter);
		docuselect = homePage.documentObjClick(reporter);
		verify102 = docuselect.docu102Verify(methodName, tcName, reporter);
		
		verify102.document102DetailPage(methodName, tcName, reporter);
		claimDetailPage = verify102.document102RelatedPage(methodName, tcName,reporter);
		claimDetailPage.editClaimantSection(methodName, tcName, reporter);
		accountPage = claimDetailPage.clickOnClaimantAccount(methodName,reporter);
		accountPage.verifyClaimantAccount(methodName, tcName, reporter);
	}	
}
