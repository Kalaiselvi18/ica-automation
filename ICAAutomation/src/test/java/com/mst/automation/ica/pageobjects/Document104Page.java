package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document104Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document104Page(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim No.']/following::span[2]")
	public WebElement icaClaimNo;
	
	
public void verify104Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		String icaClaim = icaClaimNo.getText();
		TestUtils.compareText(icaClaim, GoogleSheetAPI.ReadData(methodName, tcName, "ICA Claim No"), icaClaimNo, driver);
		
		
}

}
