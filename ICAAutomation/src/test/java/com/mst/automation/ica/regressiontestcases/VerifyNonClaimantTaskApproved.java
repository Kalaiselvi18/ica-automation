package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

public class VerifyNonClaimantTaskApproved extends BaseTest{
	@Test (groups = "Regression Suite")
	@Parameters({ "userType" })
	
	public void nonclaimantrequest (String userType) throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_044";

		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");

		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.login(user, pwd, reporter);
		reporter.childReport("Login_Verify");
		claimTaskSelect = homePage.claimTaskObjClick(reporter);
		pageClaimTask = claimTaskSelect.selectClaimTask(methodName, tcName, reporter);
		pageClaimTask.editPageNonClaimant(methodName, tcName, reporter);
		
	}

}
