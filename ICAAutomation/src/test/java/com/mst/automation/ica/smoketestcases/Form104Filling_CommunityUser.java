/**
 * 
 */
package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Aartheeswaran
 * Created date: Jan 18, 2018
 * Last Edited by: Aartheeswaran...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form104Filling_CommunityUser extends BaseTest {
	
	@Test (groups = "Smoke Suite")
	@Parameters({ "userType" })
		
		public void formfilling104(String userType) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_104";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.loginCommunity(user, pwd, reporter);
		page104 = homePage.formUrl104cu();
		page104.fillingForm104Community(methodName, tcName, reporter);
	}

}
