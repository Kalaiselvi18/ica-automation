package com.mst.automation.ica.abstractclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;



public abstract class DriverClass  {

	protected WebDriver driver;
	protected WebDriverWait wait;
	protected Actions action;
	
	public DriverClass(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
}
