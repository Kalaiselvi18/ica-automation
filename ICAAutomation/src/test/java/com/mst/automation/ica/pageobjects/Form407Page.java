/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 22-Nov-2017
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form407Page extends DriverClass{
	
	@FindBy(xpath = "html/body/form/div[1]/div[1]/div[2]")
	public WebElement ica407Verify;
	
	@FindBy(xpath = "//label[text()='First Name']/following::input[1]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//label[text()='Last Name']/following::input[1]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//label[text()='Middle Initial']/following::input[1]")
	public WebElement claimantMiddleInitial;
	
	@FindBy(xpath = "//th[text()='Social Security Number *']/following::input[1]")
	public WebElement socialSecurityNumber;
	
	@FindBy(xpath = "html/body/form/div[1]/div[4]/div[1]/div/div[2]/div[1]/div[2]/table/tbody/tr[3]/td[1]/div/span/span/a")
	public WebElement claimantBirthDate;
	
	@FindBy(xpath = "//label[text()='Date of Birth']/following::input[2]")
	public WebElement claimantPhone;
	
	@FindBy(xpath = "//label[text()='Date of Birth']/following::textarea[1]")
	public WebElement claimantAddress;
	
	@FindBy(xpath = "//label[text()='Date of Birth']/following::input[3]")
	public WebElement claimantCity;
	
	@FindBy(xpath = "//h3[contains(.,'Claimant')]/following::input[8]")
	public WebElement claimantState;
	
	@FindBy(xpath = "//label[text()='Date of Birth']/following::input[5]")
	public WebElement claimantZipCode;
	
	
	@FindBy(xpath = "//label[text()='Marital Status']/following::select[1]")
	public WebElement claimantMartialStatus;
	
	@FindBy(xpath = "//label[text()='Dependents at Time of Injury?']/following::select[1]")
	public WebElement claimantDependentsTime;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::textarea[1]")
	public WebElement employer;
	
	@FindBy(xpath = "//label[contains(.,'Supervisor')]/following::input[1]")
	public WebElement suprvisor;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[2]")
	public WebElement employerPhone;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::textarea[2]")
	public WebElement employerAddress;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[4]")
	public WebElement employerCity;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[5]")
	public WebElement employerState;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[6]")
	public WebElement employerZipCode;
	
	@FindBy(xpath = "//h3[contains(.,'Occupation')]/following::td[1]/div/span/span/a")
	public WebElement occupationDateHire;
	
	@FindBy(xpath = "//h3[contains(.,'Occupation')]/following::textarea[1]")
	public WebElement occupationWhereHire;
	
	@FindBy(xpath = "//h3[contains(.,'Occupation')]/following::input[2]")
	public WebElement occupation;
	
	@FindBy(xpath = "//h3[contains(.,'Occupation')]/following::input[3]")
	public WebElement occupationWorkHours;
	
	@FindBy(xpath = "//h3[contains(.,'Occupation')]/following::input[4]")
	public WebElement occupationPerWeek;
	
	@FindBy(xpath = "//h3[contains(.,'Occupation')]/following::input[5]")
	public WebElement occupationHourlyWage;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::a[1]")
	public WebElement injuryDate;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::textarea[1]")
	public WebElement injuryLocation;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::input[6]")
	public WebElement injuryToWhom;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::a[3]")
	public WebElement injuryReportDate;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::input[7]")
	public WebElement injuryTitle;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::input[12]")
	public WebElement injuryAccidentHappend;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::input[13]")
	public WebElement injuryBodyPart;
	
	@FindBy(xpath = "//h3[text()='Injury Details']/following::textarea[3]")
	public WebElement injuryDescribe;
	
	@FindBy(xpath = "//h3[text()='Treatment Details']/following::input[1]")
	public WebElement treatmentName1;
	
	@FindBy(xpath = "//h3[text()='Treatment Details']/following::input[2]")
	public WebElement treatmentName2;
	
	@FindBy(xpath = "//h3[text()='Treatment Details']/following::textarea[1]")
	public WebElement treatmentAddress1;
	
	@FindBy(xpath = "//h3[text()='Treatment Details']/following::textarea[2]")
	public WebElement treatmentAddress2;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::a[1]")
	public WebElement authenticationDate;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[3]")
	public WebElement authenticationAddress;
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By element() {
		By ul=By.xpath("//label[text()='First Name']/following::input[1]");
		return ul;
	}
	
	public By verfication() {
		By ver=By.xpath("//td[contains(.,'Your document')]");
		return ver;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifySuccess;
	

	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public Form407Page(WebDriver driver){
		 super(driver);
	 }

	/*To fill the 407 Form*/
	
	public void fillingForm407(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica407Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant first name"));
		generator.childReport("Claimant First name entered");
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant last name"));
		generator.childReport("Claimant Last name entered");
		
		SeleniumUtils.highLightElement(claimantMiddleInitial, driver);
		claimantMiddleInitial.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant middle initial"));
		generator.childReport("claimantMiddleInitial entered");
		
		SeleniumUtils.highLightElement(socialSecurityNumber, driver);
		socialSecurityNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant social security number"));
		generator.childReport("socialSecurityNumber entered");
		
		SeleniumUtils.highLightElement(claimantBirthDate, driver);
		//claimantBirthDate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant birth day"));
		claimantBirthDate.click();
		generator.childReport("claimantBirthDate entered");
		
		SeleniumUtils.highLightElement(claimantPhone, driver);
		claimantPhone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant phone"));
		generator.childReport("claimantPhone entered");
		
		SeleniumUtils.highLightElement(claimantAddress, driver);
		claimantAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant address"));
		generator.childReport("claimantAddress entered");
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		claimantCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant city"));
		generator.childReport("claimantCity entered");
		
		SeleniumUtils.highLightElement(claimantState, driver);
		claimantState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant state"));
		generator.childReport("claimantState entered");
		
		SeleniumUtils.highLightElement(claimantZipCode, driver);
		claimantZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant zip code"));
		generator.childReport("claimantZipCode entered");
		
		SeleniumUtils.dropDown(claimantMartialStatus, GoogleSheetAPI.ReadData(methodName, tcName, "claimant martial status"));
		generator.childReport("Claimants Martial Status selected");
		
		SeleniumUtils.dropDown(claimantDependentsTime, GoogleSheetAPI.ReadData(methodName, tcName, "claimant dependents time"));
		generator.childReport("Claimants Dependents time selected");
		
		SeleniumUtils.highLightElement(employer, driver);
		employer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employer"));
		generator.childReport("employer entered");
		
		SeleniumUtils.highLightElement(suprvisor, driver);
		suprvisor.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "suprvisor"));
		generator.childReport("suprvisor entered");
		
		SeleniumUtils.highLightElement(employerPhone, driver);
		employerPhone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerPhone"));
		generator.childReport("employerPhone entered");
		
		SeleniumUtils.highLightElement(employerAddress, driver);
		employerAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerAddress"));
		generator.childReport("employerAddress entered");
		
		SeleniumUtils.highLightElement(employerCity, driver);
		employerCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerCity"));
		generator.childReport("employerCity entered");
		
		SeleniumUtils.highLightElement(employerState, driver);
		employerState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerState"));
		generator.childReport("employerState entered");
		
		SeleniumUtils.highLightElement(employerZipCode, driver);
		employerZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerZipCode"));
		generator.childReport("employerZipCode entered");
		
		SeleniumUtils.highLightElement(occupationDateHire, driver);
		occupationDateHire.click();
		generator.childReport("occupationDateHire entered");
		
		SeleniumUtils.highLightElement(occupationWhereHire, driver);
		occupationWhereHire.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationWhereHire"));
		generator.childReport("occupationWhereHire entered");
		
		SeleniumUtils.highLightElement(occupation, driver);
		occupation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupation"));
		generator.childReport("occupation entered");
		
		SeleniumUtils.highLightElement(occupationWorkHours, driver);
		occupationWorkHours.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationWorkHours"));
		generator.childReport("occupationWorkHours entered");
		
		SeleniumUtils.highLightElement(occupationPerWeek, driver);
		occupationPerWeek.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationPerWeek"));
		generator.childReport("occupationPerWeek entered");
		
		SeleniumUtils.highLightElement(occupationHourlyWage, driver);
		occupationHourlyWage.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationHourlyWage"));
		generator.childReport("occupationHourlyWage entered");

		SeleniumUtils.highLightElement(injuryDate, driver);
		injuryDate.click();
		generator.childReport("injuryDate entered");
		
		SeleniumUtils.highLightElement(injuryLocation, driver);
		injuryLocation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryLocation"));
		generator.childReport("injuryLocation entered");
		
		SeleniumUtils.highLightElement(injuryToWhom, driver);
		injuryToWhom.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryToWhom"));
		generator.childReport("injuryToWhom entered");
		
		SeleniumUtils.highLightElement(injuryReportDate, driver);
		injuryReportDate.click();
		generator.childReport("injuryReportDate entered");
		
		SeleniumUtils.highLightElement(injuryTitle, driver);
		injuryTitle.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryTitle"));
		generator.childReport("injuryTitle entered");
		
		SeleniumUtils.highLightElement(injuryAccidentHappend, driver);
		injuryAccidentHappend.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryAccidentHappend"));
		generator.childReport("injuryAccidentHappend entered");
		
		SeleniumUtils.highLightElement(injuryBodyPart, driver);
		injuryBodyPart.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryBodyPart"));
		generator.childReport("injuryBodyPart entered");
		
		SeleniumUtils.highLightElement(injuryDescribe, driver);
		injuryDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryDescribe"));
		generator.childReport("injuryDescribe entered");
		
		SeleniumUtils.highLightElement(treatmentName1, driver);
		treatmentName1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentName1"));
		generator.childReport("treatmentName1 entered");
		
		SeleniumUtils.highLightElement(treatmentName2, driver);
		treatmentName2.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentName2"));
		generator.childReport("treatmentName2 entered");
		
		SeleniumUtils.highLightElement(treatmentAddress1, driver);
		treatmentAddress1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentAddress1"));
		generator.childReport("treatmentAddress1 entered");
		
		SeleniumUtils.highLightElement(treatmentAddress2, driver);
		treatmentAddress2.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentAddress2"));
		generator.childReport("treatmentAddress2 entered");
		
		SeleniumUtils.highLightElement(authenticationDate, driver);
		authenticationDate.click();
		generator.childReport("authenticationDate entered");
		
		SeleniumUtils.highLightElement(authenticationAddress, driver);
		authenticationAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationAddress"));
		generator.childReport("authenticationAddress entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		SeleniumUtils.presenceOfElement(driver, verfication());
		SeleniumUtils.highLightElement(verifySuccess, driver);
}
public void fillingForm407Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	SeleniumUtils.presenceOfElement(driver, community());
	
	SeleniumUtils.switchToFrame(driver, frameswitch);
	
	generator.childReport("Signature frame switched");
	
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica407Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant first name"));
		generator.childReport("Claimant First name entered");
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant last name"));
		generator.childReport("Claimant Last name entered");
		
		SeleniumUtils.highLightElement(claimantMiddleInitial, driver);
		claimantMiddleInitial.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant middle initial"));
		generator.childReport("claimantMiddleInitial entered");
		
		SeleniumUtils.highLightElement(socialSecurityNumber, driver);
		socialSecurityNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant social security number"));
		generator.childReport("socialSecurityNumber entered");
		
		SeleniumUtils.highLightElement(claimantBirthDate, driver);
		//claimantBirthDate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant birth day"));
		claimantBirthDate.click();
		generator.childReport("claimantBirthDate entered");
		
		SeleniumUtils.highLightElement(claimantPhone, driver);
		claimantPhone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant phone"));
		generator.childReport("claimantPhone entered");
		
		SeleniumUtils.highLightElement(claimantAddress, driver);
		claimantAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant address"));
		generator.childReport("claimantAddress entered");
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		claimantCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant city"));
		generator.childReport("claimantCity entered");
		
		SeleniumUtils.highLightElement(claimantState, driver);
		claimantState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant state"));
		generator.childReport("claimantState entered");
		
		SeleniumUtils.highLightElement(claimantZipCode, driver);
		claimantZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant zip code"));
		generator.childReport("claimantZipCode entered");
		
		SeleniumUtils.dropDown(claimantMartialStatus, GoogleSheetAPI.ReadData(methodName, tcName, "claimant martial status"));
		generator.childReport("Claimants Martial Status selected");
		
		SeleniumUtils.dropDown(claimantDependentsTime, GoogleSheetAPI.ReadData(methodName, tcName, "claimant dependents time"));
		generator.childReport("Claimants Dependents time selected");
		
		SeleniumUtils.highLightElement(employer, driver);
		employer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employer"));
		generator.childReport("employer entered");
		
		SeleniumUtils.highLightElement(suprvisor, driver);
		suprvisor.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "suprvisor"));
		generator.childReport("suprvisor entered");
		
		SeleniumUtils.highLightElement(employerPhone, driver);
		employerPhone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerPhone"));
		generator.childReport("employerPhone entered");
		
		SeleniumUtils.highLightElement(employerAddress, driver);
		employerAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerAddress"));
		generator.childReport("employerAddress entered");
		
		SeleniumUtils.highLightElement(employerCity, driver);
		employerCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerCity"));
		generator.childReport("employerCity entered");
		
		SeleniumUtils.highLightElement(employerState, driver);
		employerState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerState"));
		generator.childReport("employerState entered");
		
		SeleniumUtils.highLightElement(employerZipCode, driver);
		employerZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerZipCode"));
		generator.childReport("employerZipCode entered");
		
		SeleniumUtils.highLightElement(occupationDateHire, driver);
		occupationDateHire.click();
		generator.childReport("occupationDateHire entered");
		
		SeleniumUtils.highLightElement(occupationWhereHire, driver);
		occupationWhereHire.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationWhereHire"));
		generator.childReport("occupationWhereHire entered");
		
		SeleniumUtils.highLightElement(occupation, driver);
		occupation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupation"));
		generator.childReport("occupation entered");
		
		SeleniumUtils.highLightElement(occupationWorkHours, driver);
		occupationWorkHours.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationWorkHours"));
		generator.childReport("occupationWorkHours entered");
		
		SeleniumUtils.highLightElement(occupationPerWeek, driver);
		occupationPerWeek.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationPerWeek"));
		generator.childReport("occupationPerWeek entered");
		
		SeleniumUtils.highLightElement(occupationHourlyWage, driver);
		occupationHourlyWage.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupationHourlyWage"));
		generator.childReport("occupationHourlyWage entered");

		SeleniumUtils.highLightElement(injuryDate, driver);
		injuryDate.click();
		generator.childReport("injuryDate entered");
		
		SeleniumUtils.highLightElement(injuryLocation, driver);
		injuryLocation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryLocation"));
		generator.childReport("injuryLocation entered");
		
		SeleniumUtils.highLightElement(injuryToWhom, driver);
		injuryToWhom.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryToWhom"));
		generator.childReport("injuryToWhom entered");
		
		SeleniumUtils.highLightElement(injuryReportDate, driver);
		injuryReportDate.click();
		generator.childReport("injuryReportDate entered");
		
		SeleniumUtils.highLightElement(injuryTitle, driver);
		injuryTitle.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryTitle"));
		generator.childReport("injuryTitle entered");
		
		SeleniumUtils.highLightElement(injuryAccidentHappend, driver);
		injuryAccidentHappend.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryAccidentHappend"));
		generator.childReport("injuryAccidentHappend entered");
		
		SeleniumUtils.highLightElement(injuryBodyPart, driver);
		injuryBodyPart.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryBodyPart"));
		generator.childReport("injuryBodyPart entered");
		
		SeleniumUtils.highLightElement(injuryDescribe, driver);
		injuryDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuryDescribe"));
		generator.childReport("injuryDescribe entered");
		
		SeleniumUtils.highLightElement(treatmentName1, driver);
		treatmentName1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentName1"));
		generator.childReport("treatmentName1 entered");
		
		SeleniumUtils.highLightElement(treatmentName2, driver);
		treatmentName2.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentName2"));
		generator.childReport("treatmentName2 entered");
		
		SeleniumUtils.highLightElement(treatmentAddress1, driver);
		treatmentAddress1.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentAddress1"));
		generator.childReport("treatmentAddress1 entered");
		
		SeleniumUtils.highLightElement(treatmentAddress2, driver);
		treatmentAddress2.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "treatmentAddress2"));
		generator.childReport("treatmentAddress2 entered");
		
		SeleniumUtils.highLightElement(authenticationDate, driver);
		authenticationDate.click();
		generator.childReport("authenticationDate entered");
		
		SeleniumUtils.highLightElement(authenticationAddress, driver);
		authenticationAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationAddress"));
		generator.childReport("authenticationAddress entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
driver.switchTo().defaultContent();
		
SeleniumUtils.presenceOfElement(driver, community());

SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verfication());
		SeleniumUtils.highLightElement(verifySuccess, driver);
}
}
