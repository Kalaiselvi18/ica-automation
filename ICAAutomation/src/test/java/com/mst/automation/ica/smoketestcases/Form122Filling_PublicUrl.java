/**
 * 
 */
package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

/**
 * @author Infant Raja Marshall
 * Created date: 17-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form122Filling_PublicUrl extends BaseTest{
@Test (groups = "Smoke Suite")
	
	public void formfilling122() throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_122";
		
		reporter = new ReportGenerator(getbrowser(), className);
		page122 = loginPage.formUrl122();
		page122.fillingForm122(methodName, tcName, reporter);
		

	}

}
