package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document102CarrierAccountNotMappedUsingPolicyNo extends BaseTest{
	@Test (groups = {"Regression Suite","102 flow"})
	@Parameters({"env1","userType"})
		
		public void doc102CarrierAccountNotMappedUsingPolicyNo(String env1,String userType) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_002";
		
		String Url = TestUtils.getStringFromPropertyFile(env1);
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		reporter = new ReportGenerator(getbrowser(), className);
		page102 = loginPage.formUrl102();
		page102.fillingForm102(methodName, tcName, reporter);
		
		SeleniumUtils.switchToNewTab(driver, Url);
		homePage = loginPage.login(user, pwd, reporter);
		docuselect = homePage.documentObjClick(reporter);
		verify102 = docuselect.docu102Verify(methodName, tcName, reporter);
		verify102.document102DetailPage(methodName, tcName, reporter);
		claimDetailPage = verify102.document102RelatedPage(methodName, tcName, reporter);
		pageClaimTask = claimDetailPage.nonpolicyNumberVerify(methodName, tcName, reporter);
		pageClaimTask = claimDetailPage.openTask(methodName, tcName, reporter);
		pageClaimTask.VerifyClaimTask(methodName, tcName, reporter);
		

		 
		

	}

}
