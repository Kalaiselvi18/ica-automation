/**
 * 
 */
package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

/**
 * @author Infant Raja Marshall
 * Created date: 30-Nov-2017
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form446Filling_PublicUrl extends BaseTest {
	@Test (groups = "Form Regression Suite")
	
	
	public void formfilling446() throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_446";
		
		reporter = new ReportGenerator(getbrowser(), className);
		page446 = loginPage.formUrl446();
		page446.fillingForm446(methodName, tcName, reporter);

	}

}
