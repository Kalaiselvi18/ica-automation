/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 17-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form120Page extends DriverClass {
	
	@FindBy(xpath = "//div[2][contains(.,'INDUSTRIAL COMMISSION OF ARIZONA')]")
	public WebElement ica120Verify;
	
	public By element() {
		By open = By.xpath("//div[2][contains(.,'INDUSTRIAL COMMISSION OF ARIZONA')]");
		return open;
	}
	
	@FindBy(xpath = "//td[contains(.,'CHECK APPROPRIATE BOX:')]/following::input[1]")
	public WebElement spouse;
	
	@FindBy(xpath = "//label[text()='First Name of Deceased']/following::input[1]")
	public WebElement firstnamedeceased;
	
	@FindBy(xpath = "//h3[contains(.,'INFORMATION REGARDING')]/following::input[2]")
	public WebElement lastname;
	
	@FindBy(xpath = "//h3[contains(.,'INFORMATION REGARDING')]/following::a[1]")
	public WebElement dateofbirth;
	
	@FindBy(xpath = "//h3[contains(.,'INFORMATION REGARDING')]/following::a[2]")
	public WebElement dateofdeath;
	
	@FindBy(xpath = "//h3[contains(.,'INFORMATION REGARDING')]/following::a[3]")
	public WebElement dateofinjury;
	
	@FindBy(xpath = "//h3[contains(.,'INFORMATION REGARDING')]/following::textarea[1]")
	public WebElement deceasedaddress;
	
	@FindBy(xpath = "//h3[contains(.,'INFORMATION REGARDING')]/following::textarea[2]")
	public WebElement employertimeofdeath;
	
	@FindBy(xpath = "//h3[contains(.,'INFORMATION REGARDING')]/following::textarea[3]")
	public WebElement employersaddress;
	
	@FindBy(xpath = "//label[text()='Submitter Printed Name']/following::input[1]")
	public WebElement submitterprintedname;
	
	@FindBy(xpath = "//label[text()='Submitter Email Address']/following::input[1]")
	public WebElement submitteremail;
	
	@FindBy(xpath = "//label[text()='Submitter Address']/following::textarea[1]")
	public WebElement submitteraddress;
	
	@FindBy(xpath = "//label[text()='Date']/following::a[1]")
	public WebElement date;
	
	@FindBy(xpath = "//label[text()='Telephone Number']/following::input[1]")
	public WebElement telephonenumber;
	
	@FindBy(css = "input[type='Submit']")
	public WebElement submit;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;

	
	
	public Form120Page(WebDriver driver){
		 super(driver);
	 }
	
	public void fillingForm120(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica120Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(spouse, driver);
		spouse.click();
		generator.childReport("spouse entered");
		
		SeleniumUtils.highLightElement(firstnamedeceased, driver);
		firstnamedeceased.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "firstnamedeceased"));
		generator.childReport("firstnamedeceased entered");
		
		SeleniumUtils.highLightElement(lastname, driver);
		lastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "lastname"));
		generator.childReport("lastname entered");
		
		SeleniumUtils.highLightElement(dateofbirth, driver);
		dateofbirth.click();
		generator.childReport("dateofbirth entered");
		
		SeleniumUtils.highLightElement(dateofdeath, driver);
		dateofdeath.click();
		generator.childReport("dateofdeath entered");
		
		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");
		
		SeleniumUtils.highLightElement(deceasedaddress, driver);
		deceasedaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "deceasedaddress"));
		generator.childReport("deceasedaddress entered");
		
		SeleniumUtils.highLightElement(employertimeofdeath, driver);
		employertimeofdeath.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employertimeofdeath"));
		generator.childReport("employertimeofdeath entered");
		
		SeleniumUtils.highLightElement(employersaddress, driver);
		employersaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employersaddress"));
		generator.childReport("employersaddress entered");
		
		SeleniumUtils.highLightElement(submitterprintedname, driver);
		submitterprintedname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitterprintedname"));
		generator.childReport("submitterprintedname entered");

		
		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");

		
		SeleniumUtils.highLightElement(submitteraddress, driver);
		submitteraddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteraddress"));
		generator.childReport("submitteraddress entered");
		
		SeleniumUtils.highLightElement(date, driver);
		date.click();
		generator.childReport("date entered");
		
		SeleniumUtils.highLightElement(telephonenumber, driver);
		telephonenumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "telephonenumber"));
		generator.childReport("telephonenumber entered");
		
		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);

}
}
