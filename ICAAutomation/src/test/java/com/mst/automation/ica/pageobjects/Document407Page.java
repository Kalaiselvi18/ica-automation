package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document407Page extends DriverClass {
	
	@FindBy(xpath = "//h3/button/span[text()=\"Claimant's Details\"]/following::span[2]/span")
	public WebElement clmntFrstName;
	
	@FindBy(xpath = "//span[text()='Middle Initial']/following::span[1]/span")
	public WebElement middleInitial;
	
	@FindBy(xpath = "//span[text()='Social Security #']/following::span[1]/span")
	public WebElement socialSecurity;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Claimant's Details\"]/following::span[10]/span")
	public WebElement claimantAddress;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Claimant's Details\"]/following::span[22]/span")
	public WebElement claimantCity;
	
	@FindBy(xpath = "//span[text()=\"Marital Status\"]/following::span[1]/span")
	public WebElement claimantMaritalStatus;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Claimant's Details\"]/following::span[30]/span")
	public WebElement claimantZipcode;
	
	@FindBy(xpath = "//span[text()=\"Dependents at Time of Injury?\"]/following::span[1]/span")
	public WebElement claimantDependent;
	
	@FindBy(xpath = "//span[text()=\"Match Flag\"]/following::span[1]/span")
	public WebElement claimantMatchFlag;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Employer's Details\"]/following::span[2]/span")
	public WebElement employer;
		
	@FindBy(xpath = "//h3/button/span[text()=\"Employer's Details\"]/following::span[6]/span")
	public WebElement employerSupervisor;
	
	@FindBy(xpath = "//div[1]/span[text()='Employer Address']/following::span[2]") //h3/button/span[text()=\"Employer's Details\"]/following::span[18]/span
	public WebElement employerAdd;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Employer's Details\"]/following::span[22]/span")
	public WebElement employerCity;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Employer's Details\"]/following::span[26]/span")
	public WebElement employerState;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Employer's Details\"]/following::span[30]/span")
	public WebElement employerZipcode;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Occupation Details\"]/following::span[2]/span")
	public WebElement occupationDateHired;
	
	@FindBy(xpath = "//div[1]/span[text()='Where Hired']/following::span[2]")  //h3/button/span[text()=\"Occupation Details\"]/following::span[6]/span
	public WebElement occupWhereHrd;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Occupation Details\"]/following::span[10]/span")
	public WebElement occupation;

	@FindBy(xpath = "//h3/button/span[text()=\"Occupation Details\"]/following::span[14]/span")
	public WebElement hoursWorkedPerDay;
	
	@FindBy(xpath = "//div[1]/span[text()=\"Per Week\"]/following::span[2]")
	public WebElement perWeek;
	
	@FindBy(xpath = "//div[1]/span[text()=\"Address or Location of Accident\"]/following::span[2]")
	public WebElement injuryLocation;
	
	@FindBy(xpath = "//div[1]/span[text()=\"To Whom?\"]/following::span[2]")
	public WebElement toWhom;
	
	@FindBy(xpath = "//div[1]/span[text()=\"Title\"]/following::span[2]")
	public WebElement injuryTitle;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Treatment Details\"]/following::span[2]/span")
	public WebElement treatmentName1;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Treatment Details\"]/following::span[6]/span")
	public WebElement treatmentName2;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Treatment Details\"]/following::span[10]/span")
	public WebElement treatmentAddress1;
	
	@FindBy(xpath = "//h3/button/span[text()=\"Treatment Details\"]/following::span[14]/span")
	public WebElement treatmentAddress2;
	

	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document407Page(WebDriver driver) {
		super(driver);
	}
	
	public void verify407Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.highLightElement(clmntFrstName, driver);
		String clmntFstNam = clmntFrstName.getText();
		TestUtils.compareText(clmntFstNam, GoogleSheetAPI.ReadData(methodName, tcName, "claimant first name"), clmntFrstName, driver);
		
		SeleniumUtils.highLightElement(middleInitial, driver);
		String middleInit = middleInitial.getText();
		TestUtils.compareText(middleInit, GoogleSheetAPI.ReadData(methodName, tcName, "claimant middle initial"), middleInitial, driver);
		
		SeleniumUtils.highLightElement(socialSecurity, driver);
		String socialSec = socialSecurity.getText();
		TestUtils.compareText(socialSec, GoogleSheetAPI.ReadData(methodName, tcName, "claimant social security number"), socialSecurity, driver);
				
		SeleniumUtils.highLightElement(claimantAddress, driver);
		String claimantAdd = claimantAddress.getText();
		TestUtils.compareText(claimantAdd, GoogleSheetAPI.ReadData(methodName, tcName, "claimant address"), claimantAddress, driver);
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		String claimantCty = claimantCity.getText();
		TestUtils.compareText(claimantCty, GoogleSheetAPI.ReadData(methodName, tcName, "claimant city"), claimantCity, driver);
		
		SeleniumUtils.highLightElement(claimantMaritalStatus, driver);
		String claimantMarStat = claimantMaritalStatus.getText();
		TestUtils.compareText(claimantMarStat, GoogleSheetAPI.ReadData(methodName, tcName, "claimant martial status"), claimantMaritalStatus, driver);
		
		SeleniumUtils.highLightElement(claimantZipcode, driver);
		String claimantzipcode = claimantZipcode.getText();
		TestUtils.compareText(claimantzipcode, GoogleSheetAPI.ReadData(methodName, tcName, "claimant zip code"), claimantZipcode, driver);
		
		SeleniumUtils.highLightElement(claimantDependent, driver);
		String claimantDepndnt = claimantDependent.getText();
		TestUtils.compareText(claimantDepndnt, GoogleSheetAPI.ReadData(methodName, tcName, "claimant dependents time"), claimantDependent, driver);
		
		SeleniumUtils.highLightElement(employer, driver);
		String emp = employer.getText();
		TestUtils.compareText(emp, GoogleSheetAPI.ReadData(methodName, tcName, "employer"), employer, driver);
		
		SeleniumUtils.highLightElement(employerSupervisor, driver);
		String supr = employerSupervisor.getText();
		TestUtils.compareText(supr, GoogleSheetAPI.ReadData(methodName, tcName, "suprvisor"), employerSupervisor, driver);
		
		SeleniumUtils.highLightElement(employerAdd, driver);
		String empAddr = employerAdd.getText();
		TestUtils.compareText(empAddr, GoogleSheetAPI.ReadData(methodName, tcName, "employerAddress"), employerAdd, driver);
		
		SeleniumUtils.highLightElement(employerCity, driver);
		String empCty = employerCity.getText();
		TestUtils.compareText(empCty, GoogleSheetAPI.ReadData(methodName, tcName, "employerCity"), employerCity, driver);
		
		SeleniumUtils.highLightElement(employerState, driver);
		String empstat = employerState.getText();
		TestUtils.compareText(empstat, GoogleSheetAPI.ReadData(methodName, tcName, "employerState"), employerState, driver);
		
		SeleniumUtils.highLightElement(employerZipcode, driver);
		String empZip = employerZipcode.getText();
		TestUtils.compareText(empZip, GoogleSheetAPI.ReadData(methodName, tcName, "employerZipCode"), employerZipcode, driver);
		
		SeleniumUtils.highLightElement(occupWhereHrd, driver);
		String occuWhere = occupWhereHrd.getText();
		TestUtils.compareText(occuWhere, GoogleSheetAPI.ReadData(methodName, tcName, "occupationWhereHire"), occupWhereHrd, driver);
		
		SeleniumUtils.highLightElement(occupation, driver);
		String occu = occupation.getText();
		TestUtils.compareText(occu, GoogleSheetAPI.ReadData(methodName, tcName, "occupation"), occupation, driver);
		
		SeleniumUtils.highLightElement(hoursWorkedPerDay, driver);
		String hoursWrkd = hoursWorkedPerDay.getText();
		TestUtils.compareText(hoursWrkd, GoogleSheetAPI.ReadData(methodName, tcName, "occupationWorkHours"), hoursWorkedPerDay, driver);
		
		SeleniumUtils.highLightElement(perWeek, driver);
		String perWek = perWeek.getText();
		TestUtils.compareText(perWek, GoogleSheetAPI.ReadData(methodName, tcName, "occupationPerWeek"), perWeek, driver);
		
		SeleniumUtils.highLightElement(injuryLocation, driver);
		String injuryLoc = injuryLocation.getText();
		TestUtils.compareText(injuryLoc, GoogleSheetAPI.ReadData(methodName, tcName, "injuryLocation"), injuryLocation, driver);
		
		SeleniumUtils.highLightElement(toWhom, driver);
		String injryToWh = toWhom.getText();
		TestUtils.compareText(injryToWh, GoogleSheetAPI.ReadData(methodName, tcName, "injuryToWhom"), toWhom, driver);
		
		SeleniumUtils.highLightElement(injuryTitle, driver);
		String injryTit = injuryTitle.getText();
		TestUtils.compareText(injryTit, GoogleSheetAPI.ReadData(methodName, tcName, "injuryTitle"), injuryTitle, driver);
		
		SeleniumUtils.highLightElement(treatmentName1, driver);
		String trtmntNam1 = treatmentName1.getText();
		TestUtils.compareText(trtmntNam1, GoogleSheetAPI.ReadData(methodName, tcName, "treatmentName1"), treatmentName1, driver);
		
		SeleniumUtils.highLightElement(treatmentName2, driver);
		String trtmntNam2 = treatmentName2.getText();
		TestUtils.compareText(trtmntNam2, GoogleSheetAPI.ReadData(methodName, tcName, "treatmentName2"), treatmentName2, driver);
		
		SeleniumUtils.highLightElement(treatmentAddress1, driver);
		String trtmntAdd1 = treatmentAddress1.getText();
		TestUtils.compareText(trtmntAdd1, GoogleSheetAPI.ReadData(methodName, tcName, "treatmentAddress1"), treatmentAddress1, driver);
		
		SeleniumUtils.highLightElement(treatmentAddress2, driver);
		String trtmntAdd2 = treatmentAddress2.getText();
		TestUtils.compareText(trtmntAdd2, GoogleSheetAPI.ReadData(methodName, tcName, "treatmentAddress2"), treatmentAddress2, driver);
		
		
	}

}
