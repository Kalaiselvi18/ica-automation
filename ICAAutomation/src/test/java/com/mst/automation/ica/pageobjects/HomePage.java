package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.smoketestcases.FormPriorsPage;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To check Home page tab
 *
 */
public class HomePage extends DriverClass {

	By homeBy = By.xpath("//span[contains(.,'Home')]");

	@FindBy(xpath = "//span[contains(.,'Home')]")
	public WebElement homeLink;

	@FindBy(css = "#userNav-arrow")
	public WebElement userMenu;

	@FindBy(css = "a[title=Logout]")
	public WebElement logOut;

	@FindBy(how = How.CSS, using = "svg[data-key='setup']")
	public WebElement setupIcon;

	public By setupIconHigh() {
		By iconSet = By.cssSelector("svg[data-key='setup']");
		return iconSet;
	}

	public By setupLink() {
		By linkSet = By.cssSelector("a[title='Setup']");
		return linkSet;
	}

	@FindBy(how = How.CSS, using = "a[title='Setup']")
	public WebElement setupClick;

	public By searchBox() {
		By box = By.cssSelector("input[class='searchBox input']");
		return box;
	}
	
	@FindBy(css = "input[title='Search Salesforce']")
	public WebElement searchSalesforce;
	

	@FindBy(how = How.CSS, using = "input[class='searchBox input']")
	public WebElement setupVerify;
	
	public By welcome() {
		By check = By.xpath("//span[contains(.,'Welcome!')]");
		return check;
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'Welcome!')]")
	public WebElement welcome_high;
	
	@FindBy(how = How.CSS, using = "one-app-launcher-header[class=\"slds-icon-waffle_container\"]")
	public WebElement appMenu;
	
	
	@FindBy(how = How.XPATH, using = "//button[contains(.,'FORMS')]")
	public WebElement forms_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0101')]")
	public WebElement forms101_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0407')]")
	public WebElement forms407_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0102')]")
	public WebElement forms102_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0446')]")
	public WebElement forms446_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0104')]")
	public WebElement forms104_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0106')]")
	public WebElement forms106_click; 
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0107')]")
	public WebElement forms107_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0108')]")
	public WebElement forms108_click; 
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0121')]")
	public WebElement forms121_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0122')]")
	public WebElement forms122_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0529')]")
	public WebElement forms529_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0528')]")
	public WebElement forms528_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'Priors Claim')]")
	public WebElement formsPriors_click;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'0105')]")
	public WebElement forms105_click;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Combine Claims / Delete Claim request']")
	public WebElement formscombinedelete_click;
	
	
	@FindBy(how = How.XPATH, using = "//a/span[text()='Documents']")
	public WebElement docuTab;
	
	@FindBy(how = How.XPATH, using = "//a/span[text()='Claim Tasks']")
	public WebElement claimTaskTab;
	
	
	@FindBy(how = How.XPATH, using = "//li/span[text()='Documents']")
	public WebElement docuList;
	
	@FindBy(how = How.XPATH, using = "//li/a/span[text()=\"Request for a Worker's Compensation Claim\"]")
	public WebElement requestClaimtab;
	
	@FindBy(how = How.XPATH, using = "//li/a/span[text()=\"New Claim Request\"]")
	public WebElement claimrRequesttab;
	
	
	@FindBy(how = How.XPATH, using = "//li/a/span[text()=\"CLAIMS\"]")
	public WebElement claimstab;
	
	
	public By listView() {
		By list = By.xpath("//li/span[text()='Documents']");
		return list;
	}
	
	@FindBy(how = How.XPATH, using = "//h1/a/div/span[text()='Recently Viewed']")
	public WebElement taskClaimList;
	
	public By taskList() {
		By task = By.xpath("//h1/a/div/span[text()='Recently Viewed']");
		return task;
	}
	

	public HomePage(WebDriver driver) {
		super(driver);
	}

	/*To click on the setup menu on the right side corner*/

	public ProfileSetup verifyHomePage1() throws Exception {
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		SeleniumUtils.presenceOfElement(driver, setupIconHigh());
		setupIcon.click();
		SeleniumUtils.presenceOfElement(driver, setupLink());
		setupClick.click();
		SeleniumUtils.switchToTab(driver);
		SeleniumUtils.presenceOfElement(driver, searchBox());
		SeleniumUtils.highLightElement(setupVerify, driver);
		return new ProfileSetup(driver);
	}

	/*To send values to global search*/
	public ClaimsRecordPage globalValuePassing(String methodName, String tcName, ReportGenerator generator) throws Exception {
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		Thread.sleep(2000);
		searchSalesforce.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "record name"),Keys.ENTER);
		generator.childReport("Record name entered");
		return new ClaimsRecordPage(driver);
	}
	public ClaimsRecordPage globalClaimTask(String methodName, String tcName, ReportGenerator generator) throws Exception {
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		Thread.sleep(2000);
		searchSalesforce.sendKeys(ClaimTaskObjectPage.taskno,Keys.ENTER);
		generator.childReport("Record name entered");
		return new ClaimsRecordPage(driver);
	}
	public void loginverify(ReportGenerator generator) throws Exception {
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		generator.childReport("Login Verified");
	}
	public DocumentSelect documentObjClick(ReportGenerator generator) throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		generator.childReport("Login Verified");
		SeleniumUtils.highLightElement(docuTab, driver);
		docuTab.click();
		generator.childReport("Document Tab Clicked");
		SeleniumUtils.elementToBeClickable(driver, listView());
		SeleniumUtils.highLightElement(docuList, driver);
		return new DocumentSelect(driver);
		
	}
	public ClaimTaskSelect claimTaskObjClick(ReportGenerator generator) throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		generator.childReport("Login Verified");
		SeleniumUtils.highLightElement(claimTaskTab, driver);
		claimTaskTab.click();
		generator.childReport("Claim Task Tab Clicked");
		SeleniumUtils.elementToBeClickable(driver, taskList());
		SeleniumUtils.highLightElement(taskClaimList, driver);
		return new ClaimTaskSelect(driver);
		
	}
	public objectVerify appContainer() throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		appMenu.click();
		return new objectVerify(driver);
	}
	public BLSRecordPage verifyHomePage2() throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		SeleniumUtils.highLightElement(homeLink, driver);
		return new BLSRecordPage(driver);
	}
	public Form101Page formUrl101cu() throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, welcome());
		SeleniumUtils.highLightElement(welcome_high, driver);
		forms_click.click();
		SeleniumUtils.highLightElement(forms101_click, driver);
		forms101_click.click();
		return new Form101Page(driver);
	}
	public Form407Page formUrl407cu() throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, welcome());
		SeleniumUtils.highLightElement(welcome_high, driver);
		forms_click.click();
		SeleniumUtils.highLightElement(forms407_click, driver);
		forms407_click.click();
		return new Form407Page(driver);
	}
	public Form102Page formUrl102cu() throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, welcome());
		SeleniumUtils.highLightElement(welcome_high, driver);
		forms_click.click();
		SeleniumUtils.highLightElement(forms102_click, driver);
		forms102_click.click();
		return new Form102Page(driver);
	}
	public Form446Page formUrl446cu() throws Exception {
		Thread.sleep(4000);
		SeleniumUtils.elementToBeClickable(driver, welcome());
		SeleniumUtils.highLightElement(welcome_high, driver);
		forms_click.click();
		SeleniumUtils.highLightElement(forms446_click, driver);
		forms446_click.click();
		return new Form446Page(driver);
	}
	
	public Form104Page formUrl104cu() throws Exception {
		SeleniumUtils.elementToBeClickable(driver, welcome());
		SeleniumUtils.highLightElement(welcome_high, driver);
		forms_click.click();
		SeleniumUtils.highLightElement(forms104_click, driver);
		forms104_click.click();
		return new Form104Page(driver);
	}
	
	
public Form106Page formUrl106cu() throws Exception {
		SeleniumUtils.elementToBeClickable(driver, welcome());
		SeleniumUtils.highLightElement(welcome_high, driver);
		forms_click.click();
		SeleniumUtils.highLightElement(forms106_click, driver);
		forms106_click.click();
		return new Form106Page(driver);
	} 

public Form107Page formUrl107cu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(forms107_click, driver);
	forms107_click.click();
	return new Form107Page(driver);
}

public Form108Page formUrl108cu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(forms108_click, driver);
	forms108_click.click();
	return new Form108Page(driver);
} 
public Form121Page formUrl121cu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(forms121_click, driver);
	forms121_click.click();
	return new Form121Page(driver);
}
public Form122Page formUrl122cu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(forms122_click, driver);
	forms122_click.click();
	return new Form122Page(driver);
}


public Form529Page formUrl529cu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(forms529_click, driver);
	forms529_click.click();
	return new Form529Page(driver);
}
public Form528Page formUrl528cu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(forms528_click, driver);
	forms528_click.click();
	return new Form528Page(driver);
}
public FormPriorsPage formUrlpriorscu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(formsPriors_click, driver);
	formsPriors_click.click();
	return new FormPriorsPage(driver);
}
public Form105Page formUrl105cu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(forms105_click, driver);
	forms105_click.click();
	return new Form105Page(driver);
}
public FormCombineDeletePage formUrlclaimcombinecu() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	forms_click.click();
	SeleniumUtils.highLightElement(formscombinedelete_click, driver);
	formscombinedelete_click.click();
	return new FormCombineDeletePage(driver);
}
public WorkersClaimPage requestworker() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	SeleniumUtils.highLightElement(requestClaimtab, driver);
	requestClaimtab.click();
	return new WorkersClaimPage(driver);
}
public NewClaimRequestPage requestClaim() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	SeleniumUtils.highLightElement(claimrRequesttab, driver);
	claimrRequesttab.click();
	return new NewClaimRequestPage(driver);
}
public ClaimTabPage claimStatusVerify() throws Exception {
	SeleniumUtils.elementToBeClickable(driver, welcome());
	SeleniumUtils.highLightElement(welcome_high, driver);
	SeleniumUtils.highLightElement(claimstab, driver);
	claimstab.click();
	return new ClaimTabPage(driver);
}


	/*To click the logout button*/
	public void logout() throws Exception {
		SeleniumUtils.highLightElement(userMenu, driver);
		userMenu.click();
		logOut.click();
	}
}
