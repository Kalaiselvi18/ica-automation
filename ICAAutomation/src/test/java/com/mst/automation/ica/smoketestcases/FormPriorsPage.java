package com.mst.automation.ica.smoketestcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

public class FormPriorsPage extends DriverClass{
	
	public By element() {
		By ul=By.xpath("html/body/form/div[1]/div/div[2]");
		return ul;
	}
	
	@FindBy(xpath = "html/body/form/div[1]/div/div[2]")
	public WebElement icaPriorsVerify;
	
	@FindBy(xpath = "//label[text()='Claimant First Name']/following::input[1]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::input[1]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//label[text()='Date of Injury:']/following::a[1]")
	public WebElement doi;
	
	@FindBy(xpath = "//label[text()='Date of Birth:']/following::a[1]")
	public WebElement dob;
	
	@FindBy(xpath = "//label[text()='Employer:']/following::input[1]")
	public WebElement employer;
	
	@FindBy(xpath = "//label[text()='ICA Claim No.:']/following::input[1]")
	public WebElement claimnumber;
	
	@FindBy(xpath = "//label[text()='Requester:']/following::input[1]")
	public WebElement requester;
	
	@FindBy(xpath = "//label[text()='Date:']/following::a[1]")
	public WebElement date;
	
	@FindBy(xpath = "//label[text()='Address:']/following::textarea[1]")
	public WebElement address;
	
	@FindBy(xpath = "//label[text()='Telephone no.']/following::input[1]")
	public WebElement telephoneno;
	
	@FindBy(xpath = "//label[text()='Email Address:']/following::input[1]")
	public WebElement email;
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifySuccess;

	
	
	public FormPriorsPage(WebDriver driver) {
		super(driver);
	}
	public void fillingFormPriorsCommunity(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		generator.childReport("Signature frame switched");
	
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(icaPriorsVerify, driver);
		generator.childReport("Priors Claim form name verified");
		
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantFirstName"));
		generator.childReport("Claim First name Entered");
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantLastName"));
		generator.childReport("Claim Last name Entered");
		doi.click();
		generator.childReport("Date of Injury Entered");
		dob.click();
		generator.childReport("Date of Birth Entered");
		employer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employer"));
		generator.childReport("Employer Entered");
		claimnumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimnumber"));
		generator.childReport("Claimant Entered");
		requester.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requester"));
		generator.childReport("Requester Entered");
		date.click();
		generator.childReport("Date Entered");
		address.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "address"));
		generator.childReport("Address Entered");
		telephoneno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "telephoneno"));
		generator.childReport("Telephone Number Entered");
		email.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "email"));
		generator.childReport("Email Entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);
		
	}

}
