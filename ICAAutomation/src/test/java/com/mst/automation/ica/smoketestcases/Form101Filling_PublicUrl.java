/**
 * 
 */
package com.mst.automation.ica.smoketestcases;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

/**
 * @author Aartheeswaran
 * Created date: Nov 30, 2017
 * Last Edited by: Aartheeswaran...
 * Last Edited date: Nov 30, 2017
 * Description: 
 *
 */
public class Form101Filling_PublicUrl extends BaseTest {
	

@Test (groups = "Form Regression Suite")
	
	public void formfilling101() throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_101";
		
		reporter = new ReportGenerator(getbrowser(), className);
		page101 = loginPage.formUrl101();
		
		page101.fillingForm101(methodName, tcName, reporter);

	}

}
