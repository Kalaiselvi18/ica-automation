package com.mst.automation.ica.utils;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.constant.Constant;
import com.mst.automation.ica.customexception.CustomException;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This class contains the common selenium methods like dropdown, multipicklist,
 * explicit wait, highlight element etc.,
 */
public final class SeleniumUtils {
	
	private SeleniumUtils() {
		
	}

	private static ConcurrentHashMap<Long, DriverClass> currentPages = new ConcurrentHashMap<Long, DriverClass>();
	
	public static DriverClass getCurrentPage(){
		return currentPages.get(Thread.currentThread().getId());
	}
	
	public static WebElement presenceOfElement(WebDriver driver,By by){
		try {
        return new WebDriverWait(driver, 120).until(ExpectedConditions.presenceOfElementLocated(by));
	}
		catch(Exception e) {
			throw new CustomException("Element not available");
		}
	}
	
	public static WebElement elementToBeClickable(WebDriver driver,By by){
		 try {
	        return new WebDriverWait(driver, 30)
	                       .until(ExpectedConditions.elementToBeClickable(by));
		 }
		 catch(Exception e) {
			 throw new CustomException("Element not clickable");
			 
		 }
	 }
	
	public static void highlightElementBasedOnResult(WebElement element, String expectedValue,WebDriver driver) throws InterruptedException {
		String actualValue = element.getText();
		if (actualValue.equals(expectedValue)) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			highLightElement(element, driver);
		} else {
			highLightFailedElement(element, driver);
			throw new CustomException(
					"The value " + actualValue + "is not matched with the given value " + expectedValue);
		}
	}
	
	 public static void dropDown(WebElement element, String salutationValue){          
		 try {
		        Select dropdown = new Select(element);
		        dropdown.selectByVisibleText(salutationValue);
			  }
		 catch(Exception e) {
			 throw new CustomException("Dropdown element not found");
		 }
	 }
	 
	 public static void highLightElement(WebElement element ,WebDriver driver) throws InterruptedException{
		 try {
		scroll(element,driver);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].setAttribute('style','border: 1.5px solid green;');", element);
		TimeUnit.MILLISECONDS.sleep(100);
		 }
		 catch(Exception e) {
			 throw new CustomException(e.getMessage());
		 }
	}
		
	 public static void highLightFailedElement(WebElement element,WebDriver driver) throws InterruptedException{
		 try {
		scroll(element,driver);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].setAttribute('style','background: yellow;border: 1.5px solid red;');", element);
		TimeUnit.MILLISECONDS.sleep(500);
		 }
		 catch(Exception e) {
			 throw new CustomException(e.getMessage());
		 }
	}
		
	public static void scroll(WebElement element,WebDriver driver){
		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
		}
		catch(Exception e) {
			 throw new CustomException("Mousehover not performed");
		}
	}
	
	public static void scrollup(WebDriver driver){
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("scroll(0, -250);");
		}
		catch(Exception e) {
			 throw new CustomException("Mousehover not performed");
		}
	}
	public static void scrollDown(WebDriver driver){
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,250)","");
		}
		catch(Exception e) {
			 throw new CustomException("Mousehover not performed");
		}
	}  
	
	public static boolean switchToContainerFrame(WebDriver driver, List<WebElement> frameElements){
		try {
		for(WebElement element : frameElements) {
			
			if(element.isDisplayed()){
				String className = element.getAttribute("class");
				if(!className.contains("x-hide-display")) {
					driver.switchTo().frame(element.findElement(By.tagName("iframe")));
					return true;
				}
			}
		}
		return false;
		}
		catch(Exception e) {
			throw new CustomException("Container frame not found");
		}
	}
	
	public static void switchToDefaultcontent(WebDriver driver){
		try {
			driver.switchTo().defaultContent();
		}
		catch(Exception e) {
			throw new CustomException("Default content not switched");
		}
	}
	
	public static void switchToFrame(WebDriver driver, WebElement frame){
		try {
		WebDriverWait driverWait = new WebDriverWait(driver,
				TestUtils.getIntegerFromPropertyFile("driver.explicit.wait"));
		driverWait.until(ExpectedConditions.visibilityOf(frame));
		driver.switchTo().frame(frame);
		}
		catch(Exception e) {
			throw new CustomException("Excpected frame not found");
		}
	}
	public static void switchToTab(WebDriver driver) {
		try {
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles()); 
			driver.switchTo().window(tabs.get(1));
		}
		catch(Exception e) {
			throw new CustomException("Tab not found");
		}
	}
	
	public static void switchToCurrentWindow(WebDriver driver) {
		try {
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles()); 
			driver.switchTo().window(tabs.get(0));
		}
		catch(Exception e) {
			throw new CustomException("Tab not found");
		}
	}
	
	public static void frameSwitch(int i, WebDriver driver){
		try {
		driver.switchTo().frame(i);
		}
		catch(Exception e) {
			throw new CustomException("Excpected frame not found");
		}

	}
	
	public static void verifyPermissionSet(WebElement objectElement, String value, WebDriver driver) {
		try {
		String[] expected = value.split("\\,");
		List<WebElement> tds = objectElement.findElements(By.xpath("td/img"));
		
		int tdCount = tds.size();
		String[] actual = new String[tdCount];
		
		for(int i =0; i<tdCount ;i++) {
			String checkboxTitleValue = tds.get(i).getAttribute("title");
			if(checkboxTitleValue.equals("Checked"))
				actual[i] = "Y";
			else if(checkboxTitleValue.equals("Not Checked"))
				actual[i] = "N";
		}
		Assert.assertEquals(actual, expected, "The permission set values are not equal");
		}
		catch(Exception e) {
			throw new CustomException("Permission values not matched");
		}
	}

	public static void multiPicklist(WebElement preferredColleges, WebElement selectionArea, String readData) {
		String[] multiSelectValues = readData.split(",");
		
		for(String valuesToBeSelected:multiSelectValues) {
			dropDown(preferredColleges, valuesToBeSelected);
			selectionArea.click();
		}
	}
	
	public static void alertAccept(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	public static void switchToNewTab(WebDriver driver, String url) {
		
		driver.findElement(By.cssSelector("Body")).sendKeys(Keys.CONTROL+"t");
		driver.get(url);
	}

	public static void upload() throws Exception{
		Thread.sleep(3000);
		String path = Constant.UPLOADFILE;
		Runtime.getRuntime().exec(path);
	} 
	public static String randomEmail(){
	
	Random random = new Random();
	int number = random.nextInt(100000);
	String mail = "@example.com";
	String randoms = String.format("%03d", number);
	//"random-" + UUID.randomUUID().toString() + "@example.com"
	return "automation"+randoms+mail;
	
	}
}









