package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class WorkersCompensationClaimantType extends BaseTest{

	@Test (groups = "Regression Suite")
	
		
		public void claimantrequest() throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_043";

		
		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.signup();
		workerClaimPage = homePage.requestworker();
		workerClaimPage.fillingClaimant(methodName, tcName, reporter);
	}


}
