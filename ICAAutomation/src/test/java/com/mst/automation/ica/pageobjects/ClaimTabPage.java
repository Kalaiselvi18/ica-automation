package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;

public class ClaimTabPage extends DriverClass{
	
	@FindBy(xpath = "//h2[text()='Claims']")
	public WebElement verifyClaimsPage;
	
	public By element() {
		By ul=By.xpath("//h2[text()='Claims']");
		return ul;
	}
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[text()='20180131958']")
	public WebElement claimNumberVerify;
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[4]")
	public WebElement statusVerify;
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[1]/button[text()='Show Details']")
	public WebElement showDetailsButton;
	
	public By claimDetails() {
		By claim=By.xpath("//b[text()='Claim Details']");
		return claim;
	}
	
	@FindBy(xpath = "//b[text()='Claim Details']")
	public WebElement detailPageVerify;
	
	@FindBy(xpath = "//label[text()=' Claimant Name ']/following::span[1]")
	public WebElement claimsName;
	
	@FindBy(xpath = "//div[2]/div/div[3]/button/span[text()='Close']")
	public WebElement closeClicked;
	
	public By buttonRemove() {
		By remove=By.xpath("//b[text()='Claim Details']");
		return remove;
	}
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[7]/button[text()='Remove']")
	public WebElement removeClicked;
	
	@FindBy(xpath = "//button[text()='Upload Docs']")
	public WebElement uploadDocusClicked;
	
	public By upload() {
		By page=By.xpath("//div[text()='Claims File Upload']");
		return page;
	}
	
	@FindBy(xpath = "//div[text()='Claims File Upload']")
	public WebElement uploadDocusPopup;
	
	@FindBy(xpath = "//label/span[text()='Upload Files']")
	public WebElement clickUpload;	
	
	public By fileupload() {
		By file=By.xpath("//h2[text()='Upload Files']");
		return file;
	}
	
	@FindBy(xpath = "//h2[text()='Upload Files']")
	public WebElement uploadFile;	
	
	public By tickGreen() {
		By tick=By.xpath("//h2[text()='Upload Files']/following::span[14]");
		return tick;
	}
	
	@FindBy(xpath = "//button/span[text()='Done']")
	public WebElement doneClicked;
	
	public By success() {
		By message=By.xpath("//h3[text()='File Successsfully Uploaded']");
		return message;
	}
	
	@FindBy(xpath = "//h3[text()='File Successsfully Uploaded']/following::button")
	public WebElement uploadClose;
	

	public ClaimTabPage(WebDriver driver) {
		super(driver);
	}
	
	/*To verify the table got approved claim task*/
	public void verifyClaimRequestApprove(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(verifyClaimsPage, driver);
		generator.childReport("Claims Page Verified");
		
		SeleniumUtils.highLightElement(claimNumberVerify, driver);
		generator.childReport("Approves Status Verified");
		
		SeleniumUtils.highLightElement(statusVerify, driver);
		
		showDetailsButton.click();
		generator.childReport("Show Details Button Clicked");
		
		SeleniumUtils.presenceOfElement(driver, claimDetails());
		SeleniumUtils.highLightElement(detailPageVerify, driver);
		generator.childReport("Claims Detail Page Verified");
		
		SeleniumUtils.highLightElement(claimsName, driver);
		generator.childReport("Claims Name Verified");
		
		closeClicked.click();
		generator.childReport("Close Button Clicked");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, buttonRemove());
		removeClicked.click();
		generator.childReport("Remove Button Clicked");
		Thread.sleep(5000);
		SeleniumUtils.alertAccept(driver);
		generator.childReport("Alert Clicked");
		
	}
	
	/*To verify the table got more information claim task*/

public void verifyClaimRequestMoreInformation(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(verifyClaimsPage, driver);
		generator.childReport("Claims Page Verified");
		
		SeleniumUtils.highLightElement(claimNumberVerify, driver);
		generator.childReport("Approves Status Verified");
		
		SeleniumUtils.highLightElement(statusVerify, driver);
		
		uploadDocusClicked.click();
		
		SeleniumUtils.presenceOfElement(driver, upload());
		SeleniumUtils.highLightElement(uploadDocusPopup, driver);
		
		SeleniumUtils.highLightElement(clickUpload, driver);
		clickUpload.click();
		
		SeleniumUtils.upload();
		generator.childReport("Upload Successfully");
		
		SeleniumUtils.presenceOfElement(driver, fileupload());
		SeleniumUtils.highLightElement(uploadFile, driver);
		SeleniumUtils.presenceOfElement(driver, tickGreen());
		Thread.sleep(5000);
		doneClicked.click();
		SeleniumUtils.presenceOfElement(driver, success());
		
		uploadClose.click();
		generator.childReport("Close Button Clicked");
		Thread.sleep(5000);
		
		SeleniumUtils.highLightElement(statusVerify, driver);
		
		SeleniumUtils.presenceOfElement(driver, buttonRemove());
		removeClicked.click();
		generator.childReport("Remove Button Clicked");
		Thread.sleep(5000);
		SeleniumUtils.alertAccept(driver);
		generator.childReport("Alert Clicked");
		
	}

/*To verify the table got Close-Denied Status*/
public void verifyClaimRequestDenied(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	SeleniumUtils.presenceOfElement(driver, element());
	SeleniumUtils.highLightElement(verifyClaimsPage, driver);
	generator.childReport("Claims Page Verified");
	
	SeleniumUtils.highLightElement(claimNumberVerify, driver);
	generator.childReport("Approves Status Verified");
	
	SeleniumUtils.highLightElement(statusVerify, driver);
	
	SeleniumUtils.presenceOfElement(driver, buttonRemove());
	removeClicked.click();
	generator.childReport("Remove Button Clicked");
	Thread.sleep(5000);
	SeleniumUtils.alertAccept(driver);
	generator.childReport("Alert Clicked");
	
}

}

