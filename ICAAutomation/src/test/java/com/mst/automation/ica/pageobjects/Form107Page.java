/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Aartheeswaran
 * Created date: Jan 19, 2018
 * Last Edited by: Aartheeswaran...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form107Page extends DriverClass {
	
	@FindBy(xpath = "//th/label[contains(.,'ICA Claim No')]/following::input[1]")
	public WebElement icaClaimNo;
	
	@FindBy(xpath = "//tr[1]/td[1]/div/div[1]/h3/following::input[1]")
	public WebElement carrierName;
	
	@FindBy(xpath = "//tr[1]/td[1]/div/div[1]/h3/following::textarea[1]")
	public WebElement carrierStreet;
	
	@FindBy(xpath = "//tr[1]/td[1]/div/div[1]/h3/following::input[2]")
	public WebElement carrierCity;
	
	@FindBy(xpath = "//tr[1]/td[1]/div/div[1]/h3/following::input[3]")
	public WebElement carrierState;
	
	@FindBy(xpath = "//tr[1]/td[1]/div/div[1]/h3/following::input[4]")
	public WebElement carrierZipcode;
	
	@FindBy(xpath = "//th/label[contains(.,'ICA Claim No')]/following::a[1]")
	public WebElement carrierDateInjured;
	
	@FindBy(xpath = "//tr[2]/td[1]/div/div[1]/h3/following::input[5]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//tr[2]/td[1]/div/div[1]/h3/following::input[6]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//tr[2]/td[1]/div/div[1]/h3/following::textarea[2]")
	public WebElement claimantStreet;
	
	@FindBy(xpath = "//tr[2]/td[1]/div/div[1]/h3/following::input[7]")
	public WebElement claimantCity;
	
	@FindBy(xpath = "//tr[2]/td[1]/div/div[1]/h3/following::input[8]")
	public WebElement claimantState;
	
	@FindBy(xpath = "//tr[2]/td[1]/div/div[1]/h3/following::input[9]")
	public WebElement claimantZipcode;
	
	@FindBy(xpath = "//label[contains(.,'Mailed On:')]/following::a[1]")
	public WebElement mailedOn;
	
	@FindBy(xpath = "//label[contains(.,'Mailed On:')]/following::input[2]")
	public WebElement mailedBy;
	
	@FindBy(xpath = "//label[contains(.,'Mailed On:')]/following::input[3]")
	public WebElement authorizedRepresentative;	
	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public By element() {
		By ul=By.xpath("//div[1]/div/b[contains(.,'NOTICE OF PERMANENT DISABILITY AND REQUEST FOR DETERMINATION OF BENEFITS')]");
		return ul;
	}
	
	@FindBy(xpath = "//div[1]/div/b[contains(.,'NOTICE OF PERMANENT DISABILITY AND REQUEST FOR DETERMINATION OF BENEFITS')]")
	public WebElement ica107Verify;
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;

	
	
	public Form107Page(WebDriver driver){
		 super(driver);
	 }
	
	
	public void fillingForm107Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica107Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		icaClaimNo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "ICA Claim No"));
		generator.childReport("ICA Claim No Entered");
		
		SeleniumUtils.highLightElement(carrierName, driver);
		carrierName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Carrier Name"));
		generator.childReport("Carrier or Self-Insured Name Entered");
		
		SeleniumUtils.highLightElement(carrierStreet, driver);
		carrierStreet.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Carrier Street"));
		generator.childReport("Carrier or Self-Insured Street Entered");
		
		SeleniumUtils.highLightElement(carrierCity, driver);
		carrierCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Carrier City"));
		generator.childReport("Carrier or Self-Insured City Entered");
		
		SeleniumUtils.highLightElement(carrierState, driver);
		carrierState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Carrier State"));
		generator.childReport("Carrier or Self-Insured State Entered");
		
		SeleniumUtils.highLightElement(carrierZipcode, driver);
		carrierZipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Carrier Zipcode"));
		generator.childReport("Carrier or Self-Insured Zipcode Entered");
		
		SeleniumUtils.highLightElement(carrierDateInjured, driver);
		carrierDateInjured.click();
		generator.childReport("Carrier or Self-Insured Date Injured Clicked");
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Claimant First Name"));
		generator.childReport("Claimant's First Name Entered");
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Claimant Last Name"));
		generator.childReport("Claimant's Last Name Entered");
		
		SeleniumUtils.highLightElement(claimantStreet, driver);
		claimantStreet.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Claimant Street"));
		generator.childReport("Claimant's Street Entered");
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		claimantCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Claimant City"));
		generator.childReport("Claimant's City Entered");
		
		SeleniumUtils.highLightElement(claimantState, driver);
		claimantState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Claimant State"));
		generator.childReport("Claimant's State Entered");
		
		SeleniumUtils.highLightElement(claimantZipcode, driver);
		claimantZipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Claimant Zipcode"));
		generator.childReport("Claimant's Zipcode Entered");
		
		SeleniumUtils.highLightElement(mailedOn, driver);
		mailedOn.click();
		generator.childReport("Mailed on Clicked");
		
		SeleniumUtils.highLightElement(mailedBy, driver);
		mailedBy.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Mailed By"));
		generator.childReport("Mailed By Entered");
		
		SeleniumUtils.highLightElement(authorizedRepresentative, driver);
		authorizedRepresentative.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Authorized Representative"));
		generator.childReport("Authorized Representative Entered");		
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);
		
		
		
	}

}
