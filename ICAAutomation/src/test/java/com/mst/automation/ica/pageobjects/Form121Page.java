/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall Created date: 17-Jan-2018 Last Edited by: Infant
 *         Raja Marshall... Last Edited date: Description:
 *
 */
public class Form121Page extends DriverClass {

	@FindBy(xpath = "//form/div[1]/div[2]")
	public WebElement ica121Verify;

	public By element() {
		By open = By.xpath("//form/div[1]/div[2]");
		return open;
	}

	@FindBy(xpath = "//label[text()='Claimant First Name']/following::input[1]")
	public WebElement claimantfirstname;

	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::input[1]")
	public WebElement claimantlastname;

	@FindBy(xpath = "//label[text()='ICA Claim #:']/following::input[1]")
	public WebElement icaclaimno;

	@FindBy(xpath = "//label[text()='Date of Injury:']/following::a[1]")
	public WebElement dateofinjury;

	@FindBy(xpath = "//label[text()='Reason for Requesting Change of Doctors']/following::textarea[1]")
	public WebElement reasondetails;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(From)')]/following::input[1]")
	public WebElement providerdoctorname;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(From)')]/following::textarea[1]")
	public WebElement provideraddress;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(From)')]/following::input[2]")
	public WebElement providercity;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(From)')]/following::input[3]")
	public WebElement providerstate;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(From)')]/following::input[4]")
	public WebElement providerzipcode;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(From)')]/following::input[5]")
	public WebElement providerphone;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(To)')]/following::input[1]")
	public WebElement providerToname;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(To)')]/following::textarea[1]")
	public WebElement providerToaddress;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(To)')]/following::input[2]")
	public WebElement providerTocity;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(To)')]/following::input[3]")
	public WebElement providerTostate;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(To)')]/following::input[4]")
	public WebElement providerTozipcode;

	@FindBy(xpath = "//h3[contains(.,'Provider Details(To)')]/following::input[5]")
	public WebElement providerTophone;

	public By iframe() {
		By user = By.cssSelector("iframe[class='cke_wysiwyg_frame cke_reset']");
		return user;
	}

	@FindBy(css = "iframe[class='cke_wysiwyg_frame cke_reset']")
	public WebElement frame;

	@FindBy(xpath = "html/body")
	public WebElement workerDescribe;

	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::a[16]")
	public WebElement authenticationdate;

	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[2]")
	public WebElement submitteremail;

	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::textarea[2]")
	public WebElement claimantaddress;

	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[3]")
	public WebElement authenticationcity;

	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[4]")
	public WebElement authenticationstate;

	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[5]")
	public WebElement authenticationzipcode;

	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[6]")
	public WebElement authenticationphone;

	@FindBy(css = "input[type='Submit']")
	public WebElement submit;

	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}

	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;

	public By community() {
		By user = By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}

	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;

	public Form121Page(WebDriver driver) {
		super(driver);
	}

	public void fillingForm121(String methodName, String tcName, ReportGenerator generator) throws Exception {

		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica121Verify, driver);
		generator.childReport("ICA form name verified");

		SeleniumUtils.highLightElement(claimantfirstname, driver);
		claimantfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"));
		generator.childReport("claimantfirstname entered");

		SeleniumUtils.highLightElement(claimantlastname, driver);
		claimantlastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantlastname"));
		generator.childReport("claimantlastname entered");

		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");

		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");

		SeleniumUtils.highLightElement(reasondetails, driver);
		reasondetails.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "reasondetails"));
		generator.childReport("reasondetails entered");

		SeleniumUtils.highLightElement(providerdoctorname, driver);
		providerdoctorname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerdoctorname"));
		generator.childReport("providerdoctorname entered");

		SeleniumUtils.highLightElement(provideraddress, driver);
		provideraddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "provideraddress"));
		generator.childReport("provideraddress entered");

		SeleniumUtils.highLightElement(providercity, driver);
		providercity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providercity"));
		generator.childReport("providercity entered");

		SeleniumUtils.highLightElement(providerstate, driver);
		providerstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerstate"));
		generator.childReport("providerstate entered");

		SeleniumUtils.highLightElement(providerzipcode, driver);
		providerzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerzipcode"));
		generator.childReport("providerzipcode entered");

		SeleniumUtils.highLightElement(providerphone, driver);
		providerphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerphone"));
		generator.childReport("providerphone entered");

		SeleniumUtils.highLightElement(providerToname, driver);
		providerToname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerToname"));
		generator.childReport("providerToname entered");

		SeleniumUtils.highLightElement(providerToaddress, driver);
		providerToaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerToaddress"));
		generator.childReport("providerToaddress entered");

		SeleniumUtils.highLightElement(providerTocity, driver);
		providerTocity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTocity"));
		generator.childReport("providerTocity entered");

		SeleniumUtils.highLightElement(providerTostate, driver);
		providerTostate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTostate"));
		generator.childReport("providerTostate entered");

		SeleniumUtils.highLightElement(providerTozipcode, driver);
		providerTozipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTozipcode"));
		generator.childReport("providerTozipcode entered");

		SeleniumUtils.highLightElement(providerTophone, driver);
		providerTophone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTophone"));
		generator.childReport("providerTophone entered");

		SeleniumUtils.presenceOfElement(driver, iframe());

		SeleniumUtils.switchToFrame(driver, frame);

		generator.childReport("Signature frame switched");

		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");

		driver.switchTo().defaultContent();

		SeleniumUtils.highLightElement(authenticationdate, driver);
		authenticationdate.click();
		generator.childReport("authenticationdate entered");

		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");

		SeleniumUtils.highLightElement(claimantaddress, driver);
		claimantaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantaddress"));
		generator.childReport("claimantaddress entered");

		SeleniumUtils.highLightElement(authenticationcity, driver);
		authenticationcity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationcity"));
		generator.childReport("authenticationcity entered");

		SeleniumUtils.highLightElement(authenticationstate, driver);
		authenticationstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationstate"));
		generator.childReport("authenticationstate entered");

		SeleniumUtils.highLightElement(authenticationzipcode, driver);
		authenticationzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationzipcode"));
		generator.childReport("authenticationzipcode entered");

		SeleniumUtils.highLightElement(authenticationphone, driver);
		authenticationphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationphone"));
		generator.childReport("authenticationphone entered");

		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");

		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);

	}

	public void fillingForm121Community(String methodName, String tcName, ReportGenerator generator) throws Exception {

		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		generator.childReport("Signature frame switched");

		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica121Verify, driver);
		generator.childReport("ICA form name verified");

		SeleniumUtils.highLightElement(claimantfirstname, driver);
		claimantfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"));
		generator.childReport("claimantfirstname entered");

		SeleniumUtils.highLightElement(claimantlastname, driver);
		claimantlastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantlastname"));
		generator.childReport("claimantlastname entered");

		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");

		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");

		SeleniumUtils.highLightElement(reasondetails, driver);
		reasondetails.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "reasondetails"));
		generator.childReport("reasondetails entered");

		SeleniumUtils.highLightElement(providerdoctorname, driver);
		providerdoctorname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerdoctorname"));
		generator.childReport("providerdoctorname entered");

		SeleniumUtils.highLightElement(provideraddress, driver);
		provideraddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "provideraddress"));
		generator.childReport("provideraddress entered");

		SeleniumUtils.highLightElement(providercity, driver);
		providercity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providercity"));
		generator.childReport("providercity entered");

		SeleniumUtils.highLightElement(providerstate, driver);
		providerstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerstate"));
		generator.childReport("providerstate entered");

		SeleniumUtils.highLightElement(providerzipcode, driver);
		providerzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerzipcode"));
		generator.childReport("providerzipcode entered");

		SeleniumUtils.highLightElement(providerphone, driver);
		providerphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerphone"));
		generator.childReport("providerphone entered");

		SeleniumUtils.highLightElement(providerToname, driver);
		providerToname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerToname"));
		generator.childReport("providerToname entered");

		SeleniumUtils.highLightElement(providerToaddress, driver);
		providerToaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerToaddress"));
		generator.childReport("providerToaddress entered");

		SeleniumUtils.highLightElement(providerTocity, driver);
		providerTocity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTocity"));
		generator.childReport("providerTocity entered");

		SeleniumUtils.highLightElement(providerTostate, driver);
		providerTostate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTostate"));
		generator.childReport("providerTostate entered");

		SeleniumUtils.highLightElement(providerTozipcode, driver);
		providerTozipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTozipcode"));
		generator.childReport("providerTozipcode entered");

		SeleniumUtils.highLightElement(providerTophone, driver);
		providerTophone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "providerTophone"));
		generator.childReport("providerTophone entered");

		SeleniumUtils.presenceOfElement(driver, iframe());

		SeleniumUtils.switchToFrame(driver, frame);

		generator.childReport("Signature frame switched");

		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");

		driver.switchTo().defaultContent();
		
		SeleniumUtils.switchToFrame(driver, frameswitch);

		SeleniumUtils.highLightElement(authenticationdate, driver);
		authenticationdate.click();
		generator.childReport("authenticationdate entered");

		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");

		SeleniumUtils.highLightElement(claimantaddress, driver);
		claimantaddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantaddress"));
		generator.childReport("claimantaddress entered");

		SeleniumUtils.highLightElement(authenticationcity, driver);
		authenticationcity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationcity"));
		generator.childReport("authenticationcity entered");

		SeleniumUtils.highLightElement(authenticationstate, driver);
		authenticationstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationstate"));
		generator.childReport("authenticationstate entered");

		SeleniumUtils.highLightElement(authenticationzipcode, driver);
		authenticationzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationzipcode"));
		generator.childReport("authenticationzipcode entered");

		SeleniumUtils.highLightElement(authenticationphone, driver);
		authenticationphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationphone"));
		generator.childReport("authenticationphone entered");

		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");

		driver.switchTo().defaultContent();

		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);

	}
}
