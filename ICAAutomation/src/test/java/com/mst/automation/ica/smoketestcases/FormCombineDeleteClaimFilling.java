package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class FormCombineDeleteClaimFilling extends BaseTest{
	
	@Test (groups = "Smoke Suite")
	@Parameters({ "userType", "UrlQA" })
		
		public void formfillingcombinedelete(String userType, String UrlQA) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_030";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		String Url2 = TestUtils.getStringFromPropertyFile(UrlQA);
		
		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.loginCommunity(user, pwd, reporter);
		pagecombinedlt = homePage.formUrlclaimcombinecu();
		pagecombinedlt.fillingFormCombineCommunity(methodName, tcName, reporter);
		
		SeleniumUtils.switchToNewTab(driver, Url2);
		homePage = loginPage.loginCommunity(user, pwd, reporter);
		pagecombinedlt = homePage.formUrlclaimcombinecu();
		pagecombinedlt.fillingFormDeleteCommunity(methodName, tcName, reporter);
		


	}


}
