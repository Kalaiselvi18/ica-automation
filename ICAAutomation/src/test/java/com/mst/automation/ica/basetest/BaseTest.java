package com.mst.automation.ica.basetest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.abstractclasses.GlobalDeclarations;
import com.mst.automation.ica.browserfactory.BrowserFactory;
import com.mst.automation.ica.customexception.CustomException;
import com.mst.automation.ica.email.EmailReport;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.pageobjects.AccountPage;
import com.mst.automation.ica.pageobjects.BLSManagerProfile;
import com.mst.automation.ica.pageobjects.BLSRecordPage;
import com.mst.automation.ica.pageobjects.BLSTabView;
import com.mst.automation.ica.pageobjects.BLSWorkerProfile;
import com.mst.automation.ica.pageobjects.CarrierAttorneyProfile;
import com.mst.automation.ica.pageobjects.CarrierProfile;
import com.mst.automation.ica.pageobjects.ClaimDetailPage;
import com.mst.automation.ica.pageobjects.ClaimRecordCreation;
import com.mst.automation.ica.pageobjects.ClaimTabPage;
import com.mst.automation.ica.pageobjects.ClaimTaskObjectPage;
import com.mst.automation.ica.pageobjects.ClaimTaskSelect;
import com.mst.automation.ica.pageobjects.ClaimantAttorneyProfile;
import com.mst.automation.ica.pageobjects.ClaimantProfile;
import com.mst.automation.ica.pageobjects.ClaimsEditPage;
import com.mst.automation.ica.pageobjects.ClaimsManagerProfile;
import com.mst.automation.ica.pageobjects.ClaimsProcessorProfile;
import com.mst.automation.ica.pageobjects.ClaimsRecordPage;
import com.mst.automation.ica.pageobjects.Document101Page;
import com.mst.automation.ica.pageobjects.Document102Page;
import com.mst.automation.ica.pageobjects.Document104Page;
import com.mst.automation.ica.pageobjects.Document106Page;
import com.mst.automation.ica.pageobjects.Document107Page;
import com.mst.automation.ica.pageobjects.Document108Page;
import com.mst.automation.ica.pageobjects.Document121Page;
import com.mst.automation.ica.pageobjects.Document122Page;
import com.mst.automation.ica.pageobjects.Document407Page;
import com.mst.automation.ica.pageobjects.Document446Page;
import com.mst.automation.ica.pageobjects.Document528Page;
import com.mst.automation.ica.pageobjects.Document529Page;
import com.mst.automation.ica.pageobjects.DocumentSelect;
import com.mst.automation.ica.pageobjects.EmployerProfile;
import com.mst.automation.ica.pageobjects.Form101Page;
import com.mst.automation.ica.pageobjects.Form102Page;
import com.mst.automation.ica.pageobjects.Form104Page;
import com.mst.automation.ica.pageobjects.Form105Page;
import com.mst.automation.ica.pageobjects.Form106Page;
import com.mst.automation.ica.pageobjects.Form107Page;
import com.mst.automation.ica.pageobjects.Form108Page;
import com.mst.automation.ica.pageobjects.Form120Page;
import com.mst.automation.ica.pageobjects.Form121Page;
import com.mst.automation.ica.pageobjects.Form122Page;
import com.mst.automation.ica.pageobjects.Form407Page;
import com.mst.automation.ica.pageobjects.Form446Page;
import com.mst.automation.ica.pageobjects.Form528Page;
import com.mst.automation.ica.pageobjects.Form529Page;
import com.mst.automation.ica.pageobjects.FormCombineDeletePage;
import com.mst.automation.ica.pageobjects.HomePage;
import com.mst.automation.ica.pageobjects.InterestedPartyProfile;
import com.mst.automation.ica.pageobjects.LoginPage;
import com.mst.automation.ica.pageobjects.NewClaimRequestPage;
import com.mst.automation.ica.pageobjects.ProfileNameSelect;
import com.mst.automation.ica.pageobjects.ProfileSetup;
import com.mst.automation.ica.pageobjects.RolesClick;
import com.mst.automation.ica.pageobjects.SystemAdministratorProfile;
import com.mst.automation.ica.pageobjects.WorkersClaimPage;
import com.mst.automation.ica.pageobjects.objectVerify;
import com.mst.automation.ica.smoketestcases.FormPriorsPage;
import com.mst.automation.ica.utils.TestUtils;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This Base class is used to run the before suite, after suite,
 * before method, after method in all the test methods.
 */

public class BaseTest extends GlobalDeclarations{
	protected LoginPage loginPage;
	protected static HomePage homePage;
	protected ProfileSetup profileSetup;
	protected ProfileNameSelect profileNameSelect;
	protected RolesClick roleSelect;
	protected ClaimsManagerProfile claimsProfile;
	protected ClaimsProcessorProfile claimsProcessor;
	protected BLSManagerProfile blsManager;
	protected BLSWorkerProfile blsWorker;
	protected SystemAdministratorProfile systemAdmin;
	protected ClaimantProfile claimant;
	protected ClaimRecordCreation claimsrecordcreate;
	protected ClaimsRecordPage claimsrecordpage;
	protected BLSRecordPage blsRecordPage;
	protected EmployerProfile employer;
	protected CarrierProfile carrier;
	protected InterestedPartyProfile interestedParty;
	protected ClaimantAttorneyProfile claimantAttorney;
	protected CarrierAttorneyProfile carrierAttorney;
	protected objectVerify verobj;
	protected Form407Page page407;
	protected Form446Page page446;
	protected Form101Page page101;
	protected Form102Page page102;
	protected Form120Page page120;
	protected Form121Page page121;
	protected Form122Page page122; 
	protected Form104Page page104;
	protected Form106Page page106;
	protected Form107Page page107;
	protected Form108Page page108;
	protected ClaimsEditPage editPageClaims;
	protected BLSTabView tabBls;
	protected Form528Page page528;
	protected Form529Page page529;
	protected DocumentSelect docuselect;
	protected Document101Page verify101;
	protected Document407Page verify407;
	protected WorkersClaimPage workerClaimPage;
	protected ClaimTaskSelect claimTaskSelect;
	protected ClaimTaskObjectPage pageClaimTask;
	protected NewClaimRequestPage requestclaimpage;
	protected ClaimTabPage claimsTab;
	protected FormPriorsPage pagePriors;
	protected Form105Page page105;
	protected FormCombineDeletePage pagecombinedlt;
    protected Document102Page verify102;
    protected ClaimDetailPage claimDetailPage;
    protected AccountPage accountPage;
	protected Document446Page verify446;
	protected Document529Page verify529;
	protected Document121Page verify121;
	protected Document104Page verify104;
	protected Document106Page verify106;
	protected Document107Page verify107;
	protected Document108Page verify108;
	protected Document122Page verify122;
	protected Document528Page verify528; 

	protected WebDriver driver;
	protected String url;
	protected String url2;
	protected String mBrowserType;
	protected String browser;
	protected String platform;
	protected String browserVersion;
	protected Map<String, List<String>> resultMap;
	protected List<String> results;
	protected ReportGenerator reporter;

	protected static Logger logger = Logger.getLogger(BaseTest.class);

	@BeforeSuite (alwaysRun=true)
	/**
	 * This beforeSuite method call the openStream method for the workbook instance
	 * creation
	 */
	public void beforeSuite() throws IOException {
		PropertyConfigurator.configure("log4j.properties");
		GoogleSheetAPI.authorize();
		GoogleSheetAPI.getSheetsService();
	}

	/**
	 * Setup method used to configure the browser, version, platform etc.,
	 * 
	 * @param env
	 * @param userType
	 * @param browserType
	 * @throws Exception
	 */
	@BeforeMethod (alwaysRun=true)
	@Parameters({ "env", "userType", "browserType"})
	public void setup(String env, String userType, String browserType) {
		resultMap = new HashMap<>();
		results = new ArrayList<>();

		if (null != userType) {
			TestUtils.setUserType(userType);
		}
		mBrowserType = browserType;
		try {
			driver = BrowserFactory.loadDriver(mBrowserType.toUpperCase());
		} catch (Exception e) {
			logger.error(e);
		}

		url = TestUtils.getStringFromPropertyFile(env);
		
		browser = getBrowserName();
		browserVersion = getBrowserVersion();
		platform = getPlatform();

		driver.navigate().to(url);
		driver.manage().window().maximize();

		loginPage = new LoginPage(driver);
	}

	/**
	 * This method runs after every test which contains the logout, clear cookies.
	 * 
	 * @param result
	 * @throws IOException 
	 * @throws Exception
	 */
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws IOException {

		String methodName = result.getMethod().getMethodName();
		if (result.getStatus() == ITestResult.FAILURE) {
			String res = result.getThrowable().getMessage();
			reporter.logScreenshot(driver, methodName, res);
		}
		else if(result.getStatus() == ITestResult.SKIP) {
			String res = result.getThrowable().getMessage();
			try {
				reporter.logSkipTest(driver, methodName, res);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (driver != null) {
			try {
				driver.manage().deleteAllCookies();
				logger.info("Cookies are deleted");
				driver.quit();
			} catch (CustomException e) {
				e.getLocalizedMessage();
				e.getMessage();
				logger.error("Custom Exception for tear down", e);
			} catch (Exception ex) {
				logger.error("tear Down", ex);
			}
		}
		else
			logger.error("Driver is null");
		
		reporter.endTest();	
		logger.info("*****************************************************************");
		logger.info("*****************************************************************");
	}

	/**
	 * This method is executed at the end of all tests are executed.
	 * 
	 * @throws Exception
	 */
	@AfterSuite(alwaysRun=true)
	public void shutdown() {
		reporter.flush();
		EmailReport.send_report();
	}

	private String getBrowserName() {
		RemoteWebDriver webDriver = (RemoteWebDriver) driver;
		return webDriver.getCapabilities().getBrowserName();
	}

	private String getBrowserVersion() {
		RemoteWebDriver webDriver = (RemoteWebDriver) driver;
		return webDriver.getCapabilities().getVersion();
	}

	private String getPlatform() {
		RemoteWebDriver webDriver = (RemoteWebDriver) driver;
		return webDriver.getCapabilities().getPlatform().name();
	}

	public String verifyResultsMap() {
		String result = null;
		for (Map.Entry<String, List<String>> map : resultMap.entrySet()) {
			if (!map.getValue().isEmpty()) {
				result += map.getKey() + "---->" + map.getValue() + "\n";
			}
		}
		return result;
	}
}
