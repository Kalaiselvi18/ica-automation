package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

public class LoginVerification extends BaseTest{
	
	@Test (groups = "Smoke Suite")
	@Parameters({ "userType" })
	
	public void verifyLogin(String userType) throws Exception {

		String className = this.getClass().getSimpleName();
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");

		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.login(user, pwd, reporter);
		reporter.childReport("Login_Verify");
		homePage.loginverify(reporter);
	}
}
