package com.mst.automation.ica.browserfactory;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.mst.automation.ica.constant.Constant;
import com.mst.automation.ica.customexception.CustomException;
/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: The Browser Factory Class is used to load the type of
 * browser with the help of testng browser parameter value.
 */
public class BrowserFactory {

	static WebDriver driver;
    static ChromeOptions options;
     
    public enum DriverSupportedBrowsers {
 		FIREFOX, CHROME, IE;
 	}
          
	public static WebDriver loadDriver(String mBrowserType) {
		WebDriver loadDriver = getNewDriver(DriverSupportedBrowsers.valueOf(mBrowserType));
		return loadDriver;
	}

	private static WebDriver getNewDriver(DriverSupportedBrowsers driverType) {

		String os=System.getProperty("os.name");
		if(os.contains("Windows")) {
		switch (driverType) {
		case FIREFOX: 
		System.setProperty("webdriver.gecko.driver",Constant.WINDOWSFIREFOXDRIVER); 
		DesiredCapabilities dc=DesiredCapabilities.firefox();
		return new FirefoxDriver(dc);
		case CHROME:
		System.setProperty("webdriver.chrome.driver", Constant.WINDOWSCHROMEDRIVER);
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications",2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		default:
		throw new InvalidParameterException("You must choose one of the defined Windows driver type");
		}
		}
		else if(os.contains("Linux")) {
		switch (driverType) {
		case FIREFOX: 
		
		System.setProperty("webdriver.gecko.driver",Constant.LINUXFIREFOXDRIVER); 
		DesiredCapabilities dc=DesiredCapabilities.firefox();
		
		return new FirefoxDriver(dc);
		case CHROME:
		System.setProperty("webdriver.chrome.driver",Constant.LINUXCHROMEDRIVER);
		options = new ChromeOptions();
		options.setBinary(Constant.CHROMEBINARY);
		options.addArguments("--headless");
		options.addArguments("--disable-gpu");
		options.addArguments("--no-sandbox");
		options.addArguments("--disable-extensions");
		options.addArguments("--test-type");
		return new ChromeDriver(options);
		default:
		throw new InvalidParameterException("You must choose one of the defined Linux driver type");
		}
		}
		return driver;
		}
	}


