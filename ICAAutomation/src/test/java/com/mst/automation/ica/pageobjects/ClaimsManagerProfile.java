package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To check permissions for Claims Manager Profile
 *
 */
public class ClaimsManagerProfile extends DriverClass{
	

	@FindBy(xpath = "//label[contains(.,'User License')]/following::td[1]")
	public WebElement userProfile;
	
	public By element() {
		By ul=By.xpath("//label[contains(.,'User License')]/following::td[1]");
		return ul;
	}
	
	@FindBy(xpath = "//h3[contains(.,'Standard Object Permissions')]")
	public WebElement standardObjectPermission;
	
	@FindBy(xpath = "//th[contains(.,'Accounts')]")
	public WebElement soAccount;
	
	@FindBy(xpath = "//th[contains(.,'Contacts')]")
	public WebElement soContact;
	
	@FindBy(xpath = "//th[contains(.,'Cases')]")
	public WebElement soClaims;
	
	@FindBy(xpath = "//th[contains(.,'101')]")
	public WebElement co101;
	
	@FindBy(xpath = "//th[contains(.,'102')]")
	public WebElement co102;
	
	@FindBy(xpath = "//th[contains(.,'104')]")
	public WebElement co104;
	
	@FindBy(xpath = "//th[contains(.,'407')]")
	public WebElement co407;
	
	@FindBy(xpath = "//th[contains(.,'BLS')]")
	public WebElement coBLS;
	
	@FindBy(xpath = "//th[contains(.,'Documents')]")
	public WebElement coDocuments;
	
	@FindBy(xpath = "//th[contains(.,'Accounts')]/following::tr[1]")
	public WebElement accountPermission;
	
	@FindBy(xpath = "//th[contains(.,'Contacts')]/following::tr[1]")
	public WebElement contactPermission;
	
	@FindBy(xpath = "//th[contains(.,'Cases')]/following::tr[1]")
	public WebElement claimsPermission;
	
	@FindBy(xpath = "//th[contains(.,'101')]/following::tr[1]")
	public WebElement permission102;
	
	@FindBy(xpath = "//th[contains(.,'102')]/following::tr[1]")
	public WebElement permission101;
	
	@FindBy(xpath = "//th[contains(.,'104')]/following::tr[1]")
	public WebElement permission104;
	
	@FindBy(xpath = "//th[contains(.,'407')]/following::tr[1]")
	public WebElement permission407;
	
	@FindBy(xpath = "//th[contains(.,'BLS')]/following::tr[1]")
	public WebElement permissionBls;
	
	@FindBy(xpath = "//th[contains(.,'BLS')]/following::tr[10]")
	public WebElement permissionDocuments;
	
	@FindBy(css = "iframe[title='Profile: Claims Manager ~ Salesforce - Unlimited Edition']")
	public WebElement iframe;

	public By iframeVerify() {
		By frame = By.cssSelector("iframe[title='Profile: Claims Manager ~ Salesforce - Unlimited Edition']");
		return frame;
	}
	
	
	public ClaimsManagerProfile(WebDriver driver){
		 super(driver);
	 }

	/*To check the claims*/
	
	public void claimsCheck(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		
			
		SeleniumUtils.presenceOfElement(driver, iframeVerify());
		SeleniumUtils.switchToFrame(driver, iframe);
		generator.childReport("Profile frame switched");
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(userProfile, driver);
		generator.childReport("User License verified");
		SeleniumUtils.highLightElement(standardObjectPermission, driver);
		SeleniumUtils.highLightElement(soAccount, driver);
		SeleniumUtils.verifyPermissionSet(accountPermission,GoogleSheetAPI.ReadData(methodName, tcName, "account_Permission"),driver);
		generator.childReport("Account Permission Verified");
		SeleniumUtils.highLightElement(soClaims, driver);
		SeleniumUtils.verifyPermissionSet(claimsPermission,GoogleSheetAPI.ReadData(methodName, tcName, "claims_Permission"),driver);
		generator.childReport("Claims Permission Verified");
		SeleniumUtils.highLightElement(soContact, driver);
		SeleniumUtils.verifyPermissionSet(contactPermission,GoogleSheetAPI.ReadData(methodName, tcName, "contact_Permission"),driver);
		generator.childReport("Contact Permission Verified");
		SeleniumUtils.highLightElement(co101, driver);
		SeleniumUtils.verifyPermissionSet(permission101,GoogleSheetAPI.ReadData(methodName, tcName, "101_Permission"),driver);
		generator.childReport("101 Permission Verified");
		SeleniumUtils.highLightElement(co102, driver);
		SeleniumUtils.verifyPermissionSet(permission102,GoogleSheetAPI.ReadData(methodName, tcName, "102_Permission"),driver);
		generator.childReport("102 Permission Verified");
		SeleniumUtils.highLightElement(co104, driver);
		SeleniumUtils.verifyPermissionSet(permission104,GoogleSheetAPI.ReadData(methodName, tcName, "104_Permission"),driver);
		generator.childReport("104 Permission Verified");
		SeleniumUtils.highLightElement(co407, driver);
		SeleniumUtils.verifyPermissionSet(permission407,GoogleSheetAPI.ReadData(methodName, tcName, "407_Permission"),driver);
		generator.childReport("407 Permission Verified");
		SeleniumUtils.highLightElement(coBLS, driver);
		SeleniumUtils.verifyPermissionSet(permissionBls,GoogleSheetAPI.ReadData(methodName, tcName, "BLS_Permission"),driver);
		generator.childReport("BLS Permission Verified");
		SeleniumUtils.highLightElement(coDocuments, driver);
		SeleniumUtils.verifyPermissionSet(permissionDocuments,GoogleSheetAPI.ReadData(methodName, tcName, "Docu_Permission"),driver);
		generator.childReport("Documents Permission Verified");
}
}
