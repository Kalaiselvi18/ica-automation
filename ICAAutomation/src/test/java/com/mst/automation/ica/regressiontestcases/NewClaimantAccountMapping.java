/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Feb 10, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: Create a 102 form and verify new claim is created with current date (depends on DOI,DOB, Last name)
 * Login to Salesforce and verify the field mapping in New Claimant Account
 */

public class NewClaimantAccountMapping extends BaseTest {

	@Test (groups = {"Form Regression Suite","102 Flow"})
	@Parameters({"env1","userType"})
	
	public void doc102NewClaimantAccountMapping(String env1,String userType) throws Exception {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_104";
		
		String Url = TestUtils.getStringFromPropertyFile(env1);
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		reporter = new ReportGenerator(getbrowser(), className);
		page102 = loginPage.formUrl102();
		page102.fillingForm102(methodName, tcName, reporter);
		
		SeleniumUtils.switchToNewTab(driver, Url);
		homePage = loginPage.login(user, pwd, reporter);
		docuselect = homePage.documentObjClick(reporter);
		verify102 = docuselect.docu102Verify(methodName, tcName, reporter);
		
		verify102.document102DetailPage(methodName, tcName, reporter);
		claimDetailPage = verify102.document102RelatedPage(methodName, tcName,reporter);
		accountPage = claimDetailPage.clickOnClaimantAccount(methodName,reporter);
		accountPage.verifyClaimantAccount(methodName, tcName, reporter);
	}	
}
