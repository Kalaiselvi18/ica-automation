package com.mst.automation.ica.retryutility;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.annotations.ITestAnnotation;
/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This class is used in the retry test execution
 */
public class RetryListener implements IAnnotationTransformer {

	@SuppressWarnings("rawtypes")
	@Override
	public void transform(ITestAnnotation testannotation, Class testClass, Constructor testConstructor,
			Method testMethod) {
		IRetryAnalyzer retry = testannotation.getRetryAnalyzer();

		if (retry == null) {
			testannotation.setRetryAnalyzer(Retry.class);
		}
	}

}