package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document122Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document122Page(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant First Name']/following::span[2]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Last Name']/following::span[2]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim #:']/following::span[2]")
	public WebElement icaClaimNo;
	
	@FindBy(xpath = "//div[1]/span[text()='Reason for Leaving the State:']/following::span[2]")
	public WebElement reasonForLeaving;
	
	@FindBy(xpath = "//h3/button/span[text()='Out of State Details']/following::span[3]")
	public WebElement outAddress;
	
	@FindBy(xpath = "//h3/button/span[text()='Out of State Details']/following::span[7]")
	public WebElement outCity;
	
	@FindBy(xpath = "//h3/button/span[text()='Out of State Details']/following::span[11]")
	public WebElement outState;
	
	@FindBy(xpath = "//h3/button/span[text()='Out of State Details']/following::span[15]")
	public WebElement outZip;
	
	@FindBy(xpath = "//div[1]/span[text()='Physician Name']/following::span[2]")
	public WebElement physicianName;
	
	@FindBy(xpath = "//div[1]/span[text()='Physician Name']/following::span[6]")
	public WebElement physicianState;
	
	@FindBy(xpath = "//div[1]/span[text()='Physician Name']/following::span[10]")
	public WebElement physicianCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Physician Name']/following::span[14]")
	public WebElement physicianZipcode;
	
	@FindBy(xpath = "//div[1]/span[text()='Physician Name']/following::span[22]")
	public WebElement physicianAddress;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Signature']/following::span[1]")
	public WebElement claimantSignature;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[2]")
	public WebElement claimantAddress;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[10]")
	public WebElement claimantState;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[22]")
	public WebElement claimantCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Address']/following::span[26]")
	public WebElement claimantZipcode;
	
	
	
	
public void verify122Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		String clmntFrstName = claimantFirstName.getText();
		TestUtils.compareText(clmntFrstName, GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"), claimantFirstName, driver);
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		String clmntLstName = claimantLastName.getText();
		TestUtils.compareText(clmntLstName, GoogleSheetAPI.ReadData(methodName, tcName, "claimantlastname"), claimantLastName, driver);
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		String icaClaim = icaClaimNo.getText();
		TestUtils.compareText(icaClaim, GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"), icaClaimNo, driver);
		
		SeleniumUtils.highLightElement(reasonForLeaving, driver);
		String leavingreason = reasonForLeaving.getText();
		TestUtils.compareText(leavingreason, GoogleSheetAPI.ReadData(methodName, tcName, "reasondetails"), reasonForLeaving, driver);
		
		SeleniumUtils.highLightElement(outAddress, driver);
		String outAdd = outAddress.getText();
		TestUtils.compareText(outAdd, GoogleSheetAPI.ReadData(methodName, tcName, "outaddress"), outAddress, driver);
		
		SeleniumUtils.highLightElement(outCity, driver);
		String outCty = outCity.getText();
		TestUtils.compareText(outCty, GoogleSheetAPI.ReadData(methodName, tcName, "outcity"), outCity, driver);
		
		SeleniumUtils.highLightElement(outState, driver);
		String outStat = outState.getText();
		TestUtils.compareText(outStat, GoogleSheetAPI.ReadData(methodName, tcName, "outstate"), outState, driver);
		
		SeleniumUtils.highLightElement(outZip, driver);
		String otZip = outZip.getText();
		TestUtils.compareText(otZip, GoogleSheetAPI.ReadData(methodName, tcName, "outzipcode"), outZip, driver);
		
		SeleniumUtils.highLightElement(physicianName, driver);
		String phyName = physicianName.getText();
		TestUtils.compareText(phyName, GoogleSheetAPI.ReadData(methodName, tcName, "providername"), physicianName, driver);
		
		SeleniumUtils.highLightElement(physicianState, driver);
		String phyStat = physicianState.getText();
		TestUtils.compareText(phyStat, GoogleSheetAPI.ReadData(methodName, tcName, "providerstate"), physicianState, driver);
		
		SeleniumUtils.highLightElement(physicianCity, driver);
		String phyCty = physicianCity.getText();
		TestUtils.compareText(phyCty, GoogleSheetAPI.ReadData(methodName, tcName, "providercity"), physicianCity, driver);
		
		SeleniumUtils.highLightElement(physicianZipcode, driver);
		String phyZip = physicianZipcode.getText();
		TestUtils.compareText(phyZip, GoogleSheetAPI.ReadData(methodName, tcName, "providerzipcode"), physicianZipcode, driver);
		
		SeleniumUtils.highLightElement(physicianAddress, driver);
		String phyAdd = physicianAddress.getText();
		TestUtils.compareText(phyAdd, GoogleSheetAPI.ReadData(methodName, tcName, "provideraddress"), physicianAddress, driver);
		
		SeleniumUtils.highLightElement(claimantSignature, driver);
		String clmSign = claimantSignature.getText();
		TestUtils.compareText(clmSign, GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"), claimantSignature, driver);
		
		SeleniumUtils.highLightElement(claimantAddress, driver);
		String clmAdd = claimantAddress.getText();
		TestUtils.compareText(clmAdd, GoogleSheetAPI.ReadData(methodName, tcName, "submitteraddress"), claimantAddress, driver);
		
		SeleniumUtils.highLightElement(claimantState, driver);
		String clmStat = claimantState.getText();
		TestUtils.compareText(clmStat, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationstate"), claimantState, driver);
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		String clmCty = claimantCity.getText();
		TestUtils.compareText(clmCty, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationcity"), claimantCity, driver);
		
		SeleniumUtils.highLightElement(claimantZipcode, driver);
		String clmZip = claimantZipcode.getText();
		TestUtils.compareText(clmZip, GoogleSheetAPI.ReadData(methodName, tcName, "authenticationzipcode"), claimantZipcode, driver);
		
		
}


}
