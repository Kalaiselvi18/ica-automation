package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

public class Form105Page extends DriverClass{
	

	public By element() {
		By ul=By.xpath("//b[text()='NOTICE OF SUSPENSION OF BENEFITS']");
		return ul;
	}
	
	@FindBy(xpath = "//b[text()='NOTICE OF SUSPENSION OF BENEFITS']")
	public WebElement ica105Verify;
	
	@FindBy(xpath = "//h3[text()='Carrier or Self-Insured Details']/following::input[1]")
	public WebElement carrierName;
	
	@FindBy(xpath = "//h3[text()='Carrier or Self-Insured Details']/following::input[2]")
	public WebElement carrierAddress;
	
	@FindBy(xpath = "//h3[text()='Carrier or Self-Insured Details']/following::input[3]")
	public WebElement carrierCity;
	
	@FindBy(xpath = "//h3[text()='Carrier or Self-Insured Details']/following::input[4]")
	public WebElement carrierState;
	
	@FindBy(xpath = "//h3[text()='Carrier or Self-Insured Details']/following::input[5]")
	public WebElement carrierZipcode;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::input[1]")
	public WebElement claimNumber;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::input[3]")
	public WebElement claimCarrierNumber;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::input[4]")
	public WebElement claimEmployer;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::textarea[1]")
	public WebElement claimAddress;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::input[5]")
	public WebElement claimCity;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::input[6]")
	public WebElement claimState;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::input[7]")
	public WebElement claimZipcode;
	
	@FindBy(xpath = "//h3[text()='Claim Details']/following::a[1]")
	public WebElement claimDate;
	
	@FindBy(xpath = "//label[text()='Claimant First Name']/following::input[1]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::input[1]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::textarea[1]")
	public WebElement claimantAddress;
	
	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::input[2]")
	public WebElement claimantCity;
	
	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::input[3]")
	public WebElement claimantState;
	
	@FindBy(xpath = "//label[text()='Claimant Last Name']/following::input[4]")
	public WebElement claimantZipcode;
	
	
	@FindBy(xpath = "//h3[text()='Suspension Details']/following::a[1]")
	public WebElement suspensionDate;
	
	@FindBy(xpath = "//h3[text()='Suspension Details']/following::input[3]")
	public WebElement suspensionCheckbox;
	
	
	@FindBy(xpath = "//label[text()='Mailed On:']/following::a[1]")
	public WebElement mailedOn;
	
	@FindBy(xpath = "//label[text()='By:']/following::input[1]")
	public WebElement mailedBy;
	
	@FindBy(xpath = "//label[text()='By:']/following::input[2]")
	public WebElement mailedAutorized;
	
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifySuccess;


	public Form105Page(WebDriver driver) {
		super(driver);
	}
	
	public void fillingForm105Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica105Verify, driver);
		generator.childReport("ICA 105 form name verified");
		
		carrierName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "carrierName"));
		generator.childReport("Carrier name Entered");
		carrierAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "carrierAddress"));
		generator.childReport("Carrier Address Entered");
		carrierCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "carrierCity"));
		generator.childReport("Carrier City Entered");
		carrierState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "carrierState"));
		generator.childReport("Carrier State Entered");
		carrierZipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "carrierZipcode"));
		generator.childReport("Carrier Zipcode Entered");
		
		claimNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimNumber"));
		generator.childReport("Claim Number Entered");
		claimCarrierNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimCarrierNumber"));
		generator.childReport("Claim Carrier Number Entered");
		claimEmployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimEmployer"));
		generator.childReport("Claim Employer Entered");
		claimAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimAddress"));
		generator.childReport("Claim Address Entered");
		claimCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimCity"));
		generator.childReport("Claim City Entered");
		claimState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimState"));
		generator.childReport("Claim State Entered");
		claimZipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimZipcode"));
		generator.childReport("Claim Zipcode Entered");
		claimDate.click();
		generator.childReport("Claim Date Entered");
		
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantFirstName"));
		generator.childReport("Claimant First name Entered");
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantLastName"));
		generator.childReport("Claimant Last name Entered");
		claimantAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantAddress"));
		generator.childReport("Claimant Address Entered");
		claimantCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantCity"));
		generator.childReport("Claimant City Entered");
		claimantState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantState"));
		generator.childReport("Claimant State Entered");
		claimantZipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantZipcode"));
		generator.childReport("Claimant Zipcode Entered");
		
		suspensionDate.click();
		generator.childReport("Suspension Date Entered");
		suspensionCheckbox.click();
		
		mailedOn.click();
		generator.childReport("Mailed No Entered");
		mailedBy.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "mailedBy"));
		generator.childReport("Mailed By Entered");
		mailedAutorized.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "mailedAutorized"));
		generator.childReport("Mailed Authorized Entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);
		
		
	}

}
