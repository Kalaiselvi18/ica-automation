/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 27-Nov-2017
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class ClaimsEditPage extends DriverClass {
	
	@FindBy(xpath  = "//div[text()='New Contact']/following::a[3]")
	public WebElement narrowClick;

	@FindBy(css = "div[title='Edit']")
	public WebElement editClick;
	
	public By dropDown() {
		By narrow = By.xpath("//div[text()='New Contact']/following::a[3]");
		return narrow;
	}
	
	public By label() {
		By headerClaims = By.xpath("//h1/span[text()='20180131958']");
		return headerClaims;
	}
	
	@FindBy(xpath = "//h1/span[text()='20180131958']")
	public WebElement claimsHeader;
	
	public By editHeader() {
		By headerEdit = By.xpath("//h2[text()='Edit 20180131958']");
		return headerEdit;
	}
	
	@FindBy(xpath = "//h2[text()='Edit 20180131958']")
	public WebElement claimsEditHeader;
	
	@FindBy(xpath = "//h2[text()='Edit 20180131958']/following::a[3]")
	public WebElement claimsEditType;
	
	@FindBy(css = "a[title='Time Loss']")
	public WebElement claimsSelectTimeloss;
	
	@FindBy(css = "a[title='--None--']")
	public WebElement claimsSelectnone;
	
	
	@FindBy(xpath = "//div[3]/div/button[3]/span[text()='Save']")
	public WebElement claimsSave;
	
	public By claimType() {
		By type = By.xpath("//span[text()='Claim Type']/following::span[1]");
		return type;
	}
	
	@FindBy(xpath = "//span[text()='Claim Type']/following::span[1]")
	public WebElement claimsType;
	
	public By relatedTab() {
		By tab = By.xpath("//span[text()='Related']");
		return tab;
	}
	
	@FindBy(xpath = "//span[text()='Related']")
	public WebElement claimsRelatedTab;
	
	public By relatedBLS() {
		By record = By.xpath("//h2[contains(.,'BLS')]");
		return record;
	}
	
	@FindBy(xpath = "//h2/a[contains(.,'BLS')]")
	public WebElement claimsBLSVerify;
	
	public By numberBLS() {
		By num = By.xpath("//table/thead/tr/th[1]");
		return num;
	}
	
	@FindBy(xpath = "//table/thead/tr/th[1]")
	public WebElement claimsBLSNumber;
	
	
	public By recordBLS() {
		By rec = By.xpath("//h2[contains(.,'BLS')]/following::a[1]");
		return rec;
	}
	
	@FindBy(xpath = "//h2[contains(.,'BLS')]/following::a[1]")
	public WebElement claimsBLSRecoordClick;
	
	public By verifyBLS() {
		By ver = By.xpath("//span[text()='BLS Number']/following::span[2]");
		return ver;
	}
	
	@FindBy(xpath = "//span[text()='BLS Number']/following::span[2]")
	public WebElement claimsBLSRecoordVerify;
	
	public By deletedBLS() {
		By del = By.xpath("//p[text()='No items to display.']");
		return del;
	}
	
	@FindBy(xpath = "//p[text()='No items to display.']")
	public WebElement claimsBLSRecoordNotFound;
	
	@FindBy(xpath = "//span[text()='Occupation']/following::a[1]")
	public WebElement occupationCheck;
	
	
	public ClaimsEditPage(WebDriver driver) {
		super(driver);
	}
	
	public BLSRecordPage editRecordClick(String methodName, String tcName, ReportGenerator generator)
			throws Exception{
		SeleniumUtils.presenceOfElement(driver, label());
		SeleniumUtils.highLightElement(claimsHeader, driver);
		/*SeleniumUtils.presenceOfElement(driver, dropDown());
		SeleniumUtils.highLightElement(narrowClick, driver);
		narrowClick.click();*/
		SeleniumUtils.highLightElement(editClick, driver);
		editClick.click();
		generator.childReport("Edit Clicked");
		SeleniumUtils.presenceOfElement(driver, editHeader());
		SeleniumUtils.highLightElement(claimsEditHeader, driver);
		claimsEditType.click();
		claimsSelectTimeloss.click();
		generator.childReport("Claims type selected as Time Loss");
		claimsSave.click();
		generator.childReport("Save Clicked");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, claimType());
		SeleniumUtils.highLightElement(claimsType, driver);
		SeleniumUtils.presenceOfElement(driver, relatedTab());
		SeleniumUtils.highLightElement(claimsRelatedTab, driver);
		SeleniumUtils.elementToBeClickable(driver, relatedTab());
		Thread.sleep(5000);
		claimsRelatedTab.click();
		generator.childReport("Related Tab Clicked");
		SeleniumUtils.presenceOfElement(driver, relatedBLS());
		SeleniumUtils.highLightElement(claimsBLSVerify, driver);
		generator.childReport("BLS Record founded");
		SeleniumUtils.presenceOfElement(driver, numberBLS());
		SeleniumUtils.highLightElement(claimsBLSNumber, driver);
		SeleniumUtils.presenceOfElement(driver, recordBLS());
		SeleniumUtils.highLightElement(claimsBLSRecoordClick, driver);
		claimsBLSRecoordClick.click();
		generator.childReport("BLS Record Selected");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, verifyBLS());
		SeleniumUtils.highLightElement(claimsBLSRecoordVerify, driver);
		generator.childReport("BLS Record verified");
		return new BLSRecordPage(driver);
	}
	public void noneRecordClick(String methodName, String tcName, ReportGenerator generator)
			throws Exception{
		SeleniumUtils.presenceOfElement(driver, label());
		SeleniumUtils.highLightElement(claimsHeader, driver);
/*		SeleniumUtils.presenceOfElement(driver, dropDown());
		SeleniumUtils.highLightElement(narrowClick, driver);
		narrowClick.click();*/
		SeleniumUtils.highLightElement(editClick, driver);
		editClick.click();
		generator.childReport("Edit Clicked");
		SeleniumUtils.presenceOfElement(driver, editHeader());
		SeleniumUtils.highLightElement(claimsEditHeader, driver);
		claimsEditType.click();
		claimsSelectnone.click();
		generator.childReport("Claims type selected as Time Loss");
		claimsSave.click();
		generator.childReport("Save Clicked");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, claimType());
		SeleniumUtils.highLightElement(claimsType, driver);
		SeleniumUtils.presenceOfElement(driver, relatedTab());
		SeleniumUtils.highLightElement(claimsRelatedTab, driver);
		SeleniumUtils.elementToBeClickable(driver, relatedTab());
		claimsRelatedTab.click();
		generator.childReport("Related Tab Clicked");
		SeleniumUtils.presenceOfElement(driver, relatedBLS());
		SeleniumUtils.highLightElement(claimsBLSVerify, driver);
		claimsBLSVerify.click();
		generator.childReport("BLS verified");
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, deletedBLS());
		String noRecords = claimsBLSRecoordNotFound.getText();
		TestUtils.compareText(noRecords, GoogleSheetAPI.ReadData(methodName, tcName, "message"), claimsBLSRecoordNotFound, driver);
		generator.childReport("Got '"+ noRecords +"'message no records");	
	}
	public void checkOccupation(String methodName, String tcName, ReportGenerator generator)
			throws Exception{
		
		String occ = occupationCheck.getText();
		TestUtils.compareText(occ, GoogleSheetAPI.ReadData(methodName, tcName, "occupation"), occupationCheck, driver);
		generator.childReport("Got '"+ occ +"'name");
	}
}
