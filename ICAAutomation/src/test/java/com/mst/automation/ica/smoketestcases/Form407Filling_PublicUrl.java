/**
 * 
 */
package com.mst.automation.ica.smoketestcases;


import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

/**
 * @author Infant Raja Marshall
 * Created date: 22-Nov-2017
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form407Filling_PublicUrl extends BaseTest {
	@Test (groups = "Form Regression Suite")
	
	
	public void formfilling407() throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_407";
		
		reporter = new ReportGenerator(getbrowser(), className);
		page407 = loginPage.formUrl();
		page407.fillingForm407(methodName, tcName, reporter);

	}
}
