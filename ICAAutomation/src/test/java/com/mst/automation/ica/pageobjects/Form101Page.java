/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Aartheeswaran
 * Created date: Nov 30, 2017
 * Last Edited by: Aartheeswaran...
 * Last Edited date: Nov 30, 2017
 * Description: 
 *
 */
public class Form101Page extends DriverClass {
	
	// Claimant Details Div's attributes following:
	
	@FindBy(xpath = "//h2[contains(.,'INDUSTRIAL INJURY')]")
	public WebElement ica101Verify;
	
	@FindBy(xpath = "//label[text()='First Name']/following::input[1]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//label[text()='Last Name']/following::input[1]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//label[text()='Middle Initial']/following::input[1]")
	public WebElement claimantMiddleInitial;
	
	@FindBy(xpath = "//label[text()='Social Security Number']/following::input[1]")
	public WebElement socialSecurityNumber;
	
	@FindBy(xpath = "//label[text()='Date of Birth']/following::a")
	public WebElement claimantBirthDate;
	
	@FindBy(xpath = "//label[text()='Home Address']/following::input[1]")
	public WebElement claimantAddress;
	
	@FindBy(xpath = "//label[text()='City']/following::input[1]")
	public WebElement claimantCity;
	
	@FindBy(xpath = "//h3[contains(.,'Claimant')]/following::input[8]")
	public WebElement claimantState;
	
	@FindBy(xpath = "//label[text()='Zip Code']/following::input[1]")
	public WebElement claimantZipCode;
	
	@FindBy(xpath = "//label[text()='Telephone']/following::input[1]")
	public WebElement claimantTelephone;	
	
	@FindBy(xpath = "//label[text()='Telephone']/following::select[1]")
	public WebElement claimantSex;
	
	@FindBy(xpath = "//label[text()='Marital Status']/following::select[1]")
	public WebElement claimantMartialStatus;
	
	// Employer Details Div's Attributes following:
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[1]")
	public WebElement employerName;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[4]")
	public WebElement employerOfficeAddress;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[5]")
	public WebElement employerCity;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[6]")
	public WebElement employerState;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[7]")
	public WebElement employerZipCode;
	
	@FindBy(xpath = "//h3[contains(.,'Employer')]/following::input[8]")
	public WebElement employerTelephone;
	
	// Accident Details Div's Attributes following:
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::a[1]")
	public WebElement accidentDetailsDateofInjury;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::a[2]")
	public WebElement timeClaimantBeganWork;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::a[3]")
	public WebElement dateEmployerNotified;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::a[4]")
	public WebElement lastDayofWorkAfterInjury;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::input[7]")
	public WebElement claimantJobTitle;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::input[8]")
	public WebElement classCodeonPayroll;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::input[9]")
	public WebElement claimantAssignedDepartment;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::input[10]")
	public WebElement departmentNumber;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::textarea[2]")
	public WebElement whatWasTheInjury;
	
	@FindBy(xpath = "//h3[contains(.,'Accident Details')]/following::input[15]")
	public WebElement partOfBodyInjured;
	
	// Cause Of Accident Div's attributes following:	
	
	@FindBy(xpath = "//h3[contains(.,'Cause Of Accident')]/following::textarea[1]")
	public WebElement whatHappened;
	
	@FindBy(xpath = "//h3[contains(.,'Cause Of Accident')]/following::textarea[2]")
	public WebElement objectSubstance;
	
	@FindBy(xpath = "//h3[contains(.,'Cause Of Accident')]/following::textarea[3]")
	public WebElement whatWasClmtDoingBeforeIncident;
	
	// Employee's Wage Data
	
	@FindBy(xpath = "//h3[contains(.,'Wage Data')]/following::input[1]")
	public WebElement hoursPerDayClmtWorkedFrom;
	
	@FindBy(xpath = "//h3[contains(.,'Wage Data')]/following::input[2]")
	public WebElement hoursPerDayClmtWorkedTo;
	
	@FindBy(xpath = "//h3[contains(.,'Wage Data')]/following::input[3]")
	public WebElement daysPerWeekUsuallyWorkedCo;
	
	@FindBy(xpath = "//h3[contains(.,'Wage Data')]/following::input[4]")
	public WebElement daysPerWeekUsuallyWorkedClmt;
	
	// If Work Loss Is Expected To Exceed Seven Calendar Days, Complete Items In Below Section
	
	@FindBy(xpath = "//h3[contains(.,'Work Loss Is Expected')]/following::a[1]")
	public WebElement dateOfLastHire;
	
	// If Employee Is Paid Other Than Fixed Weekly Or Monthly Salary, Complete Items in below section
	
	@FindBy(xpath = "//h3[contains(.,'Employee Is Paid')]/following::input[13]")
	public WebElement submitterEmailAddress;
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By element() {
		By ul=By.xpath("//h2[contains(.,'INDUSTRIAL INJURY')]");
		return ul;
	}
	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifySuccess;
	
	public Form101Page(WebDriver driver){
		 super(driver);
	 }

	/*To fill the 101 Form*/
	
	public void fillingForm101(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica101Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant first name"));
		generator.childReport("Claimant First name entered");
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant last name"));
		generator.childReport("Claimant Last name entered");
		
		SeleniumUtils.highLightElement(claimantMiddleInitial, driver);
		claimantMiddleInitial.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant middle initial"));
		generator.childReport("Claimant Middle Initial entered");
		
		SeleniumUtils.highLightElement(socialSecurityNumber, driver);
		socialSecurityNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant social security number"));
		generator.childReport("SocialSecurityNumber entered");
		
		SeleniumUtils.highLightElement(claimantBirthDate, driver);
		//claimantBirthDate.sendKeys(ExcelUtility.readExcel(methodName, tcName, "claimant birth day"));
		claimantBirthDate.click();
		generator.childReport("Claimant BirthDate entered");
		
		SeleniumUtils.highLightElement(claimantAddress, driver);
		claimantAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant address"));
		generator.childReport("Claimant Address entered");
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		claimantCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant city"));
		generator.childReport("Claimant City entered");
		
		SeleniumUtils.highLightElement(claimantState, driver);
		claimantState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant state"));
		generator.childReport("ClaimantState entered");
		
		SeleniumUtils.highLightElement(claimantZipCode, driver);
		claimantZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant zip code"));
		generator.childReport("Claimant Zip Code entered");
		
		SeleniumUtils.highLightElement(claimantTelephone, driver);
		claimantTelephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantTelephone"));
		generator.childReport("Claimant Telephone entered");
		
		SeleniumUtils.dropDown(claimantMartialStatus, GoogleSheetAPI.ReadData(methodName, tcName, "claimant martial status"));
		generator.childReport("Claimants Martial Status selected");
		
		SeleniumUtils.highLightElement(employerName, driver);
		employerName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employer"));
		generator.childReport("Employer entered");
				
		SeleniumUtils.highLightElement(employerOfficeAddress, driver);
		employerOfficeAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerAddress"));
		generator.childReport("Employer Address entered");
		
		SeleniumUtils.highLightElement(employerCity, driver);
		employerCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerCity"));
		generator.childReport("Employer City entered");
		
		SeleniumUtils.highLightElement(employerState, driver);
		employerState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerState"));
		generator.childReport("Employer State entered");
		
		SeleniumUtils.highLightElement(employerZipCode, driver);
		employerZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerZipCode"));
		generator.childReport("Employer Zip Code entered");
		
		SeleniumUtils.highLightElement(employerTelephone, driver);
		employerTelephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerTelephone"));
		generator.childReport("Employer Telephone entered");
		
		SeleniumUtils.highLightElement(accidentDetailsDateofInjury, driver);
		accidentDetailsDateofInjury.click();
		generator.childReport("Accident Details Date of Injury entered");  
		
		SeleniumUtils.highLightElement(timeClaimantBeganWork, driver);
		timeClaimantBeganWork.click();
		generator.childReport("TimeClaimantBeganWork entered");
		
		SeleniumUtils.highLightElement(dateEmployerNotified, driver);
		dateEmployerNotified.click();
		generator.childReport("DateEmployerNotified entered");

		SeleniumUtils.highLightElement(lastDayofWorkAfterInjury, driver);
		lastDayofWorkAfterInjury.click();
		generator.childReport("Last Day of Work After Injury entered");

		SeleniumUtils.highLightElement(claimantJobTitle, driver);
		claimantJobTitle.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantJobTitle"));
		generator.childReport("Claimant Job Title entered");

		SeleniumUtils.highLightElement(classCodeonPayroll, driver);
		classCodeonPayroll.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "classCodeonPayroll"));
		generator.childReport("Class Code on Payroll entered");

		SeleniumUtils.highLightElement(claimantAssignedDepartment, driver);
		claimantAssignedDepartment.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantAssignedDepartment"));
		generator.childReport("Claimant Assigned Department entered"); 
		
		SeleniumUtils.highLightElement(departmentNumber, driver);
		departmentNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "departmentNumber"));
		generator.childReport("DepartmentNumber entered");

		SeleniumUtils.highLightElement(whatWasTheInjury, driver);
		whatWasTheInjury.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "whatWasTheInjury"));
		generator.childReport("WhatWasTheInjury entered");

		SeleniumUtils.highLightElement(partOfBodyInjured, driver);
		partOfBodyInjured.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "partOfBodyInjured"));
		generator.childReport("PartOfBodyInjured entered");

		SeleniumUtils.highLightElement(whatHappened, driver);
		whatHappened.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "whatHappened"));
		generator.childReport("WhatHappened entered");

		SeleniumUtils.highLightElement(objectSubstance, driver);
		objectSubstance.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "objectSubstance"));
		generator.childReport("ObjectSubstance entered");

		SeleniumUtils.highLightElement(whatWasClmtDoingBeforeIncident, driver);
		whatWasClmtDoingBeforeIncident.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "whatWasClmtDoingBeforeIncident"));
		generator.childReport("WhatWasClmtDoingBeforeIncident entered");

		SeleniumUtils.highLightElement(hoursPerDayClmtWorkedFrom, driver);
		hoursPerDayClmtWorkedFrom.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "hoursPerDayClmtWorkedFrom"));
		generator.childReport("HoursPerDayClmtWorkedFrom entered");

		SeleniumUtils.highLightElement(hoursPerDayClmtWorkedTo, driver);
		hoursPerDayClmtWorkedTo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "hoursPerDayClmtWorkedTo"));
		generator.childReport("HoursPerDayClmtWorkedTo entered");

		SeleniumUtils.highLightElement(daysPerWeekUsuallyWorkedCo, driver);
		daysPerWeekUsuallyWorkedCo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "daysPerWeekUsuallyWorkedCo"));		
		generator.childReport("DaysPerWeekUsuallyWorkedCo entered");

		SeleniumUtils.highLightElement(daysPerWeekUsuallyWorkedClmt, driver);
		daysPerWeekUsuallyWorkedClmt.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "daysPerWeekUsuallyWorkedClmt"));
		generator.childReport("DaysPerWeekUsuallyWorkedClmt entered");
		
		SeleniumUtils.highLightElement(dateOfLastHire, driver);
		dateOfLastHire.click();
		generator.childReport("DateOfLastHire entered");  

		SeleniumUtils.highLightElement(submitterEmailAddress, driver);
		submitterEmailAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitterEmailAddress"));
		generator.childReport("SubmitterEmailAddress entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("Submitbutton entered");
		
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);
		
}
public void fillingForm101Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	SeleniumUtils.presenceOfElement(driver, community());
	
	SeleniumUtils.switchToFrame(driver, frameswitch);
	
	generator.childReport("Signature frame switched");
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica101Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant first name"));
		generator.childReport("Claimant First name entered");
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant last name"));
		generator.childReport("Claimant Last name entered");
		
		SeleniumUtils.highLightElement(claimantMiddleInitial, driver);
		claimantMiddleInitial.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant middle initial"));
		generator.childReport("claimantMiddleInitial entered");
		
		SeleniumUtils.highLightElement(socialSecurityNumber, driver);
		socialSecurityNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant social security number"));
		generator.childReport("socialSecurityNumber entered");
		
		SeleniumUtils.highLightElement(claimantBirthDate, driver);
		//claimantBirthDate.sendKeys(ExcelUtility.readExcel(methodName, tcName, "claimant birth day"));
		claimantBirthDate.click();
		generator.childReport("claimantBirthDate entered");
		
		SeleniumUtils.highLightElement(claimantAddress, driver);
		claimantAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant address"));
		generator.childReport("claimantAddress entered");
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		claimantCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant city"));
		generator.childReport("claimantCity entered");
		
		SeleniumUtils.highLightElement(claimantState, driver);
		claimantState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant state"));
		generator.childReport("claimantState entered");
		
		SeleniumUtils.highLightElement(claimantZipCode, driver);
		claimantZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant zip code"));
		generator.childReport("claimantZipCode entered");
		
		SeleniumUtils.highLightElement(claimantTelephone, driver);
		claimantTelephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantTelephone"));
		generator.childReport("claimantTelephone entered");
		
		SeleniumUtils.dropDown(claimantMartialStatus, GoogleSheetAPI.ReadData(methodName, tcName, "claimant martial status"));
		generator.childReport("Claimants Martial Status selected");
		
		SeleniumUtils.highLightElement(employerName, driver);
		employerName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employer"));
		generator.childReport("employer entered");
				
		SeleniumUtils.highLightElement(employerOfficeAddress, driver);
		employerOfficeAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerAddress"));
		generator.childReport("employerAddress entered");
		
		SeleniumUtils.highLightElement(employerCity, driver);
		employerCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerCity"));
		generator.childReport("employerCity entered");
		
		SeleniumUtils.highLightElement(employerState, driver);
		employerState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerState"));
		generator.childReport("employerState entered");
		
		SeleniumUtils.highLightElement(employerZipCode, driver);
		employerZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerZipCode"));
		generator.childReport("employerZipCode entered");
		
		SeleniumUtils.highLightElement(employerTelephone, driver);
		employerTelephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employerTelephone"));
		generator.childReport("employerTelephone entered");
		
		SeleniumUtils.highLightElement(accidentDetailsDateofInjury, driver);
		accidentDetailsDateofInjury.click();
		generator.childReport("accidentDetailsDateofInjury entered");  
		
		SeleniumUtils.highLightElement(timeClaimantBeganWork, driver);
		timeClaimantBeganWork.click();
		generator.childReport("timeClaimantBeganWork entered");
		
		SeleniumUtils.highLightElement(dateEmployerNotified, driver);
		dateEmployerNotified.click();
		generator.childReport("dateEmployerNotified entered");

		SeleniumUtils.highLightElement(lastDayofWorkAfterInjury, driver);
		lastDayofWorkAfterInjury.click();
		generator.childReport("lastDayofWorkAfterInjury entered");

		SeleniumUtils.highLightElement(claimantJobTitle, driver);
		claimantJobTitle.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantJobTitle"));
		generator.childReport("claimantJobTitle entered");

		SeleniumUtils.highLightElement(classCodeonPayroll, driver);
		classCodeonPayroll.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "classCodeonPayroll"));
		generator.childReport("classCodeonPayroll entered");

		SeleniumUtils.highLightElement(claimantAssignedDepartment, driver);
		claimantAssignedDepartment.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantAssignedDepartment"));
		generator.childReport("claimantAssignedDepartment entered"); 
		
		SeleniumUtils.highLightElement(departmentNumber, driver);
		departmentNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "departmentNumber"));
		generator.childReport("departmentNumber entered");

		SeleniumUtils.highLightElement(whatWasTheInjury, driver);
		whatWasTheInjury.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "whatWasTheInjury"));
		generator.childReport("whatWasTheInjury entered");

		SeleniumUtils.highLightElement(partOfBodyInjured, driver);
		partOfBodyInjured.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "partOfBodyInjured"));
		generator.childReport("partOfBodyInjured entered");

		SeleniumUtils.highLightElement(whatHappened, driver);
		whatHappened.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "whatHappened"));
		generator.childReport("whatHappened entered");

		SeleniumUtils.highLightElement(objectSubstance, driver);
		objectSubstance.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "objectSubstance"));
		generator.childReport("objectSubstance entered");

		SeleniumUtils.highLightElement(whatWasClmtDoingBeforeIncident, driver);
		whatWasClmtDoingBeforeIncident.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "whatWasClmtDoingBeforeIncident"));
		generator.childReport("whatWasClmtDoingBeforeIncident entered");

		SeleniumUtils.highLightElement(hoursPerDayClmtWorkedFrom, driver);
		hoursPerDayClmtWorkedFrom.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "hoursPerDayClmtWorkedFrom"));
		generator.childReport("hoursPerDayClmtWorkedFrom entered");

		SeleniumUtils.highLightElement(hoursPerDayClmtWorkedTo, driver);
		hoursPerDayClmtWorkedTo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "hoursPerDayClmtWorkedTo"));
		generator.childReport("hoursPerDayClmtWorkedTo entered");

		SeleniumUtils.highLightElement(daysPerWeekUsuallyWorkedCo, driver);
		daysPerWeekUsuallyWorkedCo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "daysPerWeekUsuallyWorkedCo"));		
		generator.childReport("daysPerWeekUsuallyWorkedCo entered");

		SeleniumUtils.highLightElement(daysPerWeekUsuallyWorkedClmt, driver);
		daysPerWeekUsuallyWorkedClmt.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "daysPerWeekUsuallyWorkedClmt"));
		generator.childReport("daysPerWeekUsuallyWorkedClmt entered");
		
		SeleniumUtils.highLightElement(dateOfLastHire, driver);
		dateOfLastHire.click();
		generator.childReport("dateOfLastHire entered");  

		SeleniumUtils.highLightElement(submitterEmailAddress, driver);
		submitterEmailAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitterEmailAddress"));
		generator.childReport("submitterEmailAddress entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);
		
}


}