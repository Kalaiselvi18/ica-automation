package com.mst.automation.ica.extentreport;



import com.mst.automation.ica.constant.Constant;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;


/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This class generates the reporter object in order to work with the Extent Report.
 */
public class ExtentReportFactory {
	
	private static ExtentReports reporter;
		 
    public static synchronized ExtentReports getReporter() {
           if (reporter == null) {
                  reporter = new ExtentReports(Constant.REPORTPATH, true, DisplayOrder.NEWEST_FIRST);
           }
           return reporter;
    }

 }

