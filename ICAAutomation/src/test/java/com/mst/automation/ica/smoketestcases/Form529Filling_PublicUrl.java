/**
 * 
 */
package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

/**
 * @author Infant Raja Marshall
 * Created date: 18-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form529Filling_PublicUrl extends BaseTest{
@Test (groups = "Form Smoke Suite")
	
	
	public void formfilling529() throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_529";
		
		reporter = new ReportGenerator(getbrowser(), className);
		page529 = loginPage.formUrl529();
		page529.fillingForm529(methodName, tcName, reporter);

	}

}
