package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To check permissions for BLS Record Page
 *
 */

public class BLSRecordPage extends DriverClass {
	
	static String getRecord;

	@FindBy(xpath = "//span[text()='DOI']/following::span[1]")
	public WebElement doiVerify;

	@FindBy(xpath = "//span[text()='BLS Number']/following::span[2]")
	public WebElement verifyBLS;

	public By blsVerify() {
		By bls = By.xpath("//a[text()='BLS']");
		return bls;
	}

	@FindBy(xpath = "//span[text()='BLS Number']/following::span[8]")
	public WebElement blsStatus;
	
	@FindBy(xpath = "//span[text()='BLS Number']/following::span[5]")
	public WebElement blsOwner;
	
	@FindBy(xpath = "//span[text()='BLS Number']/following::span[25]")
	public WebElement blsDob;
	
	@FindBy(xpath = "//span[text()='BLS Number']/following::span[28]")
	public WebElement blsDoi;
	
	@FindBy(css = "a[title='Edit']")
	public WebElement clickEdit;
	
	public By editShown() {
		By edit = By.cssSelector("a[title='Edit']");
		return edit;
	}
	public By header() {
		By h2 = By.xpath("//h2[contains(.,'Edit BLS')]");
		return h2;
	}
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]")
	public WebElement verifyHeader;
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::a[1]")
	public WebElement blsStatusEdit;
	
	@FindBy(css = "a[title='Finalized']")
	public WebElement blsFinalized;
		
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::button[3]")
	public WebElement saveClick;
	
	@FindBy(xpath = "//span[contains(.,'Review')]")
	public WebElement error;
	
	public By errorReview() {
		By er = By.xpath("//span[contains(.,'Review')]");
		return er;
	}
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::input[1]")
	public WebElement blsBodypart;
	
	public By bodyPart() {
		By bop = By.cssSelector("div[title='9999 - Nonclassifiable']");
		return bop;
	}
	
	@FindBy(css = "div[title='9999 - Nonclassifiable']")
	public WebElement blsBodypartRecord;
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::input[2]")
	public WebElement nature;
	
	public By natureRec() {
		By bop = By.cssSelector("div[title='9999 - Nonclassifiable']");
		return bop;
	}
	
	@FindBy(css = "div[title='9999 - Nonclassifiable']")
	public WebElement natureRecordSelect;
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::input[3]")
	public WebElement event;
	
	public By eventRec() {
		By eve = By.cssSelector("div[title='9999 - Nonclassifiable']");
		return eve;
	}
	
	@FindBy(css = "div[title='9999 - Nonclassifiable']")
	public WebElement eventRecordSelect;
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::input[4]")
	public WebElement secondarySource;
	
	public By secondary() {
		By sor = By.cssSelector("div[title='9999 - Nonclassifiable']");
		return sor;
	}
	
	@FindBy(css = "div[title='9999 - Nonclassifiable']")
	public WebElement secSourceSelect;
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::input[5]")
	public WebElement source;
	
	public By source() {
		By sec = By.cssSelector("div[title='9999 - Nonclassifiable']");
		return sec;
	}
	
	@FindBy(css = "div[title='9999 - Nonclassifiable']")
	public WebElement sourceSelect;
	
	@FindBy(xpath = "//h2[contains(.,'Edit BLS')]/following::input[7]")
	public WebElement naics;
	
	public By naicsRec() {
		By na = By.cssSelector("div[title='999999 - Nonclassifiable']");
		return na;
	}
	
	@FindBy(css = "div[title='999999 - Nonclassifiable']")
	public WebElement naicsReocrdSelect;
	
	
	@FindBy(xpath = "//span[text()='Ownership']/following::a[1]")
	public WebElement ownerShip;
	
	@FindBy(css = "a[title='50 - Private']")
	public WebElement private50;
	
	@FindBy(xpath = "//span[text()='County']/following::a[1]")
	public WebElement county;
	
	@FindBy(css = "a[title='001 - Apache']")
	public WebElement apache001;
	
	@FindBy(xpath = "//span[text()='Claims Body Part']/following::span[1]")
	public WebElement claimsBody;
	
	@FindBy(xpath = "//span[text()='DOB']/following::span[1]")
	public WebElement dob;
	
	@FindBy(xpath = "//span[text()='DOI']/following::span[1]")
	public WebElement doi;
	
	@FindBy(xpath = "//span[text()='Sex']/following::span[1]")
	public WebElement sex;
	
	@FindBy(xpath = "//span[text()='Employer']/following::span[1]")
	public WebElement employer;
	
	public By recVerify() {
		By rv = By.cssSelector("a[title='20180131958']");
		return rv;
	}
	
	@FindBy(css = "a[title='20180131958']")
	public WebElement recordName;
	
	@FindBy(xpath = "//a[@title='20180131958']/following::a")
	public WebElement recordNameEdit;
	
	@FindBy(xpath = "//span[text()='Occupation']/following::a[3]")
	public WebElement deleteOccupation;
	
	@FindBy(css = "input[title='Search Occupations']")
	public WebElement enterOccupation;
	
	public By occupationRec() {
		By occ = By.cssSelector("div[title='097 - Dietitians']");
		return occ;
	}
	
	@FindBy(css = "div[title='097 - Dietitians']")
	public WebElement occupationRecordSelect;

	@FindBy(xpath = "//li[3]/a/span[text()='BLS']")
	public WebElement tabBLS;
	
	@FindBy(xpath = "//span[text()='Home']/following::h1/a/div/span[1]")
	public WebElement viewDropdown;
	
	@FindBy(xpath = "//span[text()='BLS Worker Queue']")
	public WebElement viewBlsWorkerQueue;
	
	@FindBy(xpath = "//div[text()='Accept BLS']")
	public WebElement acceptBLS;
	
	public By blsVerifyh1() {
		By bls = By.xpath("//span[text()='Home']/following::h1/a/div/span[1]");
		return bls;
	}
	
	public By blsQueue() {
		By queue = By.xpath("//span[text()='BLS Worker Queue']");
		return queue;
	}
	
	public By blsNumber() {
		By blsno = By.xpath("//span[text()='BLS Number']");
		return blsno;
	}
	

	

	public BLSRecordPage(WebDriver driver) {
		super(driver);
	}

	/*To verify the fields in record page*/
	public void recordPageBls(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		String statusActual = blsStatus.getText();
		TestUtils.compareText(statusActual, GoogleSheetAPI.ReadData(methodName, tcName, "status"), blsStatus, driver);
		generator.childReport("BLS Status verified");
		String ownerActual = blsOwner.getText();
		TestUtils.compareText(ownerActual, GoogleSheetAPI.ReadData(methodName, tcName, "owner"), blsOwner, driver);
		generator.childReport("BLS Owner verified");
		String doiActual = blsDoi.getText();
		TestUtils.compareText(doiActual, GoogleSheetAPI.ReadData(methodName, tcName, "doi"), blsDoi, driver);
		generator.childReport("BLS DOI verified");
		String dobActual = blsDob.getText();
		TestUtils.compareText(dobActual, GoogleSheetAPI.ReadData(methodName, tcName, "dob"), blsDob, driver);
		generator.childReport("BLS DOB verified");
		getRecord = verifyBLS.getText();
		generator.childReport("BLS Record "+getRecord+"verified");
		System.out.println(getRecord);


	}
	public ClaimsEditPage verificationBlsWorker(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		tabBLS.click();
		SeleniumUtils.presenceOfElement(driver, blsVerifyh1());
		SeleniumUtils.highLightElement(viewDropdown, driver);
		viewDropdown.click();
		SeleniumUtils.presenceOfElement(driver, blsQueue());
		SeleniumUtils.highLightElement(viewBlsWorkerQueue, driver);
		viewBlsWorkerQueue.click();
		SeleniumUtils.presenceOfElement(driver, blsNumber());
		String str = "//table";
		Thread.sleep(5000);
		//CommonUtils.checkBoxTable(str, ExcelUtility.readExcel(methodName, tcName, "record no"), driver);
		CommonUtils.checkBoxTable(str, getRecord, driver);
		acceptBLS.click();
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, blsNumber());
		SeleniumUtils.presenceOfElement(driver, blsQueue());
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(viewBlsWorkerQueue, driver);
		//CommonUtils.dropdownTable(str, ExcelUtility.readExcel(methodName, tcName, "record no"), driver);
		CommonUtils.dropdownTable(str, getRecord, driver);
		SeleniumUtils.presenceOfElement(driver, editShown());
		SeleniumUtils.highLightElement(clickEdit, driver);
		clickEdit.click();
		SeleniumUtils.presenceOfElement(driver, header());
		SeleniumUtils.highLightElement(verifyHeader, driver);
		

		
		blsStatusEdit.click();
		blsFinalized.click();
		generator.childReport("Changed as Finalized");
		saveClick.click();
		SeleniumUtils.presenceOfElement(driver, errorReview());
		String dobActual = error.getText();
		TestUtils.compareText(dobActual, GoogleSheetAPI.ReadData(methodName, tcName, "Error"), error, driver);
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(blsBodypart, driver);
		//blsBodypart.click();
		blsBodypart.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "blsBodypart"));
		blsBodypart.click();
		SeleniumUtils.presenceOfElement(driver, bodyPart());
		SeleniumUtils.highLightElement(blsBodypartRecord, driver);
		blsBodypartRecord.click();
		generator.childReport("BLS Body part record Selected");
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(nature, driver);
		//nature.click();
		nature.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "nature"));
		nature.click();
		SeleniumUtils.presenceOfElement(driver, natureRec());
		SeleniumUtils.highLightElement(natureRecordSelect, driver);
		natureRecordSelect.click();
		generator.childReport("natureRecord Selected");
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(event, driver);
		//event.click();
		event.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "event"));
		event.click();
		SeleniumUtils.presenceOfElement(driver, eventRec());
		SeleniumUtils.highLightElement(eventRecordSelect, driver);
		eventRecordSelect.click();
		generator.childReport("eventRecord Selected");
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(secondarySource, driver);
		//secondarySource.click();
		secondarySource.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "secondarySource"));
		secondarySource.click();
		SeleniumUtils.presenceOfElement(driver, secondary());
		SeleniumUtils.highLightElement(secSourceSelect, driver);
		secSourceSelect.click();
		generator.childReport("secondarySource Selected");
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(source, driver);
		//source.click();
		source.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "source"));
		source.click();
		SeleniumUtils.presenceOfElement(driver, source());
		SeleniumUtils.highLightElement(sourceSelect, driver);
		sourceSelect.click();
		generator.childReport("sourceRecord Selected");
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(naics, driver);
		//naics.click();
		naics.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "naics"));
		naics.click();
		SeleniumUtils.presenceOfElement(driver, naicsRec());
		SeleniumUtils.highLightElement(naicsReocrdSelect, driver);
		naicsReocrdSelect.click();
		generator.childReport("naicsReocrd Selected");
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(claimsBody, driver);
		SeleniumUtils.highLightElement(dob, driver);
		SeleniumUtils.highLightElement(doi, driver);
		SeleniumUtils.highLightElement(sex, driver);
		SeleniumUtils.highLightElement(employer, driver);
		Thread.sleep(5000);
		ownerShip.click();
		private50.click();
		county.click();
		apache001.click();

		saveClick.click();
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, recVerify());
		SeleniumUtils.highLightElement(recordName, driver);
		recordNameEdit.click();
		SeleniumUtils.presenceOfElement(driver, editShown());
		SeleniumUtils.highLightElement(clickEdit, driver);
		clickEdit.click();
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, header());
		SeleniumUtils.highLightElement(verifyHeader, driver);
		deleteOccupation.click();
		enterOccupation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "occupation"));
		SeleniumUtils.presenceOfElement(driver, occupationRec());
		SeleniumUtils.highLightElement(occupationRecordSelect, driver);
		occupationRecordSelect.click();
		saveClick.click();
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, recVerify());
		SeleniumUtils.highLightElement(recordName, driver);
		recordName.click();
		Thread.sleep(5000);
		
		return new ClaimsEditPage(driver);
	}
	
}
