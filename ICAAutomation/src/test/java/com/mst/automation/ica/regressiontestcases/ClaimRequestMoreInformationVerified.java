package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class ClaimRequestMoreInformationVerified extends BaseTest{
	@Test (groups = "Regression Suite")
	@Parameters({ "userType", "UrlQA" })
		
		public void moreInformationnewclaim(String userType, String UrlQA) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_052";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		String Url2 = TestUtils.getStringFromPropertyFile(UrlQA);
		
		String user2 =TestUtils.getStringFromPropertyFile(userType+".usernameClaimProcessor");
		String pwd2 = TestUtils.getStringFromPropertyFile(userType+".passwordClaimProcessor");
		
		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.loginCommunity(user, pwd, reporter);
		claimsTab = homePage.claimStatusVerify();
		claimsTab.verifyClaimRequestMoreInformation(methodName, tcName, reporter);
		
		SeleniumUtils.switchToNewTab(driver, Url2);
		homePage = loginPage.login(user2, pwd2, reporter);
		 claimsrecordpage = homePage.globalClaimTask(methodName, tcName, reporter);
		 pageClaimTask = claimsrecordpage.recordPageClaimTask(methodName, tcName, reporter);
		 pageClaimTask.verifyPendingClaimRequest(methodName, tcName, reporter);	
		


	}

}
