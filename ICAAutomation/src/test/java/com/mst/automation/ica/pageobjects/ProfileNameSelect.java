package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To select the profile
 *
 */

public class ProfileNameSelect extends DriverClass {

	@FindBy(how = How.CSS, using = "input[class='searchBox input']")
	public WebElement profileName;
		
	@FindBy(how = How.CSS, using = "")
	public WebElement claimsManagerClick;

	@FindBy(how = How.XPATH, using = "//h2[contains(.,'Claims Manager')]")
	public WebElement claimsManagerVerify;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'User Profiles')]")
	public WebElement userProfile;

	public ProfileNameSelect(WebDriver driver) {
		super(driver);
	}

	/*To navigate to the Claims Manager Profile*/
	public ClaimsManagerProfile nameProfile001(String methodName, String tcName, ReportGenerator generator)
			throws Exception {

		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str =".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Claims Manager Clicked");
		return new ClaimsManagerProfile(driver);
	}
	
	/*To navigate to the Claims Processor Profile*/
	public ClaimsProcessorProfile nameProfile002(String methodName, String tcName, ReportGenerator generator)
			throws Exception {
		
		

		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str =".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Claims Processor Clicked");
		return new ClaimsProcessorProfile(driver);
	}

	/*To navigate to the BLS Manager Profile*/
	public BLSManagerProfile nameProfile003(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	
		
		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");

		String str =".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("BLS Manager Profile Clicked");
		return new BLSManagerProfile(driver);
	}
	
	/*To navigate to the BLS Worker Profile*/
	public BLSWorkerProfile nameProfile021(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	
		
		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");

		String str =".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("BLS Worker Profile Clicked");
		return new BLSWorkerProfile(driver);
	}
	
	/*To navigate to the System Administrator Profile*/
	public SystemAdministratorProfile nameProfile022(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	
		
		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");

		String str =".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("System Administrator Profile Clicked");
		return new SystemAdministratorProfile(driver);
	}

	/*To navigate to the Claimant Profile*/
	public ClaimantProfile nameProfile004(String methodName, String tcName, ReportGenerator generator)
			throws Exception {
		
		
		
		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str =".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Claimant Profile Clicked");
		return new ClaimantProfile(driver);
	}

	/*To navigate to the Employer Profile*/
	public EmployerProfile nameProfile005(String methodName, String tcName, ReportGenerator generator)
			throws Exception {
		
	

		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str = ".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Employer Profile Clicked");
		return new EmployerProfile(driver);
	}

	/*To navigate to the Carrier Profile*/
	public CarrierProfile nameProfile006(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		
		
		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str = ".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Carrier Profile Clicked");
		return new CarrierProfile(driver);
	}

	/*To navigate to the Interested Party Profile*/
	public InterestedPartyProfile nameProfile007(String methodName, String tcName, ReportGenerator generator)
			throws Exception {
		
		

		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str = ".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Interested Party Profile Clicked");
		return new InterestedPartyProfile(driver);
	}

	/*To navigate to the Claims Attorney Profile*/
	public ClaimantAttorneyProfile nameProfile008(String methodName, String tcName, ReportGenerator generator)
			throws Exception {
		
		
		
		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str = ".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Claimant's Attorney Profile Clicked");
		return new ClaimantAttorneyProfile(driver);
	}

	/*To navigate to the Carrier Attorney Profile*/
	public CarrierAttorneyProfile nameProfile009(String methodName, String tcName, ReportGenerator generator)
			throws Exception {
		

		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");
		String str = ".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "profile_name"), driver);
		generator.childReport("Claimant's Attorney Profile Clicked");
		return new CarrierAttorneyProfile(driver);
	}
	/*All profile verification*/
public void allProfiles(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	
		
		String iframe = "iframe[title='User Profiles ~ Salesforce - Unlimited Edition']";
		driver.switchTo().frame(driver.findElement(By.cssSelector(iframe)));
		generator.childReport("Switched to frame");

		String str =".list tbody .dataRow th a";
		SeleniumUtils.highLightElement(userProfile, driver);
		CommonUtils.tableIVerify(str, GoogleSheetAPI.ReadData(methodName, tcName, "Claims Processor"), driver);
		generator.childReport("Claim Processor Profile Verified");
		CommonUtils.tableIVerify(str, GoogleSheetAPI.ReadData(methodName, tcName, "Claims Manager"), driver);
		generator.childReport("Claim Manager Profile Verified");
		CommonUtils.tableIVerify(str, GoogleSheetAPI.ReadData(methodName, tcName, "BLS Manager"), driver);
		generator.childReport("BLS Manager Profile Verified");
		CommonUtils.tableIVerify(str, GoogleSheetAPI.ReadData(methodName, tcName, "BLS Worker"), driver);
		generator.childReport("BLS Worker Profile Verified");
		CommonUtils.tableIVerify(str, GoogleSheetAPI.ReadData(methodName, tcName, "System Administrator"), driver);
		generator.childReport("System Administrator Profile Verified");
		CommonUtils.tableIVerify(str, GoogleSheetAPI.ReadData(methodName, tcName, "Community-Claimant"), driver);
		generator.childReport("Community-Claimant Profile Verified");
		
	}

}
