/**
 * 
 */
package com.mst.automation.ica.abstractclasses;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import com.mst.automation.ica.extentreport.ReportGenerator;


/**
 * @author Infant Raja Marshall Created date: 14-Nov-2017 Last Edited by: Infant
 *         Raja Marshall... Last Edited date: Description:
 *
 */
public abstract class GlobalDeclarations  {
	
	
	
	
	
	
	

	protected WebDriver driver;

	public WebDriver getdriver() {
		return driver;
	}

	public void setdriver(WebDriver driver) {
		this.driver = driver;
	}

	protected String url;

	public String geturl() {
		return url;
	}

	public void seturl(String url) {
		this.url = url;
	}

	protected String mBrowserType;

	public String getmBrowserType() {
		return mBrowserType;
	}

	public void setmBrowserType(String mBrowserType) {
		this.mBrowserType = mBrowserType;
	}

	protected String browser;

	public String getbrowser() {
		return browser;
	}

	public void setbrowser(String browser) {
		this.browser = browser;
	}

	protected String platform;

	public String getplatform() {
		return platform;
	}

	public void setplatform(String platform) {
		this.platform = platform;
	}

	protected String browserVersion;

	public String getbrowserVersion() {
		return browserVersion;
	}

	public void setbrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}

	protected Map<String, List<String>> resultMap;

	public void setresultMap(Map<String, List<String>> resultMap) {
		this.resultMap = resultMap;

	}

	public Map<String, List<String>> getresultMap() {
		return this.resultMap;

	}

	protected List<String> results;

	public void setresults(List<String> results) {
		this.results = results;
	}

	public List<String> getresults() {
		return this.results;
	}

	protected ReportGenerator reporter;

	public ReportGenerator getreporter() {
		return reporter;
	}

	public void setreporter(ReportGenerator reporter) {
		this.reporter = reporter;
	}
	
}
