/**
 * 
 */
package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

/**
 * @author Infant Raja Marshall
 * Created date: 18-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form528Filling_PublicUrl extends BaseTest{
@Test (groups = "Form Smoke Suite")
	
	
	public void formfilling528() throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_528";
		
		reporter = new ReportGenerator(getbrowser(), className);
		page528 = loginPage.formUrl528();
		page528.fillingForm528(methodName, tcName, reporter);

	}

}
