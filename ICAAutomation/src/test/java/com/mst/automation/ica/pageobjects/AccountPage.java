/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.ica.pageobjects;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.customexception.CustomException;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Feb 11, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
public class AccountPage extends DriverClass {

	@FindBy(xpath="//span[text()='Claimant First Name']/following::span[1]/span")
	public WebElement claimantFirstName;
	
	@FindBy(xpath="//span[text()='Claimant Last Name']/following::span[1]/span")
	public WebElement claimantLastName;
	
	@FindBy(xpath="//span[text()='Claimant First Name']/following::div[15]/span")
	public WebElement phone;
	
	@FindBy(xpath="//span[text()='Delivery Channel']/following::span[1]/span")
	public WebElement deliveryChannel;
	
	@FindBy(xpath="//span[text()='Shipping Address']/following::span[1]/a/div[@class='slds-truncate forceOutputAddressText']")
	public WebElement shippingAddress;	
		
	@FindBy(xpath="//span[text()='Claimant Account']/following::span[1]")
	public WebElement claimantAccount;
	
	@FindBy(xpath="//span[text()='Claimant Account']/following::div[7]/span/span")
	public WebElement lastName;
		
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='SSN']/following::div[1]/span/span")
	public WebElement ssn;
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='Marital Status']/following::div[1]/span/span")
	public WebElement maritalStatus;

	@FindBy(xpath="//span[text()='Account Name']/following::span[1]/span")
	public WebElement accountName;
	
	@FindBy(xpath="//span[text()='Account Record Type']/following::span[1]/div/div/span")
	public WebElement accountRecordType;
	
	public AccountPage(WebDriver driver) {
		super(driver);
	}
	
	public void verifyClaimantAccount(String methodName, String tcName, ReportGenerator reporter) throws InterruptedException, IOException {

		Thread.sleep(5000);
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		reporter.childReport("Verify the Claimant Firstname");
		String expectedClaimantFN = GoogleSheetAPI.ReadData(methodName, tcName, "claimant fn");
		SeleniumUtils.highlightElementBasedOnResult(claimantFirstName, expectedClaimantFN, driver);
			
		SeleniumUtils.highLightElement(claimantAccount, driver);
        reporter.childReport("Verify the Claimant Last Name");
        String cAccount = claimantLastName.getText();
        if(methodName.contains("ClaimantAccountMapping")) {
            cAccount.contains(Form102Page.form102LastName);
        }
        else {
            String expectedClaimantLN = GoogleSheetAPI.ReadData(methodName, tcName, "claimant ln");
            SeleniumUtils.highlightElementBasedOnResult(claimantLastName, expectedClaimantLN, driver);
        }
		
		Thread.sleep(2000);
		SeleniumUtils.highLightElement(deliveryChannel, driver);
		reporter.childReport("Verify the Claimant Phone");
		String expectedPhone = GoogleSheetAPI.ReadData(methodName, tcName, "phone");
		SeleniumUtils.highlightElementBasedOnResult(phone, expectedPhone, driver);
		
		Thread.sleep(2000);
		SeleniumUtils.highLightElement(deliveryChannel, driver);
		reporter.childReport("Verify the Claimant Delivery Channel");
		String expectedDeliveryChannel = GoogleSheetAPI.ReadData(methodName, tcName, "deliverychannel");
		SeleniumUtils.highlightElementBasedOnResult(deliveryChannel, expectedDeliveryChannel, driver);

		Thread.sleep(2000);
		SeleniumUtils.highLightElement(shippingAddress, driver);
		reporter.childReport("Verify the Claimant Address");
		String expectedAddress = GoogleSheetAPI.ReadData(methodName, tcName, "shippingaddress");
		String actualAddress = shippingAddress.getText(); 
		if(!expectedAddress.contains(actualAddress))
			throw new CustomException("The Claimant Shipping address is not matched");
		
		Thread.sleep(2000);
		SeleniumUtils.highLightElement(ssn, driver);
		reporter.childReport("Verify the Claimant SSN");
		String expectedSSN = GoogleSheetAPI.ReadData(methodName, tcName, "ssn");
		SeleniumUtils.highlightElementBasedOnResult(ssn, expectedSSN, driver);
		
		Thread.sleep(2000);
		SeleniumUtils.highLightElement(maritalStatus, driver);
		reporter.childReport("Verify the Claimant Marital Status");
		String expectedMarital = GoogleSheetAPI.ReadData(methodName, tcName, "maritalstatus");
		SeleniumUtils.highlightElementBasedOnResult(maritalStatus, expectedMarital, driver);
	}


	public void verifyEmployerAccount(String methodName, String tcName, ReportGenerator reporter) throws InterruptedException, IOException {
        
        Thread.sleep(5000);
        SeleniumUtils.highLightElement(accountName, driver);
        reporter.childReport("Verify the Employer Account Name");
        String expectedAccountName = GoogleSheetAPI.ReadData(methodName, tcName, "accountname");
        SeleniumUtils.highlightElementBasedOnResult(accountName, expectedAccountName, driver);
        
        Thread.sleep(2000);
        SeleniumUtils.highLightElement(accountRecordType, driver);
        reporter.childReport("Verify the Employer Account Record Type");
        String expectedAccountRT = GoogleSheetAPI.ReadData(methodName, tcName, "recordtype");
        SeleniumUtils.highlightElementBasedOnResult(accountRecordType, expectedAccountRT, driver);
        
        Thread.sleep(2000);
        SeleniumUtils.highLightElement(shippingAddress, driver);
        reporter.childReport("Verify the Employer Address");
        String expectedAddress = GoogleSheetAPI.ReadData(methodName, tcName, "employershippingaddress");
        String actualAddress = shippingAddress.getText(); 
        if(!expectedAddress.contains(actualAddress))
             throw new CustomException("The Employer Shipping address is not matched");  
	}
}
