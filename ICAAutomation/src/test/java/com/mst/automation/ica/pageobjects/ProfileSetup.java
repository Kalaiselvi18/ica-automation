package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To select the profile name
 *
 */
public class ProfileSetup extends DriverClass {

	@FindBy(css = "input[class='searchBox input']")
	public WebElement profileName;

	public By claimsManagerVerify() {
		By verifyManager = By.xpath("//mark[contains(.,'Profiles')]");
		return verifyManager;
	}
	
	public By objectManagerVerify() {
		By objManager = By.xpath("//mark[contains(.,'Object Manager')]");
		return objManager;
	}

	@FindBy(xpath = "//mark[contains(.,'Profiles')]")
	public WebElement claimsManager;
	
	@FindBy(xpath = "//mark[contains(.,'Object Manager')]")
	public WebElement objectManager;

	public By profileVerifyHighlight() {
		By verifyProfile = By.xpath("//span[contains(.,'Profiles')]");
		return verifyProfile;
	}

	public By rolesVerifyHighlight() {
		By verifyRoles = By.xpath("//span[contains(.,'Roles')]");
		return verifyRoles;
	}

	@FindBy(xpath = "//span[contains(.,'Profiles')]")
	public WebElement profileVerify;

	@FindBy(xpath = "//span[contains(.,'Roles')]")
	public WebElement rolesVerify;

	@FindBy(xpath = "//a[text()='Users']/following::ul[1]")
	public WebElement rolesClick;
	
	public By rolesHigh() {
		By rh = By.xpath("//a[text()='Users']/following::ul[1]");
		return rh;
	}

	public By rolesVerified() {
		By rv = By.xpath("//a[text()='Users']/following::ul[1]");
		return rv;
	}

	public ProfileSetup(WebDriver driver) {
		super(driver);
	}

	/*To select the profile name*/
	public ProfileNameSelect searchProfile(String methodName, String tcName, ReportGenerator generator)
			throws Exception {

		
		
		profileName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "profile"));

		generator.childReport("Profile entered");
		SeleniumUtils.presenceOfElement(driver, claimsManagerVerify());
		claimsManager.click();
		generator.childReport("Profile Clicked");
		SeleniumUtils.presenceOfElement(driver, profileVerifyHighlight());
		SeleniumUtils.highLightElement(profileVerify, driver);
		generator.childReport("Profile Verified");
		return new ProfileNameSelect(driver);
	}
	

	/*To click on the roles*/
	public RolesClick searchRoles(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		
		
		profileName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "role"));
		generator.childReport("Roles entered");
		SeleniumUtils.highLightElement(rolesClick, driver);
		SeleniumUtils.presenceOfElement(driver, rolesHigh());
		SeleniumUtils.elementToBeClickable(driver, rolesHigh());
		SeleniumUtils.presenceOfElement(driver, rolesHigh());
		rolesClick.click();
		SeleniumUtils.presenceOfElement(driver, rolesVerifyHighlight());
		SeleniumUtils.highLightElement(rolesVerify, driver);
		return new RolesClick(driver);
	}
}
